<?php

class Affiliate_WP_Lifetime_Commissions_WooCommerce extends Affiliate_WP_Lifetime_Commissions_Base {
    
	/**
	 * Get things started
	 *
	 * @access  public
	 * @since   1.0
	*/
    public function init() {
        $this->context = 'woocommerce';
    }

    /**
     * Retrieves the user's email or ID
     *
     * @param string $get what to retrieve
     * @param int $reference Payment reference number
     *
     * @since 1.1
     */
    public function get( $get = '', $reference = 0, $context ) {

        if ( ! $get ) {
            return false;
        }

        $order = new WC_Order( $reference );

        if ( 'email' === $get ) {
            return $order->billing_email;
        } elseif ( 'user_id' === $get ) {
            return get_post_meta( $reference, '_customer_user', true );
        }

        return false;

    }


}
new Affiliate_WP_Lifetime_Commissions_WooCommerce;
