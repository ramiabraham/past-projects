Contains various products from the AffiliateWP product family, created
during my time at Sandhills Development, LLC.  

 - `affiliate-wp` contains many contributors and is the core product.
 - All other subdirs are products written solely by me.
 - By request, this code is the state at which it was on my last day of employment, so contains no code
 	for which I was not present.