<?php
/**
 * Plugin Name: Maintainn Emphasis
 * Plugin URI: http://webdevstudios.com
 * Description: Creates a shortcode to add an emphesis class around content.
 * Version: 1.0.0
 * Author: WebDevStudios
 * Author URI: http://webdevstudios.com
 * License: GPL2
 */

function maintainn_emphasis( $atts, $content ) {

	if ( ! isset( $content ) ) {
		return;
	}

	ob_start();

	?>
		<div class="em">
			<?php echo $content; ?>
		</div>
	<?php

	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

add_shortcode( 'emphasis', 'maintainn_emphasis' );