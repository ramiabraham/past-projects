<?php
/**
 * Plugin Name: Maintainn Team
 * Plugin URI: http://webdevstudios.com
 * Description: Sets up Team CPT and other related functionality.
 * Version: 1.0.0
 * Author: WebDevStudios
 * Author URI: http://webdevstudios.com
 * License: GPL2
 */

function maintainn_team_post_type() {
	$labels = array(
		'name'               => _x( 'Team', 'post type general name', 'maintainn' ),
		'singular_name'      => _x( 'Team', 'post type singular name', 'maintainn' ),
		'add_new'            => _x( 'Add New Member', 'team', 'maintainn' ),
		'add_new_item'       => __( 'Add New Member', 'maintainn' ),
		'edit_item'          => __( 'Edit Member', 'maintainn' ),
		'edit'               => _x( 'Edit Member', 'team', 'maintainn' ),
		'new_item'           => __( 'New Member', 'maintainn' ),
		'view_item'          => __( 'View Member', 'maintainn' ),
		'search_items'       => __( 'Search Team', 'maintainn' ),
		'not_found'          => __( 'No Member found', 'maintainn' ),
		'not_found_in_trash' => __( 'No member found in Trash', 'maintainn' ),
		'view'               => __( 'View Member', 'maintainn' ),
		'parent_item_colon'  => ''
	);
	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'has_archive'        => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'team' ),
		'capability_type'    => 'post',
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'          => plugins_url( dirname( plugin_basename( __FILE__ ) ) ) . '/images/maintainn_icon.png',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
	);

	register_post_type( 'team', $args );
}
add_action( 'init', 'maintainn_team_post_type', 1 );

function maintainn_team_metaboxes( ) {

	$prefix = 'cmb_';

	$cmb = new_cmb2_box( array(
		'id'            => 'team_member_details',
		'title'         => __( 'Team Member Details', 'maintainn' ),
		'object_types'  => array( 'team', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true,
	) );

	$cmb->add_field( array(
		'name' => 'Member Title',
		'desc' => 'Position/Title for team member',
		'id' => $prefix . 'member-title',
		'type' => 'text'
	) );

	$cmb->add_field( array(
		'name' => 'Twitter Username',
		'desc' => 'Twitter username for team memeber',
		'id' => $prefix . 'twitter-username',
		'type' => 'text'
	) );

}
add_action( 'cmb2_init', 'maintainn_team_metaboxes' );

function maintainn_team_home_page_metabox() {

	$cmb = cmb2_get_metabox( 'home-page-options-metabox' );
	$cmb->add_field( array(
		'name' => __( 'Team Section', 'maintainn' ),
		'desc' => __( 'Configure the team section of the home page.', 'maintainn' ),
		'id'   => 'maintainn_home_page_team',
		'type' => 'title',
	) );
	$cmb->add_field( array(
		'name' => __( 'Home Page Team Title', 'maintainn' ),
		'id'   => 'maintainn_home_team_title',
		'type' => 'text',
	) );
	$cmb->add_field( array(
		'name' => __( 'Home Page Team Copy', 'maintainn' ),
		'id'   => 'maintainn_home_team_copy',
		'type' => 'textarea',
	) );
	$cmb->add_field( array(
		'name' => __( 'Team WDS section title', 'maintainn' ),
		'id'   => 'maintainn_home_team_wds_title',
		'type' => 'text',
	) );
	$cmb->add_field( array(
		'name' => __( 'Team WDS section copy', 'maintainn' ),
		'id'   => 'maintainn_home_team_wds_copy',
		'type' => 'textarea',
	) );
}
add_action( 'cmb2_init', 'maintainn_team_home_page_metabox', 20 );

function maintainn_display_team() {

	$html = '';
	$args = array(
		'post_type'      => 'team',
		'posts_per_page' => 999,
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
	);
	$team = new WP_Query( $args );

	if ( $team->have_posts() ):

		while( $team->have_posts() ): $team->the_post();
			$html .= '<a href="' . get_the_permalink() . '">';
			$html .= '<div class="team-member ' . esc_attr( sanitize_title_with_dashes( get_the_title() ) ) . '">';
			$html .= '<span class="photo">' . get_the_post_thumbnail( get_the_ID(), array( 126, 126) ) . '</span>';
			$html .= '<span class="name">' . get_the_title() . '</span>';
			$html .= '<span class="job-title">' . esc_html( get_post_meta( get_the_ID(), 'cmb_member-title', true ) );
			$html .= '</div>';
			$html .= '</a>';
		endwhile;

		wp_reset_postdata();

	endif;

	return $html;
}

function maintainn_team_section_title() {

	$option = get_option( 'maintainn-home-page-options' );
	if ( ! isset( $option['maintainn_home_team_title'] ) ) {
		return;
	}
	echo esc_html( $option['maintainn_home_team_title'] );

}

function maintainn_team_section_copy() {

	$option = get_option( 'maintainn-home-page-options' );
	if ( ! isset( $option['maintainn_home_team_title'] ) ) {
		return;
	}
	echo wp_kses_post( $option['maintainn_home_team_copy'] );

}

function maintainn_team_loop() {

	echo maintainn_display_team();

}

function maintainn_team_wds_section_title() {

	$option = get_option( 'maintainn-home-page-options' );
	if ( ! isset( $option['maintainn_home_team_wds_title'] ) ) {
		return;
	}
	echo esc_html( $option['maintainn_home_team_wds_title'] );

}

function maintainn_team_wds_section_copy() {

	$option = get_option( 'maintainn-home-page-options' );
	if ( ! isset( $option['maintainn_home_team_wds_title'] ) ) {
		return;
	}
	echo wp_kses_post( $option['maintainn_home_team_wds_copy'] );

}