<?php
/*
Plugin Name: WDS Post Editor Updates
Plugin URI: 
Description: Update post editor functionality
Version: 1.0.0
Author: Maintainn
Author URI: http://www.maintainn.com
License: GPLv2
Text Domain:
*/

class WDS_Post_Editor_Updates {
  protected static $instance = null;

  public static function get_instance() {
    if ( null === self::$instance ) {
      self::$instance = new self;
    }

    return self::$instance;
  }

  private function __construct() {
    // bail early if not in admin
    if ( ! is_admin() ) {
      return;
    }

    add_filter( 'mce_buttons', array( $this, 'wds7_style_select' ) );
    add_filter( 'tiny_mce_before_init', array( $this, 'wds7_styles_dropdown' ) );

    add_action( 'admin_head', array( $this, 'add_styles' ) );

    // add styles to editor
    add_filter( 'mce_css' , array( $this, 'add_styles' ) );
  }

  /**
   * Add Formats Dropdown Menu To MCE.
   */
   public function wds7_style_select( $buttons ) {
     array_push( $buttons, 'styleselect' );
     return $buttons;
   }

  /**
   * Add new styles to the TinyMCE "formats" menu dropdown.
   */
  public function wds7_styles_dropdown( $settings ) {
     // Create array of new styles
     $new_styles = array(
       array(
         'title' => __( 'WDS7 Styles', 'wds7' ),
         'items' => array(
           array(
             'title'   => __('Intro Paragraph','wds7'),
             'block'   => 'div',
             'classes' => 'intro',
           ),
         ),
       ),
     );
    
     // Merge old & new styles
     $settings['style_formats_merge'] = true;

     // Add new styles
     $settings['style_formats'] = json_encode( $new_styles );

     // Return New Settings
     return $settings;
  }

  public function add_styles( $url ) {
    if ( ! empty( $url ) ) {
        $url .= ',';
    }

    // Retrieves the plugin directory URL and adds editor stylesheet
    // Change the path here if using different directories
    $url .= trailingslashit( plugin_dir_url(__FILE__) ) . 'styles/editor-styles.css';
 
    return $url;
  }
}

// kick it off only if on admin
if ( is_admin() ) {
  WDS_Post_Editor_Updates::get_instance();
}
