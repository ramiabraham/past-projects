<?php
/**
 * Plugin Name: Maintainn Home Page Options
 * Plugin URI: http://webdevstudios.com
 * Description: Sets up Home Page Options.
 * Version: 1.0.0
 * Author: WebDevStudios
 * Author URI: http://webdevstudios.com
 * License: GPL2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Maintainn_Home_Page' ) ) {

	class Maintainn_Home_Page {

		public function __construct() {
			// Set up some variables.
			$this->key = 'maintainn-home';

			add_action( 'cmb2_init', array( $this, 'add_home_page_metabox' ) );
			add_filter( 'maintainn_site_options_tabs', array( $this, 'add_options_tab' ) );
		}

		function add_options_tab( $tabs = array() ) {
			$tabs['home-page'] = array(
				'name'     => __( 'Home Page', 'maintainn' ),
				'callback' => array( $this, 'tab_display' ),
				'priority' => 10,
			);
			return $tabs;
		}

		function tab_display() {
			echo '<h3>' . __( 'Home Page Options', 'maintainn' ) . '</h3>';
			cmb2_metabox_form( 'home-page-options-metabox', 'maintainn-home-page-options' );
		}

		function add_home_page_metabox() {
			$cmb = new_cmb2_box( array(
				'id'      => 'home-page-options-metabox',
				'hookup'  => false,
				'show_on' => array(
					// These are important, don't remove
					'key'   => 'options-page',
					'value' => array( 'maintainn-home-page-options', )
				),
			) );
			// Set our CMB2 fields
			$cmb->add_field( array(
				'name' => __( 'Home page Hero', 'maintain' ),
				'desc' => __( 'Configure the home page Hero call-to-action.', 'maintainn' ),
				'id'   => 'maintainn_home_page_cta',
				'type' => 'title',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Hero Title', 'maintainn' ),
				'id'   => 'maintainn_home_cta_title',
				'type' => 'text',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Hero Copy', 'maintainn' ),
				'id'   => 'maintainn_home_cta_copy',
				'type' => 'textarea',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Hero Button Text', 'maintainn' ),
				'id'   => 'maintainn_home_cta_button_text',
				'type' => 'text',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Hero Button URL', 'maintainn' ),
				'id'   => 'maintainn_home_cta_button_url',
				'type' => 'text_url',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Hero Button Text Underneath', 'maintainn' ),
				'id'   => 'maintainn_home_cta_button_text_underneath',
				'type' => 'text',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Hero Image', 'maintainn' ),
				'id'   =>'maintainn_home_page_cta_image',
				'type' => 'file',
			) );
			
			$cmb->add_field( array(
				'name' => __( 'Home page Sign-up CTA', 'maintain' ),
				'desc' => __( 'Configure the home page Sign-up call-to-action.', 'maintainn' ),
				'id'   => 'maintainn_home_page_signup_cta',
				'type' => 'title',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Sign-up CTA Title', 'maintainn' ),
				'id'   => 'maintainn_home_signup_cta_title',
				'type' => 'text',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Sign-up CTA Copy', 'maintainn' ),
				'id'   => 'maintainn_home_signup_cta_copy',
				'type' => 'textarea',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Sign-up CTA Button Text', 'maintainn' ),
				'id'   => 'maintainn_home_signup_cta_button_text',
				'type' => 'text',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Sign-up CTA Button URL', 'maintainn' ),
				'id'   => 'maintainn_home_signup_cta_button_url',
				'type' => 'text_url',
			) );
			$cmb->add_field( array(
				'name' => __( 'Home Page Sign-up CTA Button Text Underneath', 'maintainn' ),
				'id'   => 'maintainn_home_signup_cta_button_text_underneath',
				'type' => 'text',
			) );
			
		}


	}

	new Maintainn_Home_Page();

}


/**
 * Loop through the Home page options
 * and display the Sign-up CTA info
 */
function maintainn_build_signup_cta_loop() {

	$option = get_option( 'maintainn-home-page-options' );

	if ( ! $option ) {
		return;
	}
	
	$html = '';
	$html .= '<div class="signup-cta-content">';
	$html .= '<h6 class="signup-cta-title h2">' . esc_html( $option['maintainn_home_signup_cta_title'] ) . '</h6>';
	$html .= '<div class="signup-cta-copy">' . esc_html( $option['maintainn_home_signup_cta_copy'] ) . '</div>';
	$html .= '</div><!-- .signup-cta-content -->';
	$html .= '<div class="signup-cta-button">';
	$html .= '<a class="button button--blue" href="' . $option['maintainn_home_signup_cta_button_url'] . '">' . esc_html( $option['maintainn_home_signup_cta_button_text'] ) . '</a>';
	$html .= '<small>' . esc_html( $option['maintainn_home_signup_cta_button_text_underneath'] ) . '</small>';
	$html .= '</div><!-- .signup-cta-button -->';

	return $html;

}

/**
 * Echo the Sign-up CTA
 */
function maintainn_signup_cta_loop() {

	echo maintainn_build_signup_cta_loop();

}