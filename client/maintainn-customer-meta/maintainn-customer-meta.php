<?php
/*
Plugin Name: Maintainn Customer Meta
Plugin URI: https://maintainn.com
Description: Shows customer plan and wp site url in their profile page and the list table view on the user profiles screen.
Version: 1.0
License: GPL
Author: Maintainn
Author URI: https://maintainn.com

*/

add_action( 'show_user_profile', 'maintainn_customer_meta' );
add_action( 'edit_user_profile', 'maintainn_customer_meta' );
add_action( 'personal_options_update', 'update_maintainn_customer_profile_fields' );
add_action( 'edit_user_profile_update', 'update_maintainn_customer_profile_fields' );

// Add WP User Profiles plugin support
add_filter( 'wp_user_profiles_sections', 'maintainn_customer_add_profile_section' );


function maintainn_customer_meta( $user ) { ?>

	<h3>Customer information</h3>

	<table class="form-table">

		<tr>
			<th><label for="maintainn_plan">Current Plan</label></th>

			<td>
				<input type="text" name="maintainn_plan" id="maintainn_plan" value="<?php echo esc_attr( get_the_author_meta( 'maintainn_plan', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Current Maintainn Plan</span>
			</td>
		</tr>
		<tr>
			<th><label for="maintainn_plan">Maintainn Site URL</label></th>

			<td>
				<input type="text" name="customer_site_url" id="customer_site_url" value="<?php echo esc_attr( get_the_author_meta( 'customer_site_url', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Maintainn customer site url</span>
			</td>
		</tr>

	</table>
<?php }

function update_maintainn_customer_profile_fields( $user_id ) {
    if ( current_user_can( 'edit_user', $user_id ) )
        update_user_meta( $user_id, 'maintainn_plan', $_POST['maintainn_plan'] );
        update_user_meta( $user_id, 'customer_site_url', $_POST['customer_site_url'] );
}


// WP User Profiles

/**
 * Add new section to User Profiles
 *
 * @since 0.1.9
 *
 * @param array $sections
 */
function maintainn_customer_add_profile_section( $sections = array() ) {

	// Copy for modifying
	$new_sections = $sections;

	// Add the "Activity" section
	$new_sections['customer'] = array(
		'id'    => 'customer',
		'slug'  => 'customer',
		'name'  => esc_html__( 'Customer Info', 'customer-info' ),
		'cap'   => 'edit_profile',
		'icon'  => 'dashicons-info',
		'order' => 90
	);

	// Filter & return
	return apply_filters( 'maintainn_customer_add_profile_section', $new_sections, $sections );
}
