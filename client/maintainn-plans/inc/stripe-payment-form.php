<?php

 /*
 * Replaces ng app with Gravity Forms form,
 * to allow for easier extensability.
 *
 * GF form ID should be 2, but can be adjusted below of course.
 *
 * @return mixed string Gravity Forms form output, identifier)
 */
function maintainn_intake_form( $step ) {

	switch( $step ) {
		case '1':
		// multi-part GF form step to load
		$intake_form_output = '';
		break;

		case '2':
		// multi-part GF form step to load
		$intake_form_output = '';
		break;

		case '3':
		// multi-part GF form step to load
		$intake_form_output = '';
		break;

		case '4':
		// multi-part GF form step to load
		$intake_form_output = '';
		break;
	}

	// Return the entire form, which we can't use in the case of the multi-part ng ui.
	// Kept temporarily for reference.
	//
	//$gf_intake_form = do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]');
	// echo $gf_intake_form;

	echo $intake_form_output;
}

/*
 * Prints errors in the previously-used Stripe custom form
 *
 * Kept for now, for reference.
 *
 * @return string
 */
function maintainn_signup_error( $error ) {

	switch ( $error ) {
		case 'username_invalid' :
			$message = __( 'The username you entered was invalid.', 'pippin_stripe' );
			break;

		case 'username_exists' :
			$message = sprintf( __( 'The username you entered already exists. Please <a href="%s">sign in</a> or choose a different username.', 'pippin_stripe' ), wp_login_url() );
			break;

		case 'email_exists' :
			$message = sprintf( __( 'The email address you entered already exists. Please <a href="%s">sign in</a> or choose a different email address.', 'pippin_stripe' ), wp_login_url() );
			break;

		case 'no_email' :
			$message = __( 'The email address you entered does not appear to be valid. Please use a valid email address.', 'pippin_stripe' );
			break;

		case 'no_name' :
			$message = __( 'No name was found in your submission. Please enter your name.', 'pippin_stripe' );
			break;

		case 'password_empty' :
			$message = __( 'The password field was empty. Please make sure to enter a valid password.', 'pippin_stripe' );
			break;

		case 'password_mismatch' :
			$message = __( 'The passwords you entered did not match. Please try again.', 'pippin_stripe' );
			break;

		case 'bad_coupon' :
			$message = __( 'The coupon being used is not valid. Please try again.', 'pippin_stripe' );
			break;

		default:
			$message = __( 'An unknown error occurred. Please try again.', 'pippin_stripe' );
			break;
	}

	return $message;
}

function maintainn_stripe_payment_form() {

	wp_enqueue_script( 'angular', $GLOBALS['maintainn_plans']->directory_url . '/assets/js/angular.min.js', array(), '1.3.15', false );
	wp_enqueue_script( 'lodash', $GLOBALS['maintainn_plans']->directory_url . '/assets/js/lodash.min.js', array(), '3.6.0', true );
	wp_enqueue_script( 'angular-wizard', $GLOBALS['maintainn_plans']->directory_url . '/assets/js/angular-wizard.min.js', array( 'angular', 'lodash' ), '0.4.2', true );
	wp_enqueue_script( 'angular-scroll', $GLOBALS['maintainn_plans']->directory_url . '/assets/js/angular-scroll.min.js', array( 'angular' ), '0.6.5', true );
	wp_enqueue_script( 'angular-chosen', $GLOBALS['maintainn_plans']->directory_url . '/assets/js/angular-chosen.min.js', array( 'angular', 'jquery', 'chosen-jquery' ), '04092015', true );

	global $post;
	$user = wp_get_current_user();
	$redirect = '';

	$plan_data = maintainn_get_plan_form_data();
	$form_data = array(
		'uid' => get_current_user_id(),
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
	);


	wp_enqueue_script( 'maintainn-form', $GLOBALS['maintainn_plans']->directory_url . '/assets/js/form.js', array( 'jquery', 'angular' ), '03262015', true );

	wp_localize_script( 'maintainn-form', 'plandata', $plan_data );
	wp_localize_script( 'maintainn-form', 'formdata', $form_data );


	pw_print_errors();
	pw_clear_errors();

	if( isset( $_GET['error'] ) || $_SERVER['QUERY_STRING'] && strpos( $_SERVER['QUERY_STRING'], 'error=' ) !== false ) {

		$error = ( isset( $_GET['error'] ) ) ? $_GET['error'] : str_replace( 'error=', '', $_SERVER['QUERY_STRING'] );

		echo '<p class="error">' . maintainn_signup_error( $error ) . '</p>';
	 	echo '<a href="' . esc_url( site_url( '/pricing' ) ) . '"><button class="error-button button">' . __( 'Start Over', 'maintainn' ). '</button></a>';

	} elseif(isset($_GET['payment']) && $_GET['payment'] == 'paid') {

		echo '<p class="success">' . __('Thank you for your payment. Look for a welcome email soon!', 'pippin_stripe') . '</p>';

	} elseif( isset( $_GET['payment'] ) && $_GET['payment'] == 'failed' ) {

		echo '<p class="error">' . __( 'Sorry but your payment did not go through. Try entering your credit card information again or use a different card', 'pippin_stripe' );
		echo '<a href="' . esc_url( site_url( '/pricing' ) ) . '"><button class="error-button button">' . __( 'Start Over', 'maintainn' ). '</button></a>';

	} else { ?>
		<style type="text/css">
			[ng\:cloak], [ng-cloak], .ng-cloak {
				display: none !important;
			}
		</style>

		<style type="text/css">#creating-account { display:none; color: #333; background: #fff; border: 1px solid #000; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; padding: 30px; font-size: 26px; width: 500px; text-align: center; left: 50%; top: 50%; margin-top: -2em; position: fixed;z-index:99999; margin-left: -250px; } #overlay { background:rgba(0,0,0,0.7); display:none; width:100%; height:100%; position:fixed; top:0; left:0; z-index:99998; }</style>

		<div id="top"></div>

		<div ng-app="MaintainnWizard" ng-cloak class="ng-cloak form-wrap alt-version">

			<div ng-controller="PlansController">

				<wizard on-finish="finishedWizard()" ng-class="'step-' + step()" data-ng-element-ready>

					<form method="POST" name="MaintainnFunnel" action="<?php echo get_permalink($post->ID); ?>" id="maintainn-payment-form" class="stripe-form">

						<!-- BEGIN - Step 1 -->

						<wz-step title="<?php _e('Pick a Plan', 'maintainn'); ?>" canexit="exitPageOne" canenter="enterPage">
							<section id="page-1" class="page pick-plan">
								<label><?php _e('Pick a Plan', 'maintainn'); ?></label>

								<div ng-repeat="plan in plans" class="plan-card {{plan.slug}} {{plan.plan_type}} " ng-class="plan.slug == plan_select.data.slug ? 'selected' : 'not-selected'"  ng-click="planSelected( plan )">

									<span ng-show="$index == 1" class="flag"><?php _e( 'Amazing Value!', 'maintainn' ); ?></span>

									<div ng-if="plan.plan_title && 'pricing' == plan.plan_type" class="title">
										<span class="plan-select {{plan.slug}}">{{plan.plan_title}}</span>
									</div><!-- .title -->

									<div ng-if="plan.plan_title && 'custom' == plan.plan_type" class="title">
										<span class="plan-select {{plan.slug}}">{{plan.plan_title}}</span>
									</div><!-- .title -->

									<div ng-if="plan.short_description" class="short-description" ng-bind-html="plan.short_description | allowHTML">
									</div><!-- .short-description -->

									<div ng-if="'pricing' == plan.plan_type" class="features card-middle">
										<div class="card-middle-feature">
											<ul ng-show="plan.special_features" class="special-features">
												<li ng-repeat="special_feature in plan.special_features" class="special-feature {{special_feature.class}}"><i class="fa {{special_feature.icon}}"></i><span class="label">{{special_feature.label}}</span></li>
											</ul>
										</div><!-- .card-middle-feature -->

										<ul class="feature-list">
											<li ng-repeat="feature in plan.features" class="feature {{feature.class}}>"><i class="fa {{feature.icon}}"></i><span class="label">{{feature.label}}</span></li>
										</ul><!-- .feature-list -->
									</div><!-- .features .card-middle -->

									<div ng-if="'pricing' == plan.plan_type" class="price card-bottom">
										<span class="number">{{plan.payment_options.monthly.price | currency:"$":0}}</span><span class="per-month"><?php _e( '/month', 'maintainn' ); ?></span>
										<span class="per-site"><?php _e( 'per site', 'maintainn' ); ?></span>
									</div><!-- .price .card-bottom -->

									<div ng-if="(plan.custom_blocks.middle || plan.custom_blocks.mid_feature) && 'custom' == plan.plan_type" class="card-middle">
										<div class="card-middle-feature">
											<div class="custom-features" ng-bind-html="plan.custom_blocks.mid_feature | allowHTML">
											</div>
										</div><!-- .card-middle-feature -->

										<div class="custom-feature-list" ng-bind-html="plan.custom_blocks.middle | allowHTML">
										</div>
									</div><!-- .card-middle -->

									<div ng-if="plan.custom_blocks.bottom && 'custom' == plan.plan_type" class="card-bottom" ng-bind-html="plan.custom_blocks.bottom | allowHTML">
									</div><!-- .card-bottom -->

									<a href="<?php echo site_url() . '/sign-up'; ?>"><button ng-if="'pricing' == plan.plan_type" type="button" class="button button--white"><?php _e( 'Start My Support', 'maintainn' ); ?></button></a>
									<button ng-if="'custom' == plan.plan_type" type="button" class="button button--white""><?php _e( 'Contact Us', 'maintainn' ); ?></button>

								</div><!-- .plan-card -->
							</section><!-- .page .pick-plan -->

						</wz-step><!-- /END Step 1 -->


						<!-- BEGIN - Step 2 -->
						<wz-step title="<?php _e('Account Info', 'maintainn'); ?>" canexit="exitPageTwo" canenter="enterPage">

							<section id="page-2" class="page account-info">

							<?php maintainn_intake_form( $step='2' ); ?>

							</section><!-- .page .account-info -->

						</wz-step><!-- /END Step 2 -->


						<!-- BEGIN - Step 3 -->
						<wz-step title="<?php _e('Payment Options', 'maintainn'); ?>" canexit="exitPageThree" canenter="enterPage">
							<section id="page-3" class="page payment-options">

								<?php maintainn_intake_form( $step='3' ); ?>

							</section><!-- .page .payment-options -->

						</wz-step><!-- /END Step 3 -->


						<!-- BEGIN - Step 4 -->
						<wz-step title="<?php _e('Payment', 'maintainn'); ?>" canenter="enterPage">

							<section id="page-4" class="page payment">

								<?php maintainn_intake_form( $step='4' ); ?>

							</section><!-- .page .payment -->

						</wz-step><!-- /END Step 4 -->

					</form>

				</wizard>

			</div>
		</div><!-- .form-wrap -->

		<div id="overlay"></div>

	<?php
	}

}
