<?php
/**
 * Plugin Name: Maintainn Plans
 * Plugin URI: http://webdevstudios.com
 * Description: Creates plans and uses Gravity Forms to create a step by step funnel allowing users to sign up for service.
 * Author: WebDevStudios
 * Author URI: http://webdevstudios.com
 * Version: 1.0.0
 * License: GPLv2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Maintainn_Plans' ) ) {

	class Maintainn_Plans {

		/**
		 * Construct function to get things started.
		 */
		public function __construct() {
			// Setup some base variables for the plugin
			$this->basename       = plugin_basename( __FILE__ );
			$this->directory_path = plugin_dir_path( __FILE__ );
			$this->directory_url  = plugins_url( dirname( $this->basename ) );

			// Include any required files
			add_action( 'init', array( $this, 'includes' ) );

			// Activation/Deactivation Hooks
			register_activation_hook( __FILE__, array( $this, 'activate' ) );
			register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );


		}

		/**
		 * Include our plugin dependencies.
		 */
		public function includes() {
			require_once( $this->directory_path . 'inc/stripe-payment-form.php' );
		}

		/**
		 * Register CPTs & taxonomies.
		 */
		public function do_hooks() {
			add_action( 'init', array( $this, 'register_post_types' ), 9 );
			add_action( 'cmb2_init', array( $this, 'metaboxes' ) );
			add_action( 'edit_form_after_title', array( $this, 'edit_form_after_title' ) );
			add_filter( 'wp_editor_settings', array( $this, 'wp_editor_settings' ) );
			add_filter( 'cmb2_show_on', array( $this, 'show_on_filter' ), 10, 2 );

			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts') );
		}

		function enqueue_scripts() {
			$min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';

			wp_enqueue_style( 'maintainn-plans', $this->directory_url . "/assets/css/styles$min.css", array(), '150406' );
			wp_enqueue_style( 'chosen-jquery-styles', $this->directory_url . "/assets/css/chosen$min.css", array(), '150408' );
			wp_enqueue_script( 'chosen-jquery', $this->directory_url . "/assets/js/chosen-jquery$min.js", array('jquery'), false, true );
		}

		/**
		 * Activation hook for the plugin.
		 */
		public function activate() {

		}

		/**
		 * Deactivation hook for the plugin.
		 */
		public function deactivate() {

		}

		/**
		 * Check that all plugin requirements are met
		 *
		 * @return boolean
		 */
		public static function meets_requirements() {

			// We have met all requirements
			return true;

		}

		/**
		 * Register custom post-types.
		 */
		public function register_post_types() {

			$labels = array(
				'name'               => esc_html_x( 'Plans', 'post type general name', 'maintainn' ),
				'singular_name'      => esc_html_x( 'Plan', 'post type singular name', 'maintainn' ),
				'menu_name'          => esc_html_x( 'Plans', 'admin menu', 'maintainn' ),
				'add_new'            => esc_html_x( 'Add New', 'book', 'maintainn' ),
				'add_new_item'       => esc_html__( 'Add New Plan', 'maintainn' ),
				'new_item'           => esc_html__( 'New Plan', 'maintainn' ),
				'edit_item'          => esc_html__( 'Edit Plan', 'maintainn' ),
				'view_item'          => esc_html__( 'View Plan', 'maintainn' ),
				'all_items'          => esc_html__( 'All Plans', 'maintainn' ),
				'search_items'       => esc_html__( 'Search Plans', 'maintainn' ),
				'not_found'          => esc_html__( 'No plans found.', 'maintainn' ),
				'not_found_in_trash' => esc_html__( 'No plans found in Trash.', 'maintainn' )
			);
			$args = array(
				'labels'            => $labels,
				'public'            => false,
				'menu_icon'         => 'dashicons-clipboard',
				'show_ui'           => true,
				'show_in_admin_bar' => false,
				'show_in_nav_menus' => false,
				'supports'          => array( 'title', 'editor', 'page-attributes' )

			);
			register_post_type( 'maintainn_plans', $args );

		}

		/**
		 * Register metaboxes and fields using CMB2
		 */
		function metaboxes(){

			// Plan Type
			$type = new_cmb2_box( array(
				'id'            => 'maintainn_plans_type',
				'title'         => __( 'Plan Type', 'maintainn' ),
				'object_types'  => array( 'maintainn_plans', ), // Post type
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true,
			) );

			$type->add_field( array(
				'name'    => __( 'Select Plan Type', 'maintainn' ),
				'desc'    => __( 'After selecting your plan type, save draft on post to enable related fields.', 'maintainn' ),
				'id'      => 'maintainn_plan_type',
				'type'    => 'radio_inline',
				'options' => array(
					'pricing'  => __( 'Priced', 'maintainn' ),
					'custom'   => __( 'Custom', 'maintainn' ),
				),
			) );

			// Pricing Metabox
			$prices = new_cmb2_box( array(
				'id'            => 'maintainn_plans_pricing',
				'title'         => __( 'Pricing', 'maintainn' ),
				'object_types'  => array( 'maintainn_plans', ), // Post type
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true,
				'show_on'       => array( 'key' => 'plan_type', 'value' => 'pricing' ),
			) );

			if ( function_exists( 'pw_get_stripe_plans' ) ) {
				$plans      = pw_get_stripe_plans();
				$plan_array = array();
				$plan_array[0] = __( '-- Select Plan --', 'maintainn' );
				foreach ( $plans as $key => $value ) {
					$plan_array[ $key ] = $value . ' [ID: ' . $key . ']';
				}
			}

			$prices->add_field( array(
				'name' => __( 'Price Monthly', 'maintainn' ),
				'desc' => __( 'Price per month if the user selects monthly payments.', 'maintainn' ),
				'id'   => 'maintainn_plans_price_monthly',
				'type' => 'text_money',
			) );

			if ( ! function_exists( 'pw_get_stripe_plans' ) ) {
				$prices->add_field( array(
					'name'    => __( 'Monthly Stripe Plan', 'maintainn' ),
					'desc'    => __( 'The Stripe Recurring Payments plugin needs to be installed to link plans to a Stripe payment plan for processing.', 'maintainn' ),
					'id'      => 'maintainn_plans_stripe_not_installed',
					'type' => 'title',
				) );
			} else {
				$prices->add_field( array(
					'name'    => __( 'Monthly Stripe Plan', 'maintainn' ),
					'desc'    => __( 'Please select the associated plan from Stripe.', 'maintainn' ),
					'id'      => 'maintainn_plans_monthly_plan_id',
					'type'    => 'select',
					'options' => $plan_array,
				) );
			}

			$prices->add_field( array(
				'name' => __( 'Price Yearly', 'maintainn' ),
				'desc' => __( 'Price per month if the user selects yearly payments.', 'maintainn' ),
				'id'   => 'maintainn_plans_price_yearly',
				'type' => 'text_money',
			) );

			if ( ! function_exists( 'pw_get_stripe_plans' ) ) {
				$prices->add_field( array(
					'name'    => __( 'Yearly Stripe Plan', 'maintainn' ),
					'desc'    => __( 'The Stripe Recurring Payments plugin needs to be installed to link plans to a Stripe payment plan for processing.', 'maintainn' ),
					'id'      => 'maintainn_plans_stripe_not_installed',
					'type' => 'title',
				) );
			} else {
				$prices->add_field( array(
					'name'    => __( 'Yearly Stripe Plan', 'maintainn' ),
					'desc'    => __( 'Please select the associated plan from Stripe.', 'maintainn' ),
					'id'      => 'maintainn_plans_yearly_plan_id',
					'type'    => 'select',
					'options' => $plan_array,
				) );
			}

			// Special Features Metabox
			$special_features = new_cmb2_box( array(
				'id'            => 'maintainn_plans_special_feat_meta',
				'title'         => __( 'Special Features', 'maintainn' ),
				'object_types'  => array( 'maintainn_plans', ), // Post type
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true,
				'show_on'       => array( 'key' => 'plan_type', 'value' => 'pricing' ),
			) );

			$special_features_group = $special_features->add_field( array(
				'id'          => 'maintainn_plan_special_features',
				'type'        => 'group',
				'options'     => array(
					'group_title'   => __( 'Special Feature {#}', 'maintainn' ), // {#} gets replaced by row number
					'add_button'    => __( 'Add Another Feature', 'maintainn' ),
					'remove_button' => __( 'Remove Feature', 'maintainn' ),
					'sortable'      => true,
				),
			) );

			$special_features->add_group_field( $special_features_group, array(
				'name' => __( 'Feature Icon', 'maintainn' ),
				'id'   => 'icon',
				'type' => 'text',
			) );

			$special_features->add_group_field( $special_features_group, array(
				'name' => __( 'Feature Label', 'maintainn' ),
				'id'   => 'label',
				'type' => 'text',
			) );

			// Features Metabox
			$features = new_cmb2_box( array(
				'id'            => 'maintainn_plans_feat_meta',
				'title'         => __( 'Features', 'maintainn' ),
				'object_types'  => array( 'maintainn_plans', ), // Post type
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true,
				'show_on'       => array( 'key' => 'plan_type', 'value' => 'pricing' ),
			) );

			$features_group = $features->add_field( array(
				'id'          => 'maintainn_plan_features',
				'type'        => 'group',
				'options'     => array(
					'group_title'   => __( 'Feature {#}', 'maintainn' ), // {#} gets replaced by row number
					'add_button'    => __( 'Add Another Feature', 'maintainn' ),
					'remove_button' => __( 'Remove Feature', 'maintainn' ),
					'sortable'      => true,
				),
			) );

			$features->add_group_field( $features_group, array(
				'name' => __( 'Feature Icon', 'maintainn' ),
				'id'   => 'icon',
				'type' => 'text',
			) );

			$features->add_group_field( $features_group, array(
				'name' => __( 'Feature Label', 'maintainn' ),
				'id'   => 'label',
				'type' => 'text',
			) );

			// Custom Metabox
			$custom = new_cmb2_box( array(
				'id'            => 'maintainn_plans_custom',
				'title'         => __( 'Custom', 'maintainn' ),
				'desc'          => __( 'Enter info for custom plans. This plan type won\'t have a set price or features.', 'maintainn' ),
				'object_types'  => array( 'maintainn_plans', ), // Post type
				'context'       => 'normal',
				'priority'      => 'high',
				'show_on'       => array( 'key' => 'plan_type', 'value' => 'custom' ),
			) );

			$custom->add_field( array(
				'name' => __( 'Card Middle Feature Content', 'maintainn' ),
				'desc' => __( 'Content in the the featured area of the middle block in the pricing table. Leave blank if you want no featured area.', 'cmb2' ),
				'id'   => 'maintainn_plans_custom_card_middle_feature_label',
				'type' => 'title',
			) );

			$custom->add_field( array(
				'name'       => __( 'Card Middle Feature Content', 'maintainn' ),
				'id'         => 'maintainn_plans_custom_card_middle_feature',
				'type'       => 'wysiwyg',
				'options'    => array( 'textarea_rows' => 5, ),
				'show_names' => false,
			) );

			$custom->add_field( array(
				'name' => __( 'Card Middle Content', 'maintainn' ),
				'desc' => __( 'Content in the middle block of the pricing table.', 'cmb2' ),
				'id'   => 'maintainn_custom_card_middle_label',
				'type' => 'title',
			) );

			$custom->add_field( array(
				'name'    => __( 'Block 2 Content', 'maintainn' ),
				'id'      => 'maintainn_plans_custom_card_middle',
				'type'    => 'wysiwyg',
				'options' => array( 'textarea_rows' => 5, ),
				'show_names' => false,
			) );

			$custom->add_field( array(
				'name' => __( 'Card Bottom Content', 'maintainn' ),
				'desc' => __( 'Content in the bottom block of the pricing table.', 'cmb2' ),
				'id'   => 'maintainn_card_bottom_label',
				'type' => 'title',
			) );

			$custom->add_field( array(
				'name'       => __( 'Card Bottom Content', 'maintainn' ),
				'id'         => 'maintainn_plans_custom_card_bottom',
				'type'       => 'wysiwyg',
				'options'    => array( 'textarea_rows' => 5, ),
				'show_names' => false,
			) );

			$custom->add_field( array(
				'name' => __( 'Redirect URL', 'maintainn' ),
				'desc' => __( 'Redirect on click to this URL.  Usually a contact form page.', 'cmb2' ),
				'id'   => 'maintainn_plans_custom_redirect',
				'type' => 'text',
			) );

			$custom->add_field( array(
				'name' => __( 'Payment Options Page Text', 'maintainn' ),
				'desc' => __( 'Content displayed on the payment options page.', 'cmb2' ),
				'id'   => 'maintainn_payment_options_label',
				'type' => 'title',
			) );

			$custom->add_field( array(
				'name'       => __( 'Payment Options Page Text', 'maintainn' ),
				'id'         => 'maintainn_plans_custom_payment_options_page',
				'type'       => 'wysiwyg',
				'options'    => array( 'textarea_rows' => 5, ),
				'show_names' => false,
			) );

			$custom->add_field( array(
				'name' => __( 'Payment Page Text', 'maintainn' ),
				'desc' => __( 'Content displayed on the payment page.', 'cmb2' ),
				'id'   => 'maintainn_payment_label',
				'type' => 'title',
			) );

			$custom->add_field( array(
				'name'       => __( 'Payment Page Text', 'maintainn' ),
				'id'         => 'maintainn_plans_custom_payment_page',
				'type'       => 'wysiwyg',
				'options'    => array( 'textarea_rows' => 5, ),
				'show_names' => false,
			) );

		}

		/**
		 * Determine whether to show a metabox or not.
		 *
		 * @param bool  $show    If the metabox should be shown.
		 * @param array $metabox Array of metabox settings and info.
		 * @return bool Modified value of if the metabox should be shown.
		 */
		function show_on_filter( $show, $metabox ) {

			if ( ! isset( $metabox['show_on']['key'] ) ) {
				return $show;
			}

			if ( 'maintainn_plans_custom' != $metabox['id'] && 'maintainn_plans_special_feat_meta' != $metabox['id'] && 'maintainn_plans_feat_meta' != $metabox['id'] && 'maintainn_plans_pricing' != $metabox['id'] ) {
				return $show;
			}

			global $post;

			$plan_type = get_post_meta( $post->ID, 'maintainn_plan_type', true );
			if ( ! $plan_type ) {
				return false;
			}

			if ( 'maintainn_plans_custom' == $metabox['id'] && 'custom' != $plan_type ) {
				return false;
			}

			if ( 'maintainn_plans_special_feat_meta' == $metabox['id'] && 'pricing' != $plan_type ) {
				return false;
			}

			if ( 'maintainn_plans_feat_meta' == $metabox['id'] && 'pricing' != $plan_type ) {
				return false;
			}

			if ( 'maintainn_plans_pricing' == $metabox['id'] && 'pricing' != $plan_type ) {
				return false;
			}

			return $show;
		}

		/**
		 * Conditionally show metabox for set priced plan types.
		 *
		 * @return bool True if the metabox should be shown.
		 */
		function show_pricing_metabox() {

			global $post;

			if ( 'priced' == get_post_meta( $post->ID, 'maintainn_plan_type', true ) ) {
				return true;
			} else {
				return false;
			}

		}

		/**
		 * Conditionally show metabox for custom priced plan types.
		 *
		 * @return bool True if the metabox should be shown.
		 */
		function show_custom_metabox() {

			global $post;

			if ( 'custom' == get_post_meta( $post->ID, 'maintainn_plan_type', true ) ) {
				return true;
			} else {
				return false;
			}

		}

		/**
		 * Add some markup above the post edit box on Plans.
		 *
		 * @param $post The post object.
		 */
		function edit_form_after_title( $post ) {

			if ( 'maintainn_plans' == $post->post_type ) {
				echo '<br /><br /><h5 class="cmb2-metabox-title">' . esc_html__( 'Short Description', 'maintainn' ) . '</h5>';
				echo '<span class="cmb2-metabox-description">' . esc_html__( 'Enter the short description of the plan which appears in the top block of the pricing table.', 'maintainn' ) . '</span>';
			}

		}

		/**
		 * Change the size of the post editor on Plans.
		 *
		 * @param array $settings   The current editor settings.
		 * @return array The modified editor settings.
		 */
		function wp_editor_settings( $settings ) {

			global $post;
			if ( isset( $post->post_type ) && 'maintainn_plans' == $post->post_type ) {
				$settings['editor_height'] = 150;
			}

			return $settings;

		}

	}

	$GLOBALS['maintainn_plans'] = new Maintainn_Plans;
	$GLOBALS['maintainn_plans']->do_hooks();
}

function maintainn_get_plan_form_data() {

	$args = array(
		'post_type'      => 'maintainn_plans',
		'posts_per_page' => 3,
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'no_found_rows' => true,
		'update_post_term_cache' => false,
	);
	$plan_array = get_posts( $args );

	$plan_data = array();
	foreach ( $plan_array as $plan ) {
		$plan_type = get_post_meta( $plan->ID, 'maintainn_plan_type', true );
		$features = get_post_meta( $plan->ID, 'maintainn_plan_features', true );
		$special_features = get_post_meta( $plan->ID, 'maintainn_plan_special_features', true );
		$feat_array = array();

		if ( 'pricing' !== $plan_type ) {
			$feat_array = false;
			$special_feat_array = false;
		} else {
			if ( $features ) {
				foreach ( $features as $feature ) {
					$feat_array[] = array(
						'label' => esc_html( $feature['label'] ),
						'icon'  => esc_html( $feature['icon'] ),
						'class' => esc_html( sanitize_title_with_dashes( $feature['label'] ) ),
					);
				}
			} else {
				$feat_array = false;
			}
			if ( $special_features ) {
				foreach ( $special_features as $special_feature ) {
					$special_feat_array[] = array(
						'label' => esc_html( $special_feature['label'] ),
						'icon'  => esc_html( $special_feature['icon'] ),
						'class' => esc_html( sanitize_title_with_dashes( $special_feature['label'] ) ),
					);
				}
			} else {
				$special_feat_array = false;
			}
		}

		if ( 'custom' !== $plan_type ) {
			$custom_blocks = false;
		} else {
			$custom_blocks = array(
				'mid_feature'     => wp_kses_post( get_post_meta( $plan->ID, 'maintainn_plans_custom_card_middle_feature', true ) ),
				'middle'          => wp_kses_post( get_post_meta( $plan->ID, 'maintainn_plans_custom_card_middle', true ) ),
				'bottom'          => wp_kses_post( get_post_meta( $plan->ID, 'maintainn_plans_custom_card_bottom', true ) ),
				'payment_options' => wp_kses_post( get_post_meta( $plan->ID, 'maintainn_plans_custom_payment_options_page', true ) ),
				'payment_page'    => wp_kses_post( get_post_meta( $plan->ID, 'maintainn_plans_custom_payment_page', true ) ),
				'redirect'        => esc_url( get_post_meta( $plan->ID, 'maintainn_plans_custom_redirect', true ) ),
			);
		}

		if ( 'pricing' !== $plan_type ) {
			$payment_options = false;
		} else {
			$monthly_price = floatval( get_post_meta( $plan->ID, 'maintainn_plans_price_monthly', true ) );
			$yearly_price = floatval( get_post_meta( $plan->ID, 'maintainn_plans_price_yearly', true ) );
			$payment_options = array(
				'monthly'   => array(
					'type'     => 'monthly',
					'price'    => floatval( $monthly_price ),
					'id'       => intval( get_post_meta( $plan->ID, 'maintainn_plans_monthly_plan_id', true ) ),
					'per_year' => floatval( $monthly_price * 12 ),
					'pay_now'  => floatval( $monthly_price ),
					'savings'  => false,
				),
				'yearly'    => array(
					'type'     => 'yearly',
					'price'    => floatval( $yearly_price ),
					'id'       => intval( get_post_meta( $plan->ID, 'maintainn_plans_yearly_plan_id', true ) ),
					'per_year' => floatval( $yearly_price * 12 ),
					'pay_now'  => floatval( $yearly_price * 12 ),
					'savings'  => round( ( 1 - ( $yearly_price / $monthly_price ) ) * 100 ),
				),
			);
		}

		$plan_data[] = array(
			'plan_title'        => $plan->post_title,
			'plan_type'         => esc_attr( $plan_type ),
			'short_description' => wp_kses_post( $plan->post_content ),
			'slug'              => esc_attr( $plan->post_name ),
			'special_features'  => $special_feat_array,
			'features'          => $feat_array,
			'custom_blocks'     => $custom_blocks,
			'payment_options'   => $payment_options,
		);

	}

	return $plan_data;

}