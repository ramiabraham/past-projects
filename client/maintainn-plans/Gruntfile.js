module.exports = function(grunt) {

	// load all grunt tasks in package.json matching the `grunt-*` pattern
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

		sass: {
			dist: {
				options: {
					style: 'expanded',
					lineNumbers: true,
					loadPath: [
						'assets/css/bower_components/bourbon/app/assets/stylesheets',
						'assets/css/bower_components/neat/app/assets/stylesheets'
					]
				},
				files: {
					'assets/css/styles.css': 'assets/css/styles.scss'
				}
			}
		},

		cssmin: {
			minify: {
				expand: true,
				cwd: 'assets/css/',
				src: ['*.css', '!*.min.css'],
				dest: 'assets/css/',
				ext: '.min.css'
			}
		},
		
		uglify: {
			build: {
				options: {
					mangle: false
				},
				files: [{
					expand: true,
					cwd: 'assets/js/',
					src: ['**/*.js', '!**/*.min.js', '!concat/*.js'],
					dest: 'assets/js/',
					ext: '.min.js'
				}]
			}
		},

		watch: {
			
			scripts: {
				files: ['assets/js/**/*.js'],
				tasks: ['javascript'],
				options: {
					spawn: false,
					livereload: true,
				},
			},

			css: {
				files: ['assets/css/sass/**/*.scss'],
				tasks: ['sass'],
				options: {
					spawn: false,
					livereload: true,
				},
			},

		},

	});

	grunt.registerTask('javascript', ['uglify']);
	grunt.registerTask('default', ['sass', 'cssmin']);

};
