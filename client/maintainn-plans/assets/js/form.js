/**
 * Set up our Angular App
 */
var app = angular.module( 'MaintainnWizard', ['mgo-angular-wizard', 'angular.chosen', 'duScroll'] );

/**
 * Our main controller
 */
app.controller( 'PlansController', [ '$scope', '$document', '$window', 'WizardHandler', function( $scope, $document, $window, WizardHandler ) {
	//var getCookie = function() {
	//	var name = "maintainn_selected_plan=";
	//	var ca = document.cookie.split(';');
	//
	//	for(var i=0; i < ca.length; i++) {
	//		var c = ca[i];
	//		while (c.charAt(0)==' ') c = c.substring(1);
	//		if (c.indexOf(name) == 0) return angular.fromJson( c.substring(name.length,c.length) );
	//	}
	//
	//	return "";
	//}

	$scope.goTo = function(page) {
		if (false != page) {
			WizardHandler.wizard().goTo( page );
		}
	};

	$scope.planSelected = function( data ) {
		console.log( data );
		if ( 'custom' == data.plan_type ) {
			$window.location.href = data.custom_blocks.redirect;
		}
		$scope.plan_select = { data: data };
	}

	$scope.optionSelected = function( data ) {
		console.log( data );
		$scope.my_plan = { data: data };
	}

	$scope.plans = plandata;
	$scope.plan_select = { data: $scope.plans[1] };
	$scope.user_id = parseInt( formdata.uid );
	$scope.user = {};
	$scope.step = function() {
		return parseInt(WizardHandler.wizard().currentStepNumber());
	}
	$scope.error = {};

	$scope.exitPageThree = function() {
		console.log($scope.my_plan);
		if ( ! $scope.my_plan ) {
			$scope.error.payment_options = {
				class: 'select-payment-option',
				message: 'Please Select a Payment Option'
			}
			return false;
		}
		return true;
	}

	//$scope.setCookie = function(value) {
	//	var date = new Date();
	//	date.setTime(date.getTime() + (60 * 60 * 1000));
	//	var expires = "; expires=" + date.toGMTString();
	//	value = angular.toJson(value);
	//	document.cookie = "maintainn_selected_plan=" + value + expires + "; path=/";
	//}

	$scope.goToTop = function() {
		$document.scrollTopAnimated(0);
	}

	$scope.StripeSubmit = function() {
		var form = jQuery('#maintainn-payment-form');
		jQuery('#creating-account,#overlay').fadeIn(300);
		// disable the submit button to prevent repeated clicks
		form.find('.stripe-submit-button').attr("disabled", "disabled");
		// send the card details to Stripe
		Stripe.createToken({
			number: form.find('.card-number').val(),
			cvc: form.find('.card-cvc').val(),
			exp_month: form.find('.card-expiry-month').val(),
			exp_year: form.find('.card-expiry-year').val(),
			name: form.find('.card-name').val(),
			address_line1: form.find('.card-address').val(),
			address_line2: form.find('.card-address-2').val(),
			address_state: form.find('.card-state').val(),
			address_zip: form.find('.card-zip').val(),
			address_country: form.find('.card-country').val()
		}, stripeResponseHandler);

		// prevent the form from submitting with the default action
		return false;
	}

	function stripeResponseHandler(status, response) {
		if (response.error) {
			console.log( response.error );
			jQuery('#maintainn-payment-form').find('.payment-error').html('<div class="error">' + response.error.message + '</div>');

			// re-enable the submit button
			jQuery('#maintainn-payment-form').find('.stripe-submit-button').attr("disabled", false);
			jQuery('#creating-account,#overlay').fadeOut(300);
		} else {
			// token contains id, last4, and card type
			var token = response['id'];
			// insert the token into the form so it gets submitted to the server
			jQuery('#maintainn-payment-form').append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
			// and submit

			jQuery('#maintainn-payment-form').submit();
		}
	}

}]);

/**
 * Directive to go to certain page when the form is loaded and the query var is present
 */
app.directive('ngElementReady', [ function() {
	return {
		priority: -1000, // a low number so this directive loads after all other directives have loaded.
		restrict: "A", // attribute only
		link: function( scope ) {
			var getQueryVar = function() {
				var query = window.location.search.substring(1);
				var vars = query.split("&");
				for (var i=0;i<vars.length;i++) {
					var pair = vars[i].split("=");
					if(pair[0] == 'plan_page'){return pair[1];}
				}
				return(false);
			}
			var qv = parseInt( getQueryVar() ) - 1;
			if ( ! qv ) {
				return;
			}
			scope.goTo(qv);
		}
	};
}]);

/**
 * Directive comparing two fields, used to compare passwords.
 */
app.directive('compareTo', function() {
	return {
		require: 'ngModel',
		scope: {
			otherModelValue: '=compareTo'
		},
		link: function(scope, element, attributes, ngModel) {

			ngModel.$validators.compareTo = function(modelValue) {
				return modelValue == scope.otherModelValue;
			};

			scope.$watch('otherModelValue', function() {
				ngModel.$validate();
			});
		}
	};
})

/**
 * Allow HTML to be output from the model
 */
app.filter('allowHTML', [ '$sce', function( $sce ) {
	return function(val) {
		return $sce.trustAsHtml(val);
	};
}]);

/**
 * A bit of jQuery to disable pressing enter to submit form
 */
jQuery(document).ready(function($) {
	$('#maintainn-payment-form').bind('keydown', function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	});
});