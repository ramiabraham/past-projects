<?php
/*
Plugin Name: GF - Raange SMS Add-On
Plugin URI: https://raange.tech
Description: A custom Gravity Forms add-on for Raange SMS API functionality.
Version: 1.0.2
Author: gf-raange-sms
Author URI: https://raange.tech
 */

define('GF_RAANGE_SMS_ADDON_VERSION', '1.0.2');

add_action('gform_loaded', array('GFRS_Bootstrap', 'load'), 5);

class GFRS_Bootstrap {

    public static function load() {

        if (!method_exists('GFForms', 'include_addon_framework')) {
            return;
        }

        require_once 'includes/class-gf-raange-sms.php';

        GFAddOn::register('GFRaangeSMS');
    }

}

/**
 * Returns instance of the GFRaangeSMS class.
 *
 * @since  [since]
 *
 * @return [type]  [description]
 */
function gf_raange_sms() {
    return GFRaangeSMS::get_instance();
}
