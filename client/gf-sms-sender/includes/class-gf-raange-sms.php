<?php

GFForms::include_addon_framework();

class GFRaangeSMS extends GFAddOn {

    protected $_version                  = GF_RAANGE_SMS_ADDON_VERSION;
    protected $_min_gravityforms_version = '1.9';
    protected $_slug                     = 'gf-raange-sms';
    protected $_path                     = 'gf-raange-sms/gf-raange-sms.php';
    protected $_full_path                = __FILE__;
    protected $_title                    = 'Raange SMS Add-on';
    protected $_short_title              = 'Raange SMS';

    private static $_instance = null;

    /**
     * Logger class instance.
     *
     * @var [type]
     */
    public $logs;

    /**
     * Get an instance of this class.
     *
     * @return GFRaangeSMS
     */
    public static function get_instance() {
        if (self::$_instance == null) {
            self::$_instance = new GFRaangeSMS();
        }

        return self::$_instance;
    }

    /**
     * Handles hooks and loading of files.
     */
    public function init() {
        parent::init();

        // Plugin dir path
        if (!defined('GFRS_PLUGIN_DIR')) {
            define('GFRS_PLUGIN_DIR', dirname(plugin_dir_path(__FILE__)));
        }

        // Plugin folder url
        if (!defined('GFRS_PLUGIN_URL')) {
            define('GFRS_PLUGIN_URL', plugin_dir_url(__FILE__));
        }

        $this->includes();
        $this->logs = new GF_Raange_Logging;

        add_action('admin_init', array($this, 'submit_debug_log'));
        add_action('gform_enqueue_scripts', array($this, 'frontend_assets'), 10, 2);
        add_filter('gform_validation', array($this, 'check_number'));
    }

    /**
     * Includes files
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function includes() {
        include trailingslashit(GFRS_PLUGIN_DIR) . 'includes/class-gf-raange-logging.php';
    }

    /**
     * Returns true if site_url() contains .dev, .text, or staging in the url.
     *
     * @since  1.0.0
     *
     * @return boolean [description]
     */
    public function is_dev() {
        if (false !== strpos(site_url(), '.dev') || false !== strpos(site_url(), '.test') || false !== strpos(site_url(), 'staging')) {
            return true;
        }

        return false;
    }

    /**
     * Returns empty string for local or staging server, otherwise returns '.min'.
     *
     * @return string
     * @since  04102018
     */
    public function min_string() {
        if ($this->is_dev()) {
            return '';
        }

        return '.min';
    }

    /**
     * Enqueues front-end scripts and styles.
     *
     * @since  1.0.0
     *
     * @param  [type]  $form    [description]
     * @param  [type]  $is_ajax [description]
     *
     * @return [type]           [description]
     */
    public function frontend_assets($form, $is_ajax) {
        $min = $this->min_string();

        $localized_data             = $this->get_localized_data($form);
        $localized_data['settings'] = $this->gfrs_settings($form);

        wp_register_script('gf-raange-sms-js', plugins_url('/assets/js/main' . $min . '.js', dirname(__FILE__)), array('jquery'), '');

        // wp_localize_script('gf-raange-sms-js', 'gfrs', $localized_data);

        wp_enqueue_script('gf-raange-sms-js');

        wp_enqueue_style('gf-raange-sms-css', plugins_url('/assets/css/main' . $min . '.css', dirname(__FILE__)), array(), '');
    }

    /**
     * Generates a log entry.
     *
     * @since  1.0.0
     *
     * @return void
     */
    public function log($message = '') {
        if (empty($message)) {
            return false;
        }

        $this->logs->log($message);
    }

    /**
     * Creates a custom page for this add-on.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function plugin_page() {
        ?>
        <div class="metabox-holder">
            <div class="postbox">
                <h3><span><?php esc_html_e('Debug Log', 'gf-raange-sms');?></span></h3>
                <div class="inside">
                    <form id="raange-debug-log" method="post">
                        <p><?php esc_html_e('Activity logs for Raange SMS', 'gf-raange-sms');?></p>
                        <textarea readonly="readonly" onclick="this.focus(); this.select()" class="large-text" rows="15" name="raange-debug-log-contents">
                            <?php echo esc_textarea($this->logs->get_log()); ?>
                        </textarea>
                        <p class="submit">
                            <input type="hidden" name="raange_action" value="submit_debug_log" />
                            <?php
submit_button(__('Download Log File', 'gf-raange-sms'), 'primary', 'raange-download-debug-log', false);
        submit_button(__('Clear Log', 'gf-raange-sms'), 'secondary raange-inline-button', 'raange-clear-debug-log', false);
        ?>
                        </p>
                        <?php wp_nonce_field('raange-debug-log-action');?>
                    </form>
                </div><!-- .inside -->
            </div><!-- .postbox -->
        </div><!-- .metabox-holder -->
        <style>
        .raange-inline-button {
            margin-left: .5rem !important;
        }
        </style>
        <?php
}

    /**
     * Handles submit actions for the debug log.
     *
     * @since 1.0.0
     */
    public function submit_debug_log() {
        if (!current_user_can('manage_options')) {
            return;
        }

        if (defined('DOING_AJAX') && DOING_AJAX) {
            return;
        }

        if (isset($_REQUEST['raange-download-debug-log'])) {
            nocache_headers();

            header('Content-Type: text/plain');
            header('Content-Disposition: attachment; filename="raange-debug-log.txt"');

            echo wp_strip_all_tags($_REQUEST['raange-debug-log-contents']);
            exit;

        } elseif (isset($_REQUEST['raange-clear-debug-log'])) {

            // Clear the debug log.
            $this->logs->clear_log();

            wp_safe_redirect(admin_url('admin.php?page=gf-raange-sms'));
            exit;

        }
    }

    /**
     * Gets global add-on settings.
     *
     * @since  1.0.0
     *
     * @return [array]  Array of settings
     */
    public function get_addon_settings_values() {
        $settings = maybe_unserialize(get_option('gravityformsaddon_gf-raange-sms_settings'));
        return (array) $settings;
    }

    /**
     * Configures the settings which should be rendered on the main add-on settings tab.
     *
     * @return array
     * @since 1.0.0
     */
    public function plugin_settings_fields() {
        return array(
            array(
                'title'  => esc_html__('These add-on settings are used globally as a fallback for all Raange SMS forms. If no value for a field is found in a form, the matching global setting below will be used in place.', 'gf-raange-sms'),
                'fields' => array(
                    array(
                        'name'              => 'global_raange_auth_url',
                        'tooltip'           => esc_html__('Enter the Raange API Auth URL to be used globally. This Raange API URL will be used for all forms on the site which do not have a custom Raange API Auth URL already specified.', 'gf-raange-sms'),
                        'label'             => esc_html__('Global Raange Auth URL', 'gf-raange-sms'),
                        'type'              => 'text',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                    array(
                        'name'              => 'global_raange_sms_url',
                        'tooltip'           => esc_html__('Enter the Raange API SMS URL to be used globally. This Raange API URL will be used for all forms on the site which do not have a custom Raange API SMS URL already specified.', 'gf-raange-sms'),
                        'label'             => esc_html__('Global Raange SMS URL', 'gf-raange-sms'),
                        'type'              => 'text',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                    array(
                        'name'              => 'global_raange_account_id',
                        'tooltip'           => esc_html__('Enter the Raange account ID to be used globally here. This Raange account ID will be used for all forms on the site which do not have a custom Raange account ID already specified.', 'gf-raange-sms'),
                        'label'             => esc_html__('Global Raange Account ID', 'gf-raange-sms'),
                        'type'              => 'text',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                    array(
                        'name'              => 'global_raange_secret_key',
                        'tooltip'           => esc_html__('Place the global Raange secret key here. This key will be used for all forms on the site which do not have a custom Raange secret key already specified.', 'gf-raange-sms'),
                        'label'             => esc_html__('Global Raange Secret Key', 'gf-raange-sms'),
                        'type'              => 'global_raange_secret_key',
                        'class'             => 'medium gaddon-setting gaddon-text raange-secret-key',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                    array(
                        'name'              => 'global_raange_from',
                        'tooltip'           => esc_html__('Enter the global Raange number to use as the SMS From sender. This number will be used for all forms on the site which do not have a custom From number already specified.', 'gf-raange-sms'),
                        'label'             => esc_html__('Global Raange From Number', 'gf-raange-sms'),
                        'type'              => 'text',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                    array(
                        'name'              => 'global_raange_debug_log_Link',
                        'type'              => 'raange_debug_log_link',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                ),
            ),
        );
    }

    public function secret_key_markup() {?>
        <script language="javascript">
        jQuery(document).ready(function($) {

            $('input.secret-key').after('<span title="<?php echo __('Show or hide Raange secret key', 'gf-raange-sms'); ?>" class="fa fa-eye secret-key-toggle"></span>');

            $('.secret-key-toggle').css('margin-left','.5rem');
            $('.secret-key-toggle').css('cursor','pointer');

            if ($('input.secret-key').val().length > 0) {
                $('input.secret-key').attr('type', 'password');
            }

            $('.secret-key-toggle').click(function(event) {
                if ($(this).hasClass('hide')) {
                    $('input.secret-key').attr('type', 'text');
                    $(this).removeClass('hide');
                    $('.secret-key-toggle').removeClass('fa-eye');
                    $('.secret-key-toggle').addClass('fa-eye-slash');
                } else {
                    $('input.secret-key').attr('type', 'password');
                    $(this).addClass('hide');
                    $('.secret-key-toggle').removeClass('fa-eye-slash');
                    $('.secret-key-toggle').addClass('fa-eye');
                }
            });
        });
        </script>

    <?php }

    /**
     * Returns the add-on debug log link.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function settings_raange_debug_log_link() {
        echo '<hr /><a class="button secondary" href="' . admin_url('admin.php?page=gf-raange-sms') . '">' . __('View SMS Activity Logs', 'gf-raange-sms') . '</a>';
    }

    /**
     * Returns the global raange_secret_key field markup.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function settings_global_raange_secret_key() {
        $this->settings_text(
            array(
                'name'  => 'global_raange_secret_key',
                'class' => 'medium secret-key',
            )
        );

        $this->secret_key_markup();
    }

    /**
     * Returns the raange_secret_key field markup.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function settings_raange_secret_key() {
        $this->settings_text(
            array(
                'name'  => 'raange_secret_key',
                'class' => 'medium secret-key',
            )
        );

        $this->secret_key_markup();
    }

    /**
     * Configures the settings which should be rendered
     * on the Form Settings > Raange SMS Add-On tab.
     *
     * @return array
     */
    public function form_settings_fields($form) {
        $admin_link = admin_url('page=gf_settings&subview=gf-raange-sms');

        return array(
            array(
                'title'  => esc_html__('Raange SMS Form Settings', 'gf-raange-sms'),
                'fields' => array(
                    array(
                        'label'   => esc_html__('Enable Add-On?', 'gf-raange-sms'),
                        'type'    => 'checkbox',
                        'name'    => 'raange_is_enabled',
                        'tooltip' => esc_html__('Check this option to enable the <em>Raange SMS add-on</em> for this form.', 'gf-raange-sms'),
                        'choices' => array(
                            array(
                                'label' => esc_html__('Enable add-on for this form', 'gf-raange-sms'),
                                'name'  => 'raange_is_enabled',
                            ),
                        ),
                    ),
                    array(
                        'label'   => esc_html__('Disable logging?', 'gf-raange-sms'),
                        'type'    => 'checkbox',
                        'name'    => 'raange_logging_enabled',
                        'tooltip' => esc_html__('Logging is enabled for all enabled forms by default. To deactivate logging for this form, check this option.', 'gf-raange-sms'),
                        'choices' => array(
                            array(
                                'label' => esc_html__('Disable logging for this form', 'gf-raange-sms'),
                                'name'  => 'raange_logging_enabled',
                            ),
                        ),
                    ),
                    array(
                        'label'   => esc_html__('Strict validation mode', 'gf-raange-sms'),
                        'type'    => 'checkbox',
                        'name'    => 'raange_invalid_number_halt',
                        'tooltip' => esc_html__('Check this option to prevent form submission if the provided phone number fails verification.', 'gf-raange-sms'),
                        'choices' => array(
                            array(
                                'label' => esc_html__('Check this option to prevent form submission if the provided phone number fails verification.', 'gf-raange-sms'),
                                'name'  => 'raange_invalid_number_halt',
                            ),
                        ),
                    ),
                    array(
                        'name'    => 'raange_auth_url',
                        'tooltip' => esc_html__('Enter the Raange API Auth URL here. This API Auth URL will be used for all form submissions for this form. If no API Auth URL is set in this field, the global Raange API Auth URL will be used.', 'gf-raange-sms'),
                        'label'   => esc_html__('Raange Auth URL', 'gf-raange-sms'),
                        'type'    => 'text',
                        'class'   => 'medium',
                    ),
                    array(
                        'name'    => 'raange_sms_url',
                        'tooltip' => esc_html__('Enter the Raange API SMS URL here. This API SMS URL will be used for all form submissions for this form. If no API SMS URL is set in this field, the global Raange API SMS URL will be used.', 'gf-raange-sms'),
                        'label'   => esc_html__('Raange SMS URL', 'gf-raange-sms'),
                        'type'    => 'text',
                        'class'   => 'medium',
                    ),
                    array(
                        'name'    => 'raange_account_id',
                        'tooltip' => esc_html__('Enter the Raange Account ID here. This account ID will be used for all form submissions for this form. If no account ID is set in this field, the global Raange Account ID will be used.', 'gf-raange-sms'),
                        'label'   => esc_html__('Raange Account ID', 'gf-raange-sms'),
                        'type'    => 'text',
                        'class'   => 'medium',
                    ),
                    array(
                        'name'    => 'raange_secret_key',
                        'tooltip' => esc_html__('Enter Raange Secret Key here. This key will be used for all form submissions for this form. If no secret key is set in this field, the global Raange Secret Key will be used.', 'gf-raange-sms'),
                        'label'   => esc_html__('Raange Secret Key', 'gf-raange-sms'),
                        'type'    => 'raange_secret_key',
                        'class'   => 'medium',
                    ),
                    array(
                        'name'    => 'raange_from',
                        'tooltip' => esc_html__('Enter the number to use as the SMS sender/From number. This number will be used for all form submissions for this form. If no sender/From number is set in this field, the global Raange From number will be used.', 'gf-raange-sms'),
                        'label'   => esc_html__('Sender/From Number', 'gf-raange-sms'),
                        'type'    => 'text',
                        'class'   => 'medium',
                    ),
                    array(
                        'name'              => 'sms_message',
                        'tooltip'           => esc_html__('Enter the SMS message to send after number validation. This SMS message will be used for all form submissions for this form.', 'gf-raange-sms'),
                        'label'             => esc_html__('SMS message', 'gf-raange-sms'),
                        'type'              => 'textarea',
                        'class'             => 'medium merge-tag-support mt-position-right',
                        'feedback_callback' => array($this, 'sms_char_limit'),
                    ),
                    array(
                        'name'     => 'raange_phone_field',
                        'label'    => esc_html__('Phone Number Field', 'gf-raange-sms'),
                        'type'     => 'field_select',
                        'required' => true,
                        'tooltip'  => '<h6>' . esc_html__('Phone Number Field', 'gf-raange-sms') . '</h6>' . esc_html__('Select which form field will be used as the phone number the Raange API should validate and send SMS.', 'gf-raange-sms'),
                        'args'     => array(
                            'input_types' => array('phone'),
                        ),
                    ),
                    array(
                        'name'              => 'raange_form_debug_log_Link',
                        'type'              => 'raange_debug_log_link',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                ),
            ),
        );
    }

    /**
     * Ensures no settings are left empty.
     *
     * @since  1.0.0
     *
     * @param  [type]  $form [description]
     *
     * @return [type]        [description]
     */
    public function gfrs_settings($form) {

        if (!$form) {
            $form = $current_form;
        }

        $settings        = array();
        $global_settings = $this->get_addon_settings_values();
        $form_settings   = $form['gf-raange-sms'];

        $global_auth_url    = !empty($global_settings['global_raange_auth_url']) ? $global_settings['global_raange_auth_url'] : '';
        $global_sms_url     = !empty($global_settings['global_raange_sms_url']) ? $global_settings['global_raange_sms_url'] : '';
        $global_account_id  = !empty($global_settings['global_raange_account_id']) ? $global_settings['global_raange_account_id'] : '';
        $global_secret_key  = !empty($global_settings['global_raange_secret_key']) ? $global_settings['global_raange_secret_key'] : '';
        $global_raange_from = !empty($global_settings['global_raange_from']) ? $global_settings['global_raange_from'] : '';

        $settings['raange_is_enabled']      = !empty($form_settings['raange_is_enabled']) ? $form_settings['raange_is_enabled'] : false;
        $settings['raange_logging_enabled'] = !empty($form_settings['raange_logging_enabled']) ? $form_settings['raange_logging_enabled'] : false;
        $settings['raange_auth_url']        = !empty($form_settings['raange_auth_url']) ? $form_settings['raange_auth_url'] : $global_auth_url;
        $settings['raange_sms_url']         = !empty($form_settings['raange_sms_url']) ? $form_settings['raange_sms_url'] : $global_sms_url;
        $settings['raange_account_id']      = !empty($form_settings['raange_account_id']) ? $form_settings['raange_account_id'] : $global_account_id;

        $settings['raange_secret_key'] = !empty($form_settings['raange_secret_key']) ? $form_settings['raange_secret_key'] : $global_secret_key;

        $settings['raange_from']        = !empty($form_settings['raange_from']) ? $form_settings['raange_from'] : $global_raange_from;
        $settings['raange_phone_field'] = !empty($form_settings['raange_phone_field']) ? $form_settings['raange_phone_field'] : '';
        $settings['sms_message']        = !empty($form_settings['sms_message']) ? $form_settings['sms_message'] : '';

        return $settings;
    }

    /**
     * Retrieves a json web token from the Raange API authorization endpoint.
     *
     * @since  1.0.0
     *
     * @param  [type]  $auth_url  [description]
     * @param  [type]  $accountId [description]
     * @param  [type]  $secret    [description]
     *
     * @return [type]             [description]
     */
    public function get_token($auth_url, $accountId, $secret, $logging_enabled) {

        $token = '';

        if (empty($auth_url) || empty($accountId) || empty($secret)) {
            return false;
        }

        $url = $auth_url . '?accountId=' . $accountId . '&secret=' . $secret;

        $args = array(
            'method'      => 'POST',
            'timeout'     => 120,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking'    => true,
            'headers'     => array('Content-Type' => 'application/json'),
            'body'        => json_encode(
                array(
                    'accountId' => $accountId,
                    'secret'    => $secret,
                )
            ),
            'cookies'     => array(),
        );

        if ($logging_enabled) {
            $this->log(__('Authenticating with Raange API endpoint ', 'gf-raange-sms') . $auth_url . '...');
        }

        $response = wp_remote_post($url, $args);
        $body     = (array) json_decode(wp_remote_retrieve_body($response));

        $token = isset($body['jwt']) ? $body['jwt'] : $token;

        if (empty($token)) {
            $error_code = isset($body['status_code']) ? 'Error: ' . $body['status_code'] : '';
            if ($logging_enabled) {
                $this->logs->log(__('Could not authenticate with Raange ', 'gf-raange-sms') . $error_code);
            }

            return false;
        }

        if ($logging_enabled) {
            $this->log(__('Received jwt successfully. Token: ', 'gf-raange-sms') . $token);
        }
        return $token;
    }

    /**
     * Validates provided phone number using the Raange API.
     *
     * Test number for phone field: 514-803-7958
     *
     * Test api url: https://api.raange.tech/auth/v1/_auth
     * Live api url: https://api.raange.tech/sms/v1/_send
     * accountId:    BJmlApjMgjPahmJk-_t6YCeZcSePexJj
     * Pass:         vK67iCzTPs7LcY-fXwaSNcXZetyLotcP7YM1Vu76X00i-RLo
     * From:         30303
     * @since  1.0.0
     *
     * @param  [type]  $validation_result [description]
     *
     * @return [type]          [description]
     */
    public function check_number($validation_result) {

        $form     = $validation_result['form'];
        $entry    = GFFormsModel::get_current_lead();
        $settings = $this->gfrs_settings($form);

        $enabled         = isset($settings['raange_is_enabled']) ? $settings['raange_is_enabled'] : false;
        $logging_enabled = isset($settings['raange_logging_enabled']) ? $settings['raange_logging_enabled'] : false;

        // $this->log('check_number::settings: ' . print_r($settings, true));

        // Bail if not enabled.
        if (!$enabled) {
            if ($logging_enabled) {
                $this->log('Form not enabled for SMS; bailing check method');
            }

            $validation_result['is_valid'] = true;
            return $validation_result;
        }

        // Allow submission if phone field not set
        if (!isset($settings['raange_phone_field'])) {
            if ($logging_enabled) {
                $this->log(__('API validation bypassed: Raange SMS is enabled, but a phone field is not specified for this form. Please specify a phone field to use for validation in submissions.', 'gf-raange-sms'));
            }
            $validation_result['is_valid'] = true;
            return $validation_result;
        } else {
            if (false === $settings['raange_phone_field'] || empty($settings['raange_phone_field'])) {
                if ($logging_enabled) {
                    $this->log(__('API validation bypassed: Raange SMS is enabled, but a phone field is not specified for this form. Please specify a phone field to use for validation in submissions.', 'gf-raange-sms'));
                }
                $validation_result['is_valid'] = true;
                return $validation_result;
            }
        }

        if ($logging_enabled) {
            $this->log(__('Validating provided telephone number for form ID: ', 'gf-raange-sms') . $form['id'] . ', entry ID: ' . $entry['id']);
        }

        $phone_field_id = isset($settings['raange_phone_field']) ? $settings['raange_phone_field'] : false;

        // Gets actual phone number
        $phone = $entry[$phone_field_id];
        // Strips non-numeric characters
        $phone = preg_replace('/\D/', '', $phone);

        $auth_url  = $settings['raange_auth_url'];
        $sms_url   = $settings['raange_sms_url'];
        $accountID = $settings['raange_account_id'];
        $secret    = $settings['raange_secret_key'];
        $from      = $settings['raange_from'];

        $clientMessageId = '';

        // Authenticates and gets json web token.
        $token = $this->get_token($auth_url, $accountID, $secret, $logging_enabled);

        if (!$token) {
            if ($logging_enabled) {
                $this->log(__('Unable to proceed with validation - token authentication failed.', 'gf-raange-sms'));
            }
        }

        // Send SMS
        if ($logging_enabled) {
            $this->log('Entry ' . $entry['id'] . ': ' . __('Sending SMS data via POST request to Raange endpoint: ', 'gf-raange-sms') . $sms_url);
        }

        $args = array(
            'method'      => 'POST',
            'timeout'     => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking'    => true,
            'headers'     => array(
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ),
            'body'        => json_encode(
                array(
                    'from' => $from,
                    'to'   => $phone,
                    'body' => $settings['sms_message'],
                    // 'clientMessageId' => $clientMessageId,
                )
            ),
            'cookies'     => array(),
        );

        $response = wp_remote_post($sms_url, $args);

        // error_log('entire response: ' . print_r($response, true));

        $code = $response['response']['code'];
        $body = (array) json_decode(wp_remote_retrieve_body($response));

        if (!isset($body['utid'])) {
            $error_code = isset($body['status_code']) ? 'Error: ' . $body['status_code'] : '';

            if ($logging_enabled) {
                $this->log('Entry ' . $entry['id'] . ': ' . __('Could not send SMS. ', 'gf-raange-sms') . $error_code);
            }

            if (isset($code)) {
                if (400 === $code) {
                    if ($logging_enabled) {
                        $this->log(__('Phone number verification failed.', 'gf-raange-sms'));
                    }

                    if (isset($settings['raange_invalid_number_halt'])) {
                        error_log('raange_invalid_number_halt Setting is set. Value:');
                        error_log($settings['raange_invalid_number_halt']);
                        $validation_result['is_valid'] = true;
                    } else {
                        $validation_result['is_valid'] = false;

                        // Number failed validation
                        foreach ($form['fields'] as &$field) {

                            if ($field->id == $phone_field_id) {
                                $field->failed_validation  = true;
                                $field->validation_message = __('Please provide a valid telephone number', 'gf-raange-sms');
                                break;
                            }
                        }
                    }
                }

            }

        } else {
            $validation_result['is_valid'] = true;

            if (200 === $code) {
                if ($logging_enabled) {
                    $this->log(__('Successfully sent SMS message.', 'gf-raange-sms') . ' utid: ' . $body['utid'] . ' transactionDateTime: ' . $body['transactionDateTime']);
                }
            }
        }

        $validation_result['form'] = $form;

        return $validation_result;
    }

    /**
     * Makes form data available to plugin js.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_localized_data($form) {
        $settings = $this->gfrs_settings($form);

        return array(
            'auth_url'  => $settings['raange_auth_url'],
            'sms_url'   => $settings['raange_sms_url'],
            'ajax_url'  => admin_url('admin-ajax.php'),
            'accountId' => $settings['raange_account_id'],
            'secret'    => $settings['raange_secret_key'],
            'from'      => $settings['raange_from'],
        );
    }

    /**
     * Settings field validation
     *
     * @param string $value The setting value.
     *
     * @return bool
     */
    public function is_valid_setting($value) {
        return strlen($value) < 100;
    }

    /**
     * Settings field validation
     *
     * @param string $value The setting value.
     *
     * @return bool
     */
    public function sms_char_limit($value) {
        return strlen($value) < 160;
    }
}
