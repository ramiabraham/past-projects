<?php
/*
Plugin Name: Maintainn Expirator
Version: 0.1
Description: Expire Maintainn Notifications and change their status to 'draft'.
Author: Maintainn
Author URI: http://maintainn.com/
Plugin URI: htto://maintainn.com/
Text Domain: maintainn-expirator
Domain Path: /languages
*/

class Maintainn_Expirator {

	public static function instance() {
		static $instance = null;

		if ( null === $instance ) {
			$instance = new Maintainn_Expirator;
			$instance->setup_action();
		}
	}

	private function __construct() {}

	private function setup_action() {
		add_action( 'post_submitbox_misc_actions', array( $this, 'add_expiration_field' ) );
		add_action( 'save_post', array( $this, 'save_expiration' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	public function add_expiration_field() {
		global $post;

		if ( ! empty( $post->ID ) ) {
			$expires = get_post_meta( $post->ID, 'mne_expiration', true );
		}

		$label = ! empty( $expires ) ? date_i18n( 'Y-n-d', strtotime( $expires ) ) : __( 'never', 'pw-spe' );
		$date  = ! empty( $expires ) ? date_i18n( 'Y-n-d', strtotime( $expires ) ) : '';

		?>
		<div id=="mne-expiration-wrap" class="misc-pub-section">
			<span>
				<span class="wp-media-buttons-icon dashicons dashicons-calendar"></span>&nbsp;
				<?php _e( 'Expires:', 'maintainn-expirator' ); ?>
				<b id="mne-expiration-label"><?php echo $label; ?></b>
			</span>
			<a href="#" id="mne-edit-expiration" class="mne-edit-expiration hide-if-no-js">
				<span aria-hidden="true"><?php _e( 'Edit', 'pw-spe' ); ?></span>&nbsp;
				<span class="screen-reader-text"><?php _e( 'Edit date and time', 'pw-spe' ); ?></span>
			</a>
			<div id="mne-expiration-field" class="hide-if-js">
				<p>
					<input type="text" name="mne-expiration" id="mne-expiration" value="<?php echo esc_attr( $date ); ?>" placeholder="yyyy-mm-dd"/>
				</p>
				<p>
					<a href="#" class="mne-hide-expiration button secondary"><?php _e( 'OK', 'pw-spe' ); ?></a>
					<a href="#" class="mne-hide-expiration cancel"><?php _e( 'Cancel', 'pw-spe' ); ?></a>
				</p>
			</div>
			<?php wp_nonce_field( 'maintainn_expirator', 'maintainn_expirator_nonce' ); ?>
		</div>
		<?php
	}

	public function save_expiration( $post_id ) {
		if ( ! isset( $_POST['maintainn_expirator_nonce'] ) ) {
			return;
		}

		if ( ! wp_verify_nonce( $_POST['maintainn_expirator_nonce'], 'maintainn_expirator' ) ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$expiration = ! empty( $_POST['mne-expiration'] ) ? sanitize_text_field( $_POST['mne-expiration'] ) : false;

		if ( $expiration ) {
			update_post_meta( $post_id, 'mne_expiration', $expiration );
		} else {
			delete_post_meta( $post_id, 'mne_expiration' );
		}

		wp_clear_scheduled_hook( 'maintainn_expire_post', array( $post_id ) );
		wp_schedule_single_event( strtotime( get_gmt_from_date( $expiration ) . ' GMT' ), 'maintainn_expire_post', array( $post_id ) );
	}

	public function enqueue_scripts( $hook ) {
		if ( ! in_array( $hook, array( 'post-new.php', 'post.php' ) ) ) {
			return;
		}

		wp_enqueue_style( 'jquery-ui-css', plugin_dir_url( __FILE__ ) . 'assets/css/jquery-ui-fresh.min.css' );
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_script( 'jquery-ui-slider' );
		wp_enqueue_script( 'maintainn-expirator', plugin_dir_url( __FILE__ ) . 'assets/js/edit.js' );
	}
}

add_action( 'plugins_loaded', array( 'Maintainn_Expirator', 'instance' ) );

function maintainn_expire_post( $post_id ) {
	$post = get_post( $post_id );

	if ( empty( $post ) ) {
		return;
	}

	if ( 'draft' == $post->post_status ) {
		return;
	}

	$expiration = get_post_meta( $post->ID, 'mne_expiration', true );

	if ( empty( $expiration ) ) {
		return;
	}

	$time = strtotime( get_gmt_from_date( $expiration ) . ' GMT' );

	if ( $time > time() ) {
		wp_clear_scheduled_hook( 'maintainn_expire_post', array( $post_id ) ); // clear anything else in the system
		wp_schedule_single_event( $time, 'maintainn_expire_post', array( $post_id ) );
		return;
	}

	$post->post_status = 'draft';

	return wp_update_post( $post );
}