jQuery(document).ready( function( $ ) {
	var previous = $('#mne-expiration').val();

	$('#mne-expiration').datepicker({
		dateFormat: 'yy-mm-dd'
	});

	$('#mne-edit-expiration, .mne-hide-expiration').click( function(e) {
		e.preventDefault();

		var date = $('#mne-expiration').val();

		if ( $(this).hasClass('cancel' ) ) {
			$('#mne-expiration').val( previous );
		
		} else if ( date ) {
			$('#mne-expiration-label').text( $('#mne-expiration').val() );
		}

		$('#mne-expiration-field').slideToggle();
	});
});