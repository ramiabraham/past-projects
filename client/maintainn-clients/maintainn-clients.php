<?php
/**
 * Plugin Name: Maintainn Clients
 * Plugin URI: http://webdevstudios.com
 * Description: Sets up Clients CPT and other related functionality.
 * Version: 1.0.0
 * Author: WebDevStudios
 * Author URI: http://webdevstudios.com
 * License: GPL2
 */


/**
 * Register Clients Post Type
 */
function maintainn_clients_post_type() {
	$labels = array(
		'name'               => _x( 'Clients', 'post type general name', 'maintainn' ),
		'singular_name'      => _x( 'Client', 'post type singular name', 'maintainn' ),
		'add_new'            => _x( 'Add New Client', 'clients', 'maintainn' ),
		'add_new_item'       => __( 'Add New Client', 'maintainn' ),
		'edit_item'          => __( 'Edit Client', 'maintainn' ),
		'edit'               => _x( 'Edit Clients', 'clients', 'maintainn' ),
		'new_item'           => __( 'New Cleint', 'maintainn' ),
		'view_item'          => __( 'View Client', 'maintainn' ),
		'search_items'       => __( 'Search Clients', 'maintainn' ),
		'not_found'          => __( 'No Clients found', 'maintainn' ),
		'not_found_in_trash' => __( 'No Clients found in Trash', 'maintainn' ),
		'view'               => __( 'View Client', 'maintainn' ),
		'parent_item_colon'  => ''
	);
	$args = array(
		'labels'             => $labels,
		'public'             => false,
		'has_archive'        => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'query_var'          => true,
		'rewrite'            => false,
		'capability_type'    => 'post',
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'          => plugins_url( dirname( plugin_basename( __FILE__ ) ) ) . '/images/maintainn_icon.png',
		'supports'           => array( 'title', 'thumbnail', 'page-attributes' )
	);

	register_post_type( 'maintainn_clients', $args);
}

add_action( 'init', 'maintainn_clients_post_type', 1 );

/**
 * Register CMB2 Metaboxes
 */
function maintainn_clients_metaboxes() {
	$prefix = 'maintainn_clients_';

	/**
	 * Metabox for the user profile screen
	 */
	$cmb = new_cmb2_box( array(
		'id'               => $prefix . 'edit',
		'title'            => __( 'Client Info', 'maintainn' ),
		'object_types'     => array( 'maintainn_clients' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'       => true,
	) );

	$cmb->add_field( array(
		'name' => __( 'URL', 'cmb2' ),
		'desc' => __( 'URL to link logo to. (optional)', 'cmb2' ),
		'id'   => $prefix . 'url',
		'type' => 'text_url',
	) );

}
add_action( 'cmb2_init', 'maintainn_clients_metaboxes' );

/**
 * Add metabox to Site Options
 */
function maintainn_clients_home_page_metabox() {

	$cmb = cmb2_get_metabox( 'home-page-options-metabox' );
	$cmb->add_field( array(
		'name' => __( 'Clients Section', 'maintainn' ),
		'desc' => __( 'Configure the clients section of the home page.', 'maintainn' ),
		'id'   => 'maintainn_home_page_clients',
		'type' => 'title',
	) );
	$cmb->add_field( array(
		'name' => __( 'Home Page Clients Title', 'maintainn' ),
		'id'   => 'maintainn_home_clients_title',
		'type' => 'text',
	) );

}
add_action( 'cmb2_init', 'maintainn_clients_home_page_metabox', 20 );

/**
 * Add custom featured image size
 */
function maintainn_client_featured_image() {
	add_image_size( 'client-logo', 120, 30 );
}
add_action( 'after_setup_theme', 'maintainn_client_featured_image' );


/**
 * Move the featured image metabox under the title by default.
 */
function maintainn_move_featured_image_metabox() {

	remove_meta_box( 'postimagediv', 'maintainn_clients', 'side' );

	add_meta_box('postimagediv', __('Client Logo'), 'post_thumbnail_meta_box', 'maintainn_clients', 'normal', 'high');

}
add_action( 'do_meta_boxes', 'maintainn_move_featured_image_metabox' );

/**
 * Change the title in the Enter Title box.
 */
function maintainn_change_title( $title, $post ){

	if  ( 'maintainn_clients' == $post->post_type ) {
		$title = 'Enter Client Name';
	}

	return $title;
}
add_filter( 'enter_title_here', 'maintainn_change_title', 10, 2 );

/**
 * Helper function to display Section title
 */
function maintainn_clients_section_title() {

	$option = get_option( 'maintainn-home-page-options' );
	if ( ! isset( $option['maintainn_home_clients_title'] ) ) {
		return;
	}
	echo esc_html( $option['maintainn_home_clients_title'] );

}

/**
 * Loop through and return marked-up code with clients CPT
 */
function maintainn_display_clients() {

	$html = '';
	$args = array(
		'post_type'      => 'maintainn_clients',
		'posts_per_page' => -1,
		'orderby'        => 'menu_order',
		'order'          => 'ASC'
	);
	$clients = new WP_Query( $args );

	if ( $clients->have_posts() ):

		$html .= '<div class="clients">';

		while( $clients->have_posts() ): $clients->the_post();

			$html .= '<div class="client ' . esc_attr( sanitize_title_with_dashes( get_the_title() ) ) . '">';
			$link = get_post_meta( get_the_ID(), 'maintainn_clients_url', true );
				$html .= $link ? '<a href="' . esc_url( $link ) . '">' : '';
					$html .= get_the_post_thumbnail( get_the_ID(), 'medium', 'client-logo' );
				$html .= $link ? '</a>' : '';
			$html .= '</div>';

		endwhile;

		$html .= '</div>';

		wp_reset_postdata();

	endif;

	return $html;
}

/**
 * Echo and display clients CPT code
 */
function maintainn_clients_loop() {

	echo maintainn_display_clients();

}