<?php
/**
 * Plugin Name: Maintainn FAQ
 * Plugin URI: http://webdevstudios.com
 * Description: Sets up FAQ CPT and other related functionality.
 * Version: 1.0.0
 * Author: WebDevStudios
 * Author URI: http://webdevstudios.com
 * License: GPL2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Maintainn_FAQ' ) ) {

	class Maintainn_FAQ {

		/**
		 * Construct function to get things started.
		 */
		public function __construct() {

			// Setup some base variables for the plugin
			$this->basename       = plugin_basename( __FILE__ );
			$this->directory_path = plugin_dir_path( __FILE__ );
			$this->directory_url  = plugins_url( dirname( $this->basename ) );

			// Activation/Deactivation Hooks
			register_activation_hook( __FILE__, array( $this, 'activate' ) );
			register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		}

		/**
		 * Register CPTs & taxonomies.
		 */
		public function do_hooks() {
			add_action( 'init', array( $this, 'register_post_types' ), 9 );
		}

		/**
		 * Activation hook for the plugin.
		 */
		public function activate() {

		}

		/**
		 * Deactivation hook for the plugin.
		 */
		public function deactivate() {

		}

		/**
		 * Register custom post-types.
		 */
		public function register_post_types() {

			$labels = array(
				'name'               => _x( 'FAQ', 'post type general name', 'maintainn' ),
				'singular_name'      => _x( 'FAQ', 'post type singular name', 'maintainn' ),
				'add_new'            => _x( 'Add New FAQ', 'FAQ', 'maintainn' ),
				'add_new_item'       => __( 'Add New FAQ', 'maintainn' ),
				'edit_item'          => __( 'Edit FAQ', 'maintainn' ),
				'edit'               => _x( 'Edit FAQ', 'FAQ', 'maintainn' ),
				'new_item'           => __( 'New FAQ', 'maintainn' ),
				'view_item'          => __( 'View FAQ', 'maintainn' ),
				'search_items'       => __( 'Search FAQ', 'maintainn' ),
				'not_found'          => __( 'No FAQ found', 'maintainn' ),
				'not_found_in_trash' => __( 'No FAQ found in Trash', 'maintainn' ),
				'view'               => __( 'View FAQ', 'maintainn' ),
				'parent_item_colon'  => ''
			);

			$args = array(
				'labels'             => $labels,
				'public'             => true,
				'has_archive'        => false,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'faq' ),
				'capability_type'    => 'post',
				'hierarchical'       => false,
				'menu_position'      => null,
				'menu_icon'          => 'dashicons-editor-help',
				'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
			);

			register_post_type( 'faq', $args);

		}

	}

	$GLOBALS['maintainn_faq'] = new Maintainn_FAQ;
	$GLOBALS['maintainn_faq']->do_hooks();

}