<?php
/**
 * Feature Group Order options page
 * @version 0.1.0
 */
class Maintainn_Features_Group_Order {

	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	private $key = 'maintainn_features_group_order';

	/**
 	 * Options page metabox id
 	 * @var string
 	 */
	private $metabox_id = 'maintainn_features_groups';

	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';

	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct() {
		// Set our title
		$this->title = __( 'Order Feature Sections', 'maintainn' );
	}

	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_init', array( $this, 'add_options_page_metabox' ) );
	}


	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$this->options_page = add_submenu_page( 'edit.php?post_type=features', $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );
	}

	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		?>
		<div class="wrap cmb2_options_page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
		</div>
		<?php
	}

	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	function add_options_page_metabox() {

		$cmb = new_cmb2_box( array(
			'id'      => $this->metabox_id,
			'hookup'  => false,
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );

		// Set our CMB2 fields

		$group_field_id = $cmb->add_field( array(
			'id'      => 'maintainn_feature_groups',
			'desc'    => __( 'Use the up and down arrow buttons to reorder groups of features.', 'maintainn' ),
			'type'    => 'group',
			'options' => array(
				'group_title'   => __( 'Feature Section {#}', 'maintainn' ),
				'add_button'    => __( 'Add another feature section', 'maintainn' ),
				'remove_button' => __( 'Remove feature section', 'maintainn' ),
				'sortable'      => true
			)
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name'       => __( 'Feature Section', 'maintainn' ),
			'id'         => 'feature_section',
			'type'       => 'select',
			'options'    => $this->feature_section_term_list(),
			'attributes' => $this->get_attributes()
		) );

	}

	/**
	 * Check if there's a term list, return a default option if there are no terms or return the term list
	 */
	public function feature_section_term_list() {
		if ( ! maintainn_get_features_section_list() ) {
			return array( 'none' => __( '- No Feature Sections Found -', 'maintainn' ) );
		}

		return maintainn_get_features_section_list();
	}

	/**
	 * Check if there's a term list, return an attributes array that makes a cmb option disabled if there
	 * are no terms
	 */
	public function get_attributes() {
		if ( ! maintainn_get_features_section_list() ) {
			return array( 'disabled' => 'disabled' );
		}

		return array();
	}

	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed          Field value or exception is thrown
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}

		throw new Exception( 'Invalid property: ' . $field );
	}

}

/**
 * Helper function to get/return the Maintainn_Features_Group_Order object
 * @since  0.1.0
 * @return Maintainn_Features_Group_Order object
 */
function maintainn_features_group_order() {
	static $object = null;
	if ( is_null( $object ) ) {
		$object = new Maintainn_Features_Group_Order();
		$object->hooks();
	}

	return $object;
}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function maintainn_features_get_option( $key = '' ) {
	return cmb2_get_option( maintainn_features_group_order()->key, $key );
}

// Get it started
maintainn_features_group_order();
