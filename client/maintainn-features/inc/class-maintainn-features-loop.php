<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Maintainn_Features_Loop' ) ) {

	class Maintainn_Features_Loop {

		/**
		 * Construct function to get things started.
		 */
		public function __construct() {

			$this->sections = $this->get_sections();
			$GLOBALS['maintainn_features']->sections = $this->sections;

		}

		/**
		 * Runs the query to get all features and sort them into sections.
		 *
		 * @return array An array of sections including all features in each.
		 */
		function query() {
			
			$option = get_option( 'maintainn_features_group_order' );
			$groups = $option['maintainn_feature_groups'];

			$sections = array();
			$terms    = array();
			foreach ( $groups as $key => $group ) {
				$terms[] = $group['feature_section'];
				$sections[ $group['feature_section'] ] = array();
			}

			$args = array(
				'post_type'      => 'features',
				'order'          => 'ASC',
				'orderby'        => 'menu_order',
				'no_found_rows'  => true,
				'posts_per_page' => 100,
				'tax_query'      => array(
					array(
						'taxonomy' => 'maintainn_features_section',
						'field'    => 'slug',
						'terms'    => $terms,
					),
				),
			);

			$feats = get_posts( $args );

			foreach ( $feats as $feat ) {
				$terms = get_the_terms( $feat->ID, 'maintainn_features_section' );
				foreach ( $terms as $key => $term ) {
					$sections[ $term->slug ][] = array(
						'title'   => esc_html( $feat->post_title ),
						'content' => wp_kses_post( $feat->post_content ),
						'icon'    => esc_html( get_post_meta( $feat->ID, 'cmb_feature-icon', true ) ),
						'image'   => get_post_thumbnail_id( $feat->ID ),
					);
				}
			}

			return $sections;

		}

		/**
		 * Returns the array of sections and features cached from query.
		 *
		 * @return array The features sections along with their features.
		 */
		public function get_sections() {

			if ( false === ( $sections = get_transient( 'maintainn_features_sections' ) ) ) {
				$sections = $this->query();
				set_transient( 'maintainn_features_section', $sections, 1 * DAY_IN_SECONDS );
			}

			return $sections;

		}

		/**
		 * Return an arry of features within a section.
		 *
		 * @param $section_slug Slug of section to get features.
		 *
		 * @return null|array Null if slug does not exist, otherwise features within section.
		 */
		public function get_section( $section_slug ) {

			if ( ! isset( $this->sections[$section_slug] ) ) {
				return null;
			}
			$section = $this->sections[$section_slug];

			return $section;

		}

		/**
		 * Gets the template part and displays it for a section.
		 *
		 * @param $section_slug Slug of section to display.
		 */
		public function display_section( $section_slug ) {

			set_query_var( 'section_slug', $section_slug );
			get_template_part( 'parts/features/feature', $section_slug );

		}

		/**
		 * Loops through all sections and gets their template parts to display.
		 */
		public function display_all_sections() {

			foreach ( $this->sections as $key => $section ) {
				$this->display_section( $key );
			}

		}

	}
}
