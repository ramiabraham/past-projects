<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Grab a list of feature section terms, return them in an array with the slug as the index and the name
 * as the value, e.g. 'my-group' => 'My Group'
 */
function maintainn_get_features_section_list() {
	$terms = get_terms( 'maintainn_features_section', array( 'hide_empty' => false ) );

	if ( ! $terms || is_wp_error( $terms ) ) {
		return false;
	}

	$term_list = array();

	foreach ( $terms as $term ) {
		$term_list[$term->slug] = $term->name;
	}

	return $term_list;
}

/**
 * Display a Features Section
 *
 * @param string $section_slug The slug of the section you want displayed.
 */
function maintainn_display_features_section( $section_slug ) {

	$loop = new Maintainn_Features_Loop();
	$loop->display_section( $section_slug );

}

/**
 * Display all of the features sections at once.
 */
function maintainn_display_all_features_sections() {

	$loop = new Maintainn_Features_Loop();
	$loop->display_all_sections();

}

/**
 * Get an array of features for a given section.
 *
 * @param string $section_slug The Slug of the features section to get features.
 *
 * @return bool|array If no slug is found returns false, otherwise retuns features for set slug.
 */
function maintainn_get_section_features( $section_slug = '' ) {

	$section_slug = get_query_var( 'section_slug', $section_slug );

	if ( ! $section_slug ) {
		return false;
	}

	$features = $GLOBALS['maintainn_features']->sections[$section_slug];

	return $features;

}