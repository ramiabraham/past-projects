<?php
/**
 * Plugin Name: Maintainn Features
 * Plugin URI: http://webdevstudios.com
 * Description: Sets up Features CPT and other related functionality.
 * Version: 1.0.0
 * Author: WebDevStudios
 * Author URI: http://webdevstudios.com
 * License: GPL2
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Maintainn_Features' ) ) {

	class Maintainn_Features {

		/**
		 * Construct function to get things started.
		 */
		public function __construct() {

			// Setup some base variables for the plugin
			$this->basename       = plugin_basename( __FILE__ );
			$this->directory_path = plugin_dir_path( __FILE__ );
			$this->directory_url  = plugins_url( dirname( $this->basename ) );

			// Include any required files
			add_action( 'init', array( $this, 'includes' ) );

			// Activation/Deactivation Hooks
			register_activation_hook( __FILE__, array( $this, 'activate' ) );
			register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		}

		/**
		 * Include our plugin dependencies.
		 */
		public function includes() {

			require_once( $this->directory_path . '/inc/helper-functions.php' );
			require_once( $this->directory_path . '/inc/options.php' );
			require_once( $this->directory_path . '/inc/class-maintainn-features-loop.php' );

		}

		/**
		 * Register CPTs & taxonomies.
		 */
		public function do_hooks() {

			add_action( 'init', array( $this, 'register_post_types' ), 9 );
			add_action( 'init', array( $this, 'register_taxonomies' ), 9 );
			add_action( 'cmb2_init', array( $this, 'register_cmb' ) );

		}

		/**
		 * Activation hook for the plugin.
		 */
		public function activate() {

		}

		/**
		 * Deactivation hook for the plugin.
		 */
		public function deactivate() {

		}

		/**
		 * Register custom post-types.
		 */
		public function register_post_types() {

			$labels = array(
				'name'               => _x( 'Features', 'post type general name', 'maintainn' ),
				'singular_name'      => _x( 'Features', 'post type singular name', 'maintainn' ),
				'add_new'            => _x( 'Add New Feature', 'features', 'maintainn' ),
				'add_new_item'       => __( 'Add New Feature', 'maintainn' ),
				'edit_item'          => __( 'Edit Feature', 'maintainn' ),
				'edit'               => _x( 'Edit Feature', 'features', 'maintainn' ),
				'new_item'           => __( 'New Feature', 'maintainn' ),
				'view_item'          => __( 'View Feature', 'maintainn' ),
				'search_items'       => __( 'Search Features', 'maintainn' ),
				'not_found'          => __( 'No Features found', 'maintainn' ),
				'not_found_in_trash' => __( 'No features found in Trash', 'maintainn' ),
				'view'               => __( 'View Feature', 'maintainn' ),
				'parent_item_colon'  => ''
			);
			$args = array(
				'labels'             => $labels,
				'public'             => true,
				'has_archive'        => false,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'feature' ),
				'capability_type'    => 'post',
				'hierarchical'       => false,
				'menu_position'      => null,
				'menu_icon'          => plugins_url( dirname( plugin_basename( __FILE__ ) ) ) . '/images/maintainn_icon.png',
				'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
			);

			register_post_type( 'features', $args);

		}

		/**
		 * Register custom taxonomies.
		 */
		public function register_taxonomies() {

			$args = array(
				'label'             => __( 'Features Section', 'maintainn' ),
				'rewrite'           => false,
				'hierarchical'      => true,
				'public'            => true,
				'show_in_nav_menus' => false,
			);
			register_taxonomy( 'maintainn_features_section', 'features', $args );

		}

		public function register_cmb() {

			$prefix = 'cmb_';

			// Features Availability Metaboxes
			$feat_avail = new_cmb2_box( array(
				'id'            => 'features_availability',
				'title'         => __( 'Feature Availability', 'maintainn' ),
				'object_types'  => array( 'features', ), // Post type
				'context'       => 'side',
				'priority'      => 'low',
				'show_names'    => false,
			) );

			$feat_avail->add_field( array(
				'name'    => __( NULL, 'maintainn' ),
				'desc'    => __( 'Which plans have this feature available?', 'maintainn' ),
				'id'      => $prefix . 'feature-availability',
				'type'    => 'multicheck',
				'options' => array(
					'basic_available'     => __( 'Basic', 'maintainn' ),
					'premium_available'   => __( 'Premium', 'maintainn' ),
					'enterprise_avilable' => __( 'Enterprise', 'maintainn' ),
				)
			) );

			$home = new_cmb2_box( array(
				'id'            => 'homepage_features',
				'title'         => __( 'Homepage Details', 'maintainn' ),
				'object_types'  => array( 'features', ), // Post type
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true,
			) );

			$home->add_field( array(
				'name' => __( 'Homepage Info', 'maintainn' ),
				'desc' => __( 'This is what shows on homepage', 'maintainn' ),
				'id'   => $prefix . 'feature-home',
				'type' => 'textarea_small'
			) );

			$home->add_field( array(
				'name' => __( '<a href="http://fontawesome.github.io/Font-Awesome/" target="_blank">Font Awesome</a> Icon', 'maintainn' ),
				'desc' => __( 'Enter class name (ie. fa-globe).', 'maintainn' ),
				'id' => $prefix . 'feature-icon',
				'type' => 'text'
			) );

			// Page Sections
			$sections = new_cmb2_box( array(
				'id'            => 'maintainn_page_section',
				'title'         => __( 'Page Section', 'maintainn' ),
				'object_types'  => array( 'features', ), // Post type
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true,
			) );

			$sections->add_field( array(
				'name'             => __( 'Select Page Section', 'cmb2' ),
				'id'               => 'maintainn_features_page_section',
				'type'             => 'select',
				'show_option_none' => true,
				'options'          => array(
					'1' => __( 'Upper Vertical', 'maintainn' ),
					'2' => __( 'Middle Horizontal', 'maintainn' ),
					'3' => __( 'Bottom Vertical', 'maintainn' ),
				),
			) );

		}
	}

	$GLOBALS['maintainn_features'] = new Maintainn_Features;
	$GLOBALS['maintainn_features']->do_hooks();

}