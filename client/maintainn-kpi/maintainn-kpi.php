<?php
/**
 * Plugin Name: Maintainn KPI
 * Plugin URI: http://webdevstudios.com
 * Description: Sets up KPI Options.
 * Version: 1.0.0
 * Author: WebDevStudios
 * Author URI: http://webdevstudios.com
 * License: GPL2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Maintainn_KPI' ) ) {

	class Maintainn_KPI {

		public function __construct() {
			// Set up some variables.
			$this->key = 'maintainn-home';

			add_action( 'cmb2_init', array( $this, 'add_metabox' ) );
			add_filter( 'maintainn_site_options_tabs', array( $this, 'add_options_tab' ) );
		}

		function add_options_tab( $tabs = array() ) {
			$tabs['kpi'] = array(
				'name'     => __( 'KPI', 'maintainn' ),
				'callback' => array( $this, 'tab_display' ),
				'priority' => 20,
			);
			return $tabs;
		}

		function tab_display() {
			echo '<h3>' . __( 'KPI Options', 'maintainn' ) . '</h3>';
			cmb2_metabox_form( 'kpi-options-metabox', 'maintainn-kpi-options' );
		}

		function add_metabox() {
			$cmb = new_cmb2_box( array(
				'id'      => 'kpi-options-metabox',
				'hookup'  => false,
				'show_on' => array(
					// These are important, don't remove
					'key'   => 'options-page',
					'value' => array( 'maintainn-kpi-options', )
				),
			) );
			// Set our CMB2 fields
			$cmb->add_field( array(
				'name' => __( 'KPI Home Page Section Title', 'maintainn' ),
				'id'   => 'maintainn_kpi_section_title',
				'type' => 'text',
			) );

			$group_field_id = $cmb->add_field( array(
				'id'          => 'maintainn_kpi_data_group',
				'type'        => 'group',
				'name'        => __( 'KPI Data', 'maintainn' ),
				'description' => __( 'Enter your KPI values and labels.', 'maintainn' ),
				'options'     => array(
					'group_title'   => __( 'KPI Entry {#}', 'cmb2' ), // {#} gets replaced by row number
					'add_button'    => __( 'Add Another Entry', 'cmb2' ),
					'remove_button' => __( 'Remove Entry', 'cmb2' ),
					'sortable'      => true, // beta
				),
			) );
			$cmb->add_group_field( $group_field_id, array(
				'name'       => __( 'Label', 'maintainn' ),
				'id'         => 'kpi_label',
				'type'       => 'text',
			) );
			$cmb->add_group_field( $group_field_id, array(
				'name'       => __( 'Value', 'maintainn' ),
				'id'         => 'kpi_value',
				'type'       => 'text',
			) );
		}


	}

	new Maintainn_KPI();

}

/**
 * Loop through the KPI entries
 */
function maintainn_build_kpi_loop() {

	$option = get_option( 'maintainn-kpi-options' );

	if ( ! $option ) {
		return;
	}
	$html = '';
	foreach ( $option['maintainn_kpi_data_group'] as $kpi ) {
		$html .= '<div class="kpi ' . esc_attr( sanitize_title_with_dashes( $kpi['kpi_label'] ) ) . '">';
		$html .= '<span class="value">' . esc_html( $kpi['kpi_value'] ) . '</span>';
		$html .= '<span class="label">' . esc_html( $kpi['kpi_label'] ) . '</span>';
		$html .= '</div>';
	}

	return $html;

}

/**
 * Echo the KPI Loop
 */
function maintainn_kpi_loop() {

	echo maintainn_build_kpi_loop();

}

/**
 * Echo the section title for KPI
 */
function maintainn_kpi_section_title() {

	$option = get_option( 'maintainn-kpi-options' );
	if ( ! isset( $option['maintainn_kpi_section_title'] ) ) {
		return;
	}
	echo esc_html( $option['maintainn_kpi_section_title'] );

}