<?php

/**
* Define the metabox and field configurations.
*
* @param  array $meta_boxes
* @return array
*/
function wds_maintainn_announcement_metaboxes() {

	$prefix = '_wds_maintainn_announcement_';

	$metabox = new_cmb2_box( array(
		'id'         => 'announcement_info',
		'title'      => __( 'Announcement Information', 'maintainn' ),
		'object_types'  => array( 'announcements', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$metabox->add_field( array(
		'name'       => __( 'Link URL', 'maintainn' ),
		'id'         => $prefix . '_link_url',
		'type'       => 'text',
	) );

}
add_action( 'cmb2_init', 'wds_maintainn_announcement_metaboxes' );

/**
 * CMB Theme Options
 * @version 0.1.0
 */
class wds_maintainn_announcements_Admin {

    /**
     * Option key, and option page slug
     * @var string
     */
    protected static $key = 'wds_maintainn_announcements';

    /**
     * Options Page title
     * @var string
     */
    protected $title = '';

    /**
     * Constructor
     * @since 0.1.0
     */
    public function __construct() {
        // Set our title
        $this->title = __( 'Visibility Options', 'maintainn' );
    }

    /**
     * Initiate our hooks
     * @since 0.1.0
     */
    public function hooks() {
        add_action( 'admin_init', array( $this, 'init' ) );
        add_action( 'admin_menu', array( $this, 'add_options_page' ) );
	    add_action( 'cmb2_init', array( $this, 'option_fields') );

    }

    /**
     * Register our setting to WP
     * @since  0.1.0
     */
    public function init() {
        register_setting( self::$key, self::$key );
    }

    /**
     * Add menu options page
     * @since 0.1.0
     */
    public function add_options_page() {
        $this->options_page = add_submenu_page( 'edit.php?post_type=announcements', $this->title, $this->title, 'manage_options', self::$key, array( $this, 'admin_page_display' ) );
    }

    /**
     * Admin page markup. Mostly handled by CMB
     * @since  0.1.0
     */
    public function admin_page_display() {
        ?>
        <div class="wrap cmb_options_page <?php echo self::$key; ?>">
            <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
            <?php cmb2_metabox_form( 'maintainn_announcement_options', self::$key ); ?>
        </div>
        <?php
    }

    /**
     * Defines the theme option metabox and field configuration
     * @since  0.1.0
     * @return array
     */
    public static function option_fields() {

	    $cmb = new_cmb2_box( array(
		    'id'         => 'maintainn_announcement_options',
		    'show_on'    => array( 'key' => 'options-page', 'value' => array( self::$key, ), ),
		    'hookup'     => false,
	    ) );
	    $cmb->add_field( array(
		    'name' => 'Announcements visible across entire site',
		    'id'   => 'wds_maintainn_announcement_visibility',
		    'type' => 'checkbox'
	    ) );

    }

    /**
     * Make public the protected $key variable.
     * @since  0.1.0
     * @return string  Option key
     */
    public static function key() {
        return self::$key;
    }

}

// Get it started
$wds_maintainn_announcements_Admin = new wds_maintainn_announcements_Admin();
$wds_maintainn_announcements_Admin->hooks();

/**
 * Wrapper function around cmb_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function wds_maintainn_announcements_get_option( $key = '' ) {
    return cmb_get_option( wds_maintainn_announcements_Admin::key(), $key );
}