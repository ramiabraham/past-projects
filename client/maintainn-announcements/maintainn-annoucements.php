<?php
/**
 * Plugin Name: Maintainn Announcements
 * Plugin URI: http://www.webdevstudios.com/
 * Description: Globally displays a custom annocement for the site.
 * Author: WebDevStudios
 * Version: 1.0.0
 * Author URI: https://webdevstudios.com/
 * License: GPL2
 */
if( is_admin() ) {
  include( plugin_dir_path( __FILE__ ) . '/admin.php' );
}

/**
 * Enqueues our scripts needed for our plugin
 */
function wds_maintainn_announcement_enqueue(){
  wp_enqueue_script( 'jquery.cookie', plugins_url( 'js/jquery.cookies.js', __FILE__ ), array( 'jquery' ) );
  wp_enqueue_script( 'cookie-helper', plugins_url( 'js/cookie-helper.js', __FILE__ ), array( 'jquery' ) );
  wp_enqueue_script( 'announcment-bar', plugins_url( 'js/announcement-bar.js', __FILE__ ), array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'wds_maintainn_announcement_enqueue' );

/**
 * Echoes out the annoucment markup
 */
function wds_maintainn_display_announcement(){
  echo wds_maintainn_maybe_display_annoucement();
}

function wds_maintainn_maybe_display_annoucement(){
  if ( wds_maintainn_annoucements_visible() ) {
    return wds_maintainn_get_latest_announcement();
  }
}

function wds_maintainn_annoucements_visible(){
  if ( get_option( 'wds_maintainn_announcements' ) ){
    return true;
  }else{
    return false;
  }
}

function wds_maintainn_get_latest_announcement(){
  $annouce_query = wds_maintainn_get_annouce_query();

  $output = '';

  if ( $annouce_query->have_posts() ) {
	  while ( $annouce_query->have_posts() ) {
		  $annouce_query->the_post();
		  $link = get_post_meta( get_the_ID() , '_wds_maintainn_announcement__link_url', true );
		  $link_start = $link ? '<a href="' . esc_url( $link ) .'">' : '';
		  $link_end = $link ? '</a>' : '';
		  $output = '
			<div class="announcement_wrap announcement_bar" style="display:none;">
				<div class="announcement">

					<div class="announcement-box">'
						. $link_start .
							'<div class="message">
								<span>' . get_the_title() . '</span>
							</div>'
						. $link_end .
					'</div><!-- .announcement-box -->

					<div id="dismiss">
						<i class="fa fa-times-circle-o"></i>
					</div><!-- #dismiss -->

				</div><!-- .announcement -->
			</div><!-- .announcement_wrap .announcement_bar -->';
    }
  }
  wp_reset_postdata();
  return $output;
}

function wds_maintainn_get_annouce_query(){
  $args = array (
    'posts_per_page' => '1',
    'order'          => 'DESC',
    'orderby'        => 'date',
    'post_status'    => 'publish',
    'post_type'      => 'announcements'
  );
  return new WP_Query( $args );
}


add_action( 'init', 'wds_maintainn_register_cpt_annoucements' );

function wds_maintainn_register_cpt_annoucements() {

    $labels = array(
    'name'               => 'Annoucements',
    'singular_name'      => 'Announcement',
    'add_new'            => 'Add New Announcement',
    'add_new_item'       => 'Add New Announcement',
    'edit_item'          => 'Edit Announcement',
    'new_item'           => 'New Announcement',
    'view_item'          => 'View Announcement',
    'search_items'       => 'Search Announcements',
    'not_found'          => 'No Announcements found',
    'not_found_in_trash' => 'No Announcements found in Trash',
    'parent_item_colon'  => 'Parent Announcement:',
    'menu_name'          => 'Announcement',
    );

    $args = array(
    'labels'              => $labels,
    'hierarchical'        => false,
    'taxonomies'          => array(),
    'public'              => false,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 5,
    'menu_icon'         => 'dashicons-megaphone',
    'show_in_nav_menus'   => false,
    'publicly_queryable'  => true,
    'exclude_from_search' => false,
    'has_archive'         => false,
    'query_var'           => false,
    'can_export'          => true,
    'rewrite'             => false,
    'capability_type'     => 'post',
    'supports'            => array( 'title' ),
    );

    register_post_type( 'announcements', $args );
}


