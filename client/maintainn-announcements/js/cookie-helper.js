/*
 * jQuery Cookie for Announcements
 *
 */

jQuery(document).ready( function($) {

	if(!$.cookie('dismiss-announcement')) {
		$('.announcement_wrap').fadeIn(500);
		$('body').addClass('has-announcement');	
	} else {
		$('.announcement_wrap').hide();
	}
	
	$('#dismiss').click( function() {
		$.cookie('dismiss-announcement', 1, {expires : 1 });
		$('body').removeClass('has-announcement');
		$('announcement_wrap').hide();
	});
	
	if($('body').hasClass('login')) {
		$('announcement_wrap').hide();
	}
	
} );
