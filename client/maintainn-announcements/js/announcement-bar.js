/**
 * Set the correct margin for the Announcement Bar
 */
jQuery( function($) {

	// Set up variables
	var sitePage = $( '#page' );
	var siteBody = $( document.body );
	var announcementBar  = $( '.announcement_bar' );
	var adminBarHeight = parseFloat( $('html').css('margin-top') );
	var header = $( '#masthead' );

	/**
	 * Set margin based on height of the Announcement Bar
	 */
	var setMarginHeight = function() {
		if ( announcementBar.css('display') == 'none' ) {
			removeMargins();
			return;
		}

		siteBody.css( 'margin-top', '' );
		sitePage.css( 'margin-top', announcementBar.outerHeight() + header.outerHeight() );
		announcementBar.css( 'top', adminBarHeight );
		header.css( 'top', announcementBar.outerHeight() + adminBarHeight );

		if ( $(window).width() >= 800 ) {

			sitePage.css( 'margin-top', announcementBar.outerHeight() + header.outerHeight() );
			
			header.css({
				top: announcementBar.outerHeight() + adminBarHeight,
			});
			
		} else {

			sitePage.css( 'margin-top', announcementBar.outerHeight() );
			
			header.css({
				top: '0',
			});
		}
	}

	var removeMargins = function() {
				
		announcementBar.hide();
		
		header.css({
			top: '',
		});
		
		sitePage.css( 'margin-top', '' );
		
		if ( $(window).width() >= 800 ) {
			
			header.css({
				top: adminBarHeight,
			});
			
			sitePage.css( 'margin-top', header.outerHeight() );
		} else {
			sitePage.css( 'margin-top', '' );
			
		}
		
	}

	// If Announcement Bar is present
	if ( announcementBar.length ) {

		// Set margin
		setMarginHeight();
		announcementBar.on('click','#dismiss', removeMargins );

		// Make adjustments if the viewport changes too
		$( window ).resize( setMarginHeight );
	}

});
