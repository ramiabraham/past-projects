<?php
/*
Plugin Name: Maintainn - Notifications
Version: 0.1-alpha
Description: Stores improtant notifications that can be pulled by Maintainn Dashboard Plugin
Author: Maintainn
Author URI: http://maintainn.com/
Plugin URI: http://maintainn.com/
Text Domain: maintainn-notifications
Domain Path: /languages
*/

class Maintainn_Notifications {

	public static function instance() {
		static $instance = null;

		if ( null === $instance ) {
			$instance = new Maintainn_Notifications;
		}

		return $instance;
	}

	private function __construct() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_notification_plugin_meta' ) );

		add_filter( 'post_updated_messages', array( $this, 'notifications_updated_messages' ) );
		add_filter( 'json_prepare_post', array( $this, 'json_prepare_post' ), 10, 3 );
	}

	/**
	 * Register custom post type and taxonomy.
	 */
	public function init() {
		register_post_type( 'notifications', array(
			'labels'            => array(
				'name'                => __( 'Notifications', 'maintainn-notifications' ),
				'singular_name'       => __( 'Notifications', 'maintainn-notifications' ),
				'all_items'           => __( 'Notifications', 'maintainn-notifications' ),
				'new_item'            => __( 'New Notifications', 'maintainn-notifications' ),
				'add_new'             => __( 'Add New', 'maintainn-notifications' ),
				'add_new_item'        => __( 'Add New Notifications', 'maintainn-notifications' ),
				'edit_item'           => __( 'Edit Notifications', 'maintainn-notifications' ),
				'view_item'           => __( 'View Notifications', 'maintainn-notifications' ),
				'search_items'        => __( 'Search Notifications', 'maintainn-notifications' ),
				'not_found'           => __( 'No Notifications found', 'maintainn-notifications' ),
				'not_found_in_trash'  => __( 'No Notifications found in trash', 'maintainn-notifications' ),
				'parent_item_colon'   => __( 'Parent Notifications', 'maintainn-notifications' ),
				'menu_name'           => __( 'Notifications', 'maintainn-notifications' ),
			),
			'public'            => true,
			'hierarchical'      => false,
			'show_ui'           => true,
			'show_in_nav_menus' => false,
			'menu_icon'         => 'dashicons-megaphone',
			'supports'          => array( 'title', 'editor' ),
			'has_archive'       => false,
			'rewrite'           => false,
			'query_var'         => true,
		) );

		register_taxonomy( 'level', array( 'notifications' ), array(
			'hierarchical'      => true,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_ui'           => true,
			'show_admin_column' => false,
			'query_var'         => true,
			'rewrite'           => false,
			'capabilities'      => array(
				'manage_terms'  => 'edit_posts',
				'edit_terms'    => 'edit_posts',
				'delete_terms'  => 'edit_posts',
				'assign_terms'  => 'edit_posts'
			),
			'labels'            => array(
				'name'                       => __( 'Levels', 'maintainn-notifications' ),
				'singular_name'              => _x( 'Level', 'taxonomy general name', 'maintainn-notifications' ),
				'search_items'               => __( 'Search Levels', 'maintainn-notifications' ),
				'popular_items'              => __( 'Popular Levels', 'maintainn-notifications' ),
				'all_items'                  => __( 'All Levels', 'maintainn-notifications' ),
				'parent_item'                => __( 'Parent Level', 'maintainn-notifications' ),
				'parent_item_colon'          => __( 'Parent Level:', 'maintainn-notifications' ),
				'edit_item'                  => __( 'Edit Level', 'maintainn-notifications' ),
				'update_item'                => __( 'Update Level', 'maintainn-notifications' ),
				'add_new_item'               => __( 'New Level', 'maintainn-notifications' ),
				'new_item_name'              => __( 'New Level', 'maintainn-notifications' ),
				'separate_items_with_commas' => __( 'Levels separated by comma', 'maintainn-notifications' ),
				'add_or_remove_items'        => __( 'Add or remove Levels', 'maintainn-notifications' ),
				'choose_from_most_used'      => __( 'Choose from the most used Levels', 'maintainn-notifications' ),
				'menu_name'                  => __( 'Levels', 'maintainn-notifications' ),
			),
		) );
	}

	public function add_meta_boxes() {
		add_meta_box( 'notification-plugin', __( 'Notification Plugin',  'maintainn-notifications' ), array( $this, 'notification_plugin_meta' ), 'notifications' );
	}

	public function notification_plugin_meta( $post ) {
		wp_nonce_field( 'maintainn_notifications', 'maintainn_notifications_nonce' );

		$plugin = get_post_meta( $post->ID, '_notification_plugin', true );
		$plugin = empty( $plugin ) ? '' : $plugin;
		$ver = get_post_meta( $post->ID, '_notification_plugin_ver', true );
		$ver = empty( $ver ) ? '' : $ver;

		?>

		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row"><label for="notification_plugin"><?php _e( 'Plugin for this notification', 'maintainn-notifications' ); ?></label></th>
					<td><input type="text" id="notification_plugin" name="notification_plugin" value="<?php echo esc_attr( $plugin ) ?>" class="regular-text code" /></td>
				</tr>
				<tr>
					<th scope="row"><label for="notification_plugin_ver"><?php _e( 'Plugin version', 'myplugin_textdomain' ); ?></label></th>
					<td><input type="text" id="notification_plugin_ver" name="notification_plugin_ver" value="<?php echo esc_attr( $ver ); ?>" class="small-text" /></td>
				</tr>
			</tbody>
		</table>

		<?php
	}

	public function save_notification_plugin_meta( $post_id ) {
		// Check if our nonce is set.
		if ( ! isset( $_POST['maintainn_notifications_nonce'] ) ) {
			return;
		}

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['maintainn_notifications_nonce'], 'maintainn_notifications' ) ) {
			return;
		}

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! isset( $_POST['notification_plugin'] ) ) {
			return;
		}

		$plugin = sanitize_text_field( $_POST['notification_plugin'] );
		update_post_meta( $post_id, '_notification_plugin', $plugin );

		if ( isset( $_POST['notification_plugin_ver'] ) ) {
			update_post_meta( $post_id, '_notification_plugin_ver', trim( $_POST['notification_plugin_ver'] ) );
		}
	}

	public function notifications_updated_messages( $messages ) {
		global $post;

		$permalink = get_permalink( $post );

		$messages['notifications'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( __('Notifications updated. <a target="_blank" href="%s">View Notifications</a>', 'maintainn-notifications'), esc_url( $permalink ) ),
			2 => __('Custom field updated.', 'maintainn-notifications'),
			3 => __('Custom field deleted.', 'maintainn-notifications'),
			4 => __('Notifications updated.', 'maintainn-notifications'),
			/* translators: %s: date and time of the revision */
			5 => isset($_GET['revision']) ? sprintf( __('Notifications restored to revision from %s', 'maintainn-notifications'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( __('Notifications published. <a href="%s">View Notifications</a>', 'maintainn-notifications'), esc_url( $permalink ) ),
			7 => __('Notifications saved.', 'maintainn-notifications'),
			8 => sprintf( __('Notifications submitted. <a target="_blank" href="%s">Preview Notifications</a>', 'maintainn-notifications'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
			9 => sprintf( __('Notifications scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Notifications</a>', 'maintainn-notifications'),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
			10 => sprintf( __('Notifications draft updated. <a target="_blank" href="%s">Preview Notifications</a>', 'maintainn-notifications'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		);

		return $messages;
	}

	public function json_prepare_post( $post_response, $post, $context ) {
		$plugin = get_post_meta( $post['ID'], '_notification_plugin', true );
		$ver    = get_post_meta( $post['ID'], '_notification_plugin_ver', true );

		if ( empty( $plugin ) || empty( $ver ) ) {
			return $post_response;
		}

		$post_response['notification_plugin'] = array( 'name' => $plugin, 'ver' => $ver );

		return $post_response;
	}
}

// Initialize plugin
add_action( 'plugins_loaded', array( 'Maintainn_Notifications', 'instance' ) );