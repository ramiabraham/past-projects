<?php
/*
Plugin Name: Maintainn User Info
Plugin URI: http://maintainn.com
Description: Shows user info. Shortcode is structured as: [maintainn_user_info show="$att"], where $att can be one of the following: display_name, user_login, user_email, user_firstname, user_lastname, ID
Version: 1.0
License: GPL
Author: Maintainn
Author URI: http://maintainn.com

=== RELEASE NOTES ===
2014-03-13 - v1.0 - first version
*/

/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
Online: http://www.gnu.org/licenses/gpl.txt
*/

if  ( ! function_exists('maintainn_get_user_info') ) {

function maintainn_get_user_info( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'show' => '',
		), $atts )
	);

	/**
	*
	*	Get user info
	*	Shortcode is structured as: [maintainn_user_info show="$att"]
	*	where $att can be one of the following:
	*	display_name
	*	user_login
	*	user_email
	*	user_firstname
	*	user_lastname
	*	ID
	*/

global $current_user;

    get_currentuserinfo();

	if ( $show == 'display_name' ) {

		return $current_user->display_name . "\n";

	}

	else if ( $show == 'user_login' ) {

		return $current_user->user_login . "\n";

	}

	else if ( $show == 'user_email' ) {

		return $current_user->user_email . "\n";

	}

	else if ( $show == 'user_firstname' ) {

		return $current_user->user_firstname . "\n";

	}

	else if ( $show == 'user_lastname' ) {

		return $current_user->user_lastname . "\n";

	}

	else if ( $show == 'ID' ) {

		return $current_user->ID . "\n";

	}

}

add_shortcode( 'maintainn_user_info', 'maintainn_get_user_info' );
} //exists check
