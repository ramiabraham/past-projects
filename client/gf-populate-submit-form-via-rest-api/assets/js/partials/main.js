// Mamoth Group js
jQuery(document).ready(function($) {

    // console.log('GFMG: Model string encoding v5');

    if (typeof gfmg_data.fields !== 'undefined') {
        $(gfmg_data.fields).each(function(index, el) {
            $('#' + el).remove();
        });
    }

    if ('1' === gfmg_data.settings.is_dedicated) {
        console.log('Dedicated form; dealers node removed.');
        $('.dealers').remove();
    }

    $('.gform_wrapper').prepend('<div class="loading-wrap"><div class="loading"><div class="line"></div><div class="line"></div><div class="line"></div></div></div>');

    $('.loading').css({
        'position': 'absolute',
        'left': '50%',
        'top': '50%',
        'margin-left': -$('.loading').outerWidth() / 2,
        'margin-top': -$('.loading').outerHeight() / 2,
        'min-width': '100px'
    });

    $(document).ajaxStart(function(event) {
        $(".gform_wrapper .loading-wrap").show();
    }).ajaxStop(function() {
        $(".gform_wrapper .loading-wrap").hide();
    });

    // setDropDowns();
    // getDealers();

    $('.year select').change(function(event) {
        getManufacturer();
    });

    $('.make select').change(function(event) {
        getModels();
    });

    $('.model select').change(function(event) {
        getTrims();
    });

    $('.trim select').change(function(event) {
        getStyle();
    });

    function encodeChars(str) {
        return encodeURIComponent(str).
        replace(/['()]/g, escape).
        replace(/\*/g, '%2A').
        replace(/\&/g, '\\&').
        replace(/%(?:7C|60|5E)/g, unescape);
    }

    function setDropDowns() {
        var provinceId = getUrlVars()["provin"];


        clearSelect(".province", "", "Please select your Province");

        var provinces = gfmg_data.provinces;

        $.each(provinces, function(i, item) {

            $('.province select').append($('<option>', {
                value: item.value,
                text: item.text
            }));
        });

        if (!provinceId) {
            // $('.province select').prop('disabled', true);
            provinceId = 1;
        } else {
            //console.log(provinceId);
            $(".province select").val(provinceId).prop('selected', true);
            // getDealers();
        }

        $(".province select").blur(function(event) {
            // getDealers();
        });
    }

    function getDealers() {

        var provinceId = $(".province select").val();
        var dealers = gfmg_data.dealers;

        //alert('dealers'+provinceId);
        if (!provinceId) {
            provinceId = 1;
        }

        clearSelect(".dealers select", "", "Please select a dealer");

        var availableDealers = dealers[provinceId];

        // console.log(availableDealers);

        $.each(availableDealers, function(i, item) {


            $('.dealers select').append($('<option>', {
                value: item.value,
                text: item.text
            }));
        });

        $('.dealers select').prop('disabled', false);

    }

    function getManufacturer() {

        var year = $(".year select").find(":selected").val();
        if (year !== '') {

            $.ajax({
                url: gfmg_data.ajax_url,
                type: "get",
                crossDomain: true,
                dataType: "xml",
                data: {
                    action: 'get_vehicle_makes',
                    year: year
                },
                success: function(response) {

                    $(response).find('make').each(function() {
                        $('.make select').append(
                            $('<option>', {
                                value: $(this).text(),
                                text: $(this).text()
                            })
                        );
                    });

                    updatedropDowns("make");
                }

            });

        } else {
            updatedropDowns('year');
        }
    }

    function getModels() {
        var year = $(".year select").find(":selected").val();
        var make = $(".make select").find(":selected").val();
        if (year == "") {
            updatedropDowns("year");

        } else if (make == "") {
            updatedropDowns("make");

        } else {
            clearSelect(".model select", "", "Please select a Model");

            $.ajax({
                // url: gfmg_data.load_url + "?year= "+yearSelect+"&make= "+encodeChars(make),
                url: gfmg_data.ajax_url,
                type: "get",
                crossDomain: true,
                dataType: "xml",
                data: {
                    action: 'get_vehicle_models',
                    year: year,
                    make: make
                },
                success: function(response) {
                    // console.log('models:');
                    // console.log(response);

                    $(response).find('model').each(function() {
                        // console.log($(this).text());

                        $('.model select').append(
                            $('<option>', {
                                value: $(this).text(),
                                text: $(this).text()
                            })
                        );
                    });


                    updatedropDowns("model");
                }
            });
        }
    }

    function getTrims() {
        var val = "trim";
        var id = "CarTrim";
        var year = $(".year select").find(":selected").val();
        var make = $(".make select").find(":selected").val();
        var model = $(".model select").find(":selected").val();

        var getTrimsURL = gfmg_data.load_url + "?year=" + year + "&make=" + make + "&model=" + encodeChars(model);

        if (year == "") {
            updatedropDowns("year");
        } else if (make == "") {
            updatedropDowns("make");
        } else if (model == "") {
            updatedropDowns("model");
        } else {
            clearSelect(".trim select", "", "Please select a Trim");

            $.ajax({
                url: getTrimsURL,
                type: "GET",
                crossDomain: true,
                dataType: "xml",
                // data : {
                //   action : 'get_vehicle_models',
                //   year : year,
                //   make : make,
                //   model : model
                // },
                success: function(response) {
                    var fnd = false;
                    console.log('response:');
                    console.log(response);
                    if (null === response) {
                        $('.trim select').append($('<option>', {
                            value: 'Base',
                            text: 'Base'
                        }));
                    }

                    $(response).find(val).each(function() {
                        if ($(this).text() === '' && $(this).attr('id') == '1') {
                            // $('.trim select').append($('<option>', {
                            //     value: '',
                            //     text: 'None'
                            // }));
                            $('.trim select option:selected').text('None');
                            $('.trim select option:selected').val('');

                            getStyle(true);
                        } else {
                            $('.trim select').append($('<option>', {
                                value: $(this).text(),
                                text: $(this).text()
                            }));

                            if ($(this).text() !== "") {
                                fnd = true;
                            }
                        }

                    });



                    if (!fnd) {
                        getStyle(true);
                    }

                    updatedropDowns(val);

                }
            });
        }
    }

    function getStyle(valid) {
        var val = "style";
        var yearSelect = $(".make select").find(":selected").val();
        var year = $(".year select").find(":selected").val();
        var make = $(".make select").find(":selected").val();
        var model = $(".model select").find(":selected").val();
        var trim = $(".trim select").find(":selected").val();

        var getStylesURL = gfmg_data.load_url + "?year=" + year + "&make=" + make + "&model=" + encodeChars(model) + "&trim=" + trim;

        if (valid) {
            trim = "Base";
        }

        if ($(".trim select").find(":selected").val() === "" && $(".trim select").find(":selected").text() === "") {
            trim = "Base";
        }

        if (yearSelect == "") {
            updatedropDowns("year");
        } else if (make == "") {
            updatedropDowns("make");
        } else if (model == "") {
            updatedropDowns("model");
        } else if (trim == "") {

            updatedropDowns("trim");
        } else {
            clearSelect(".style select", "", "Please select a Style");
            $.ajax({
                url: getStylesURL,
                type: 'GET',
                crossDomain: true,
                dataType: "xml",
                // data : {
                //   action : 'get_vehicle_styles',
                //   year : year,
                //   make : make,
                //   model : model,
                //   trim : trim
                // },
                success: function(xml) {

                    if (null === xml) {

                        console.log('Style response is null');

                        $('.style select').append($('<option>', {
                            value: 'None',
                            text: 'None'
                        }));
                    }

                    $(xml).find(val).each(function() {

                        $('.style select').append($('<option>', {
                            value: $(this).text(),
                            text: $(this).text()
                        }));
                    });

                    updatedropDowns(val);
                }
            });
        }
    }

    function clearSelect(select, val, text) {
        $(select)
            .find('option')
            .remove()
            .end()
            .append('<option value= "' + val + '">' + text + '</option>')
            .val(val);
    }

    function getUrlVars() {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    function loadYear(xml) {
        $(xml).find('year').each(function() {
            $('<option>').val($(this).text()).text($(this).text()).appendTo('.make select');
        });

        updatedropDowns("year");
    }

    function updatedropDowns(val) {
        if (val === "year") {
            $('.make select').prop('disabled', false);
            $('.make select').hide();
            $('.make select').hide();
            $('.model select').hide();
            $('.trim select').hide();
            $('.style select').hide();
            $('.distance input').hide();
        } else if (val === "make") {
            $('.make select').prop('disabled', false);
            $('.make select').prop('disabled', false);
            $('.make select').show();
            $('.model select').hide();
            $('.trim select').hide();
            $('.style select').hide();
            $('.distance input').hide();
        } else if (val === "model") {
            $('.make select').prop('disabled', false);

            $('.make select').show();
            $('.model select').show();
            $('.trim select').hide();
            $('.style select').hide();
            $('.distance input').hide();

        } else if (val === "trim") {
            $('.year select').show();
            $('.make select').show();
            $('.model select').show();
            $('.trim select').show();
            $('.style select').hide();
            $('.distance input').hide();

        } else if (val === "style") {
            $('.year select').show();
            $('.make select').show();
            $('.model select').show();
            $('.trim select').show();
            $('.style select').show();
            $('.distance input').show();
        }
    }

    // Get by distance
    $('.distance input').change(function() {
        var year = $(".year select").find(":selected").val();
        var make = $(".make select").find(":selected").val();
        var model = $(".model select").find(":selected").val();
        var trim = $(".trim select").find(":selected").val();
        var style = $(".style select").find(":selected").val();
        var distance = $(".distance input").val();

        if ($(".trim select").find(":selected").val() === "" && $(".trim select").find(":selected").text() === "") {
            trim = "Base";
        }

        if (year == "") {
            updatedropDowns("year");
        } else if (make == "") {
            updatedropDowns("make");
        } else if (model == "") {
            updatedropDowns("model");
        } else if (trim == "") {
            updatedropDowns("trim");
        } else {

            $.ajax({
                type: "GET",
                url: gfmg_data.load_url + "?getPrice=&year=" + year + "&make=" + make + "&model=" + encodeChars(model) + "&trim=" + trim + "&style=" + style + "&distance=" + distance,
                crossDomain: true,
                dataType: "xml",
                success: function(response) {

                    $('input[type="submit"].gform_button').prop('disabled', false);

                    console.log('price response:');

                    console.log(response);

                    if (null === response) {
                        console.log('Price response is null');
                    }

                    console.log('tradeInHigh: ' + $(response).find('tradeInHigh').text());
                    console.log('tradeInLow: ' + $(response).find('tradeInLow').text());

                    $('.tradeInHigh input').val($(response).find('tradeInHigh').text());
                    $('.tradeInLow input').val($(response).find('tradeInLow').text());

                },
                error: function(response) {
                    console.log('Unable to retrieve vehicle prices. Error message:');
                    console.log(response);

                    $('input[type="submit"].gform_button').prop('disabled', true);
                }

            });
        }

    });

    $(".read-more-toggle").click(function() {
        $('.read-more-content').slideToggle();
    });

    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', gfmg_data.settings.gfmg_google_analytics, 'auto');
    ga('send', 'pageview');

    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', gfmg_data.settings.gfmg_facebook);
    fbq('track', 'PageView');
});
