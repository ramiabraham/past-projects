<?php

class SocialLead {
    public $api_url   = '';
    public $lang      = 'en';
    public $ad_source = '';
    public $data      = array();

    public function __construct($data) {
        if (is_array($data)) {
            $this->data = $data;

        }
    }

    public function create() {
        //try {
        $url = $this->api_url;

        $payload = '&lang=' . urlencode($this->lang) .
        '&ad_source=' . urlencode($this->ad_source) .
        '&dealership=' . urlencode($this->data['showroom_code']) .
        '&dealerID=' . urlencode($this->data['dealerID']) .
        '&name=' . urlencode($this->data['name']) .
        '&email=' . urlencode($this->data['email']) .
        '&tel_number=' . urlencode($this->data['phone']) .
        '&postal_code=' . urlencode($this->data['postal_code']) .
        '&http_request_headers=' .
        json_encode(
            $this->urlEncode(
                $this->parseRequestHeaders())) .
        '&meta_data=' . json_encode(
            $this->urlEncode($this->data['meta_data']));

        error_log("is this the url for sending =" . $url);

        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

        // $xml = file_get_contents($url, false, $context);

        $xml = $this->requestUrl($url, $payload);

        error_log('social-lead.php - requestUrl response:: ' . print_r($xml, true));

        if ($xml === false) {
            error_log("XML returned false");
        }

        //} catch(Exception $e) { }
    }

    public function url_get_contents($url) {
        if (!function_exists('curl_init')) {
            error_log('CURL is not installed!');
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function parseRequestHeaders() {
        $headers = array();
        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) != 'HTTP_') {
                continue;
            }
            $header           = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $headers[$header] = $value;
        }
        $headers['IP-Address'] = $this->ipAddress();
        return $headers;
    }

    public function urlEncode($input) {
        if (!is_array($input)) {
            return urlencode($input);
        }

        $arr = array();

        foreach ($input as $key => $value) {
            if (is_numeric($key)) {
                $arr[$key] = urlencode($value);
            } else {
                $arr[urlencode($key)] = urlencode($value);
            }
        }

        return $arr;
    }

    public function requestUrl($url, $payload) {

        $cmd = "curl -X POST -H 'Content-Type: application/x-www-form-urlencoded'";
        $cmd .= " -d '" . $payload . "' " . "'" . $url . "'";

        //if (!$this->debug()) {
        $cmd .= " > /dev/null 2>&1 &";
        //}

        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function ipAddress() {
        $ip = $_SERVER['REMOTE_ADDR'];
        if ($ip) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            return $ip;
        }
        // There might not be any data
        return false;
    }
}
