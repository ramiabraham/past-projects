<?php
$context     = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
$getYearsUrl = "https://ar.absoluteresults.com/taarga/cbb.php?key=9C941INMK6&getYears";
$getmake     = "https://ar.absoluteresults.com/taarga/cbb.php?key=9C941INMK6&getMakes=&year=";
$getModel    = "https://ar.absoluteresults.com/taarga/cbb.php?key=9C941INMK6&getModels=&year=insert_Year&make=insert_make";
$gettrim     = "https://ar.absoluteresults.com/taarga/cbb.php?key=9C941INMK6&getTrims=&year=insert_Year&make=insert_make&model=insert_model";
$getstyle    = "https://ar.absoluteresults.com/taarga/cbb.php?key=9C941INMK6&getStyles=&year=insert_Year&make=insert_make&model=insert_model&trim=insert_trim";
$getprice    = "https://ar.absoluteresults.com/taarga/cbb.php?key=9C941INMK6&getValue=&year=insert_Year&make=insert_make&model=insert_model&trim=insert_trim&style=insert_style&kilometers=insert_distance";

if (isset($_REQUEST['year']) && isset($_REQUEST['make']) && isset($_REQUEST['model']) && isset($_REQUEST['trim']) && isset($_REQUEST['style']) && isset($_REQUEST['distance']) &&
    empty($_REQUEST['year']) && empty($_REQUEST['make']) && empty($_REQUEST['model']) && empty($_REQUEST['trim']) && empty($_REQUEST['style']) && empty($_REQUEST['distance'])) {
    echo "Error: Incomplete parameters passed in.";
} else {

    if (isset($_REQUEST['year']) && isset($_REQUEST['make']) && isset($_REQUEST['model']) && isset($_REQUEST['trim']) && isset($_REQUEST['style']) && isset($_REQUEST['distance'])) {
        $makeSelect  = $_REQUEST['make'];
        $yearSelect  = $_REQUEST['year'];
        $modelSelect = urlencode($_REQUEST['model']);
        // $modelSelect    = str_replace('&', '%26', $modelSelect);
        $trimSelect     = urlencode($_REQUEST['trim']);
        $styleSelect    = urlencode($_REQUEST['style']);
        $distanceSelect = urlencode($_REQUEST['distance']);

        $priceSelectUrl = preg_replace("/" . "insert_Year" . "/i", $yearSelect, $getprice);
        $priceSelectUrl = preg_replace("/" . "insert_make" . "/i", $makeSelect, $priceSelectUrl);
        $priceSelectUrl = preg_replace("/" . "insert_model" . "/i", $modelSelect, $priceSelectUrl);
        $priceSelectUrl = preg_replace("/" . "insert_trim" . "/i", $trimSelect, $priceSelectUrl);
        $priceSelectUrl = preg_replace("/" . "insert_style" . "/i", $styleSelect, $priceSelectUrl);
        $priceSelectUrl = preg_replace("/" . "insert_distance" . "/i", $distanceSelect, $priceSelectUrl);

        echo file_get_contents($priceSelectUrl, false, $context);
    } else if (isset($_REQUEST['year']) && isset($_REQUEST['make']) && isset($_REQUEST['model']) && isset($_REQUEST['trim'])) {
        $makeSelect  = $_REQUEST['make'];
        $yearSelect  = $_REQUEST['year'];
        $modelSelect = urlencode($_REQUEST['model']);
        // $modelSelect = str_replace('&', '%26', $modelSelect);
        $trimSelect = urlencode($_REQUEST['trim']);

        $styleSelectUrl = preg_replace("/" . "insert_Year" . "/i", $yearSelect, $getstyle);
        $styleSelectUrl = preg_replace("/" . "insert_make" . "/i", $makeSelect, $styleSelectUrl);
        $styleSelectUrl = preg_replace("/" . "insert_model" . "/i", $modelSelect, $styleSelectUrl);
        $styleSelectUrl = preg_replace("/" . "insert_trim" . "/i", $trimSelect, $styleSelectUrl);
        //ChromePhp::log($styleSelectUrl);
        echo file_get_contents($styleSelectUrl, false, $context);
    } else if (isset($_REQUEST['year']) && isset($_REQUEST['make']) && isset($_REQUEST['model'])) {
        $makeSelect  = $_REQUEST['make'];
        $yearSelect  = $_REQUEST['year'];
        $modelSelect = $_REQUEST['model'];
        $modelSelect = urlencode($modelSelect);
        // $modelSelect   = str_replace('&', '%26', $modelSelect);
        $trimSelectUrl = preg_replace("/" . "insert_Year" . "/i", $yearSelect, $gettrim);
        $trimSelectUrl = preg_replace("/" . "insert_make" . "/i", $makeSelect, $trimSelectUrl);
        $trimSelectUrl = preg_replace("/" . "insert_model" . "/i", $modelSelect, $trimSelectUrl);

        echo file_get_contents($trimSelectUrl, false, $context);
    } else if (isset($_REQUEST['year']) && isset($_REQUEST['make'])) {
        $makeSelect     = $_REQUEST['make'];
        $yearSelect     = $_REQUEST['year'];
        $modelSelectUrl = preg_replace("/" . "insert_Year" . "/i", $yearSelect, $getModel);
        $modelSelectUrl = preg_replace("/" . "insert_make" . "/i", $makeSelect, $modelSelectUrl);
        $modelSelectUrl = str_replace('&', '%26', $modelSelectUrl);
        echo file_get_contents($modelSelectUrl, false, $context);
    } else if (isset($_REQUEST['year'])) {
        $yearSelect = $_REQUEST['year'];
        $getMakeUrl = $getmake . $yearSelect;
        echo file_get_contents($getMakeUrl, false, $context);
    } else {
        echo file_get_contents($getYearsUrl, false, $context);
    }
}
