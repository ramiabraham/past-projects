<?php

class ar_api {
    // https://ar.absoluteresults.com/rest/digitalwebleads?
    public $api_url   = 'https://ar.absoluteresults.com/rest/digitalwebleads?';
    public $lang      = 'en';
    public $ad_source = '';

    public $dealerID = '519';
    public $source   = 'digital';

    public $data     = array();
    public $carWorth = false;
    public $vehicle  = array();
    public $offer    = '';

    public function __construct($data) {
        if (is_array($data)) {
            $this->data = $data;

        }
    }

    public function create() {

        if ($this->data['carWorth']) {
            $this->carWorth = true;
        }

        if (!empty($this->data['campaign_id'])) {
            $campaign_id = $this->data['campaign_id'];
        } else {
            $site_url    = get_site_url();
            $campaign_id = substr($site_url, strpos($site_url, "//") + 1);
            $campaign_id = str_replace('/', '', $campaign_id);
        }

        $phone = preg_replace('/[^0-9.]+/', '', $this->data['phone']);

        $dealer      = $this->data['showroom_code'];
        $dealerspilt = explode(":", $dealer);

        $url     = $this->api_url;
        $payload = 'url=' . $campaign_id .
        '&fullname=' . urlencode($this->data['name']) .
        //'&lastname='.urlencode('').
        '&postalCode=' . urlencode($this->data['postal_code']) .
        '&mainPhone=' . $phone .
        '&email=' . urlencode($this->data['email']) .
        '&dealerID=' . $this->dealerID .
        '&offer=' . urlencode($this->offer) .
        '&currentDescription=' . urlencode($this->data['currVehicle']);

        if ($this->data['carWorth'] || $this->carWorth) {
            $notes = 'Trade in Value: $' . $this->data['tradeInLow'] . ' - $' . $this->data['tradeInHigh'];

            $payload = $payload . '&years=' . $this->data['year'] .
            '&makes=' . urlencode($this->data['make']) .
            '&models=' . urlencode($this->data['model']) .
            '&trims=' . urlencode($this->data['trim']) .
            '&styles=' . urlencode($this->data['style']) .
            '&kilometers=' . urlencode($this->data['distance']) .
            '&tradeInHigh=' . urlencode($this->data['tradeInHigh']) .
            '&tradeInLow=' . urlencode($this->data['tradeInLow']);
        }

        // error_log ("Full request: " . $url . $payload);

        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

        //$xml = @file_get_contents($url, false, $context);
        $xml = $this->requestUrl($url, $payload);

        // error_log ("API response: " . $xml);

        //} catch(Exception $e) { }
    }

    public function parseRequestHeaders() {
        $headers = array();
        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) != 'HTTP_') {
                continue;
            }
            $header           = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $headers[$header] = $value;
        }
        $headers['IP-Address'] = $this->ipAddress();
        return $headers;
    }

    private function requestUrl($url, $payload) {

        $cmd = "curl -X POST -H 'Content-Type: application/x-www-form-urlencoded'";
        $cmd .= " -d '" . $payload . "' " . "'" . $url . "'";

        //if (!$this->debug()) {
        $cmd .= " > /dev/null 2>&1 &";
        //}

        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function call_url($url) {
        // Call the url and return immediately with the length of the data read.
        // Useful for firing off scripts by calling a url.

        $fp = fopen($url, 'r');
        stream_set_blocking($fp, 0); // 0 => non-blocking mode
        $data = fread($fp, 8192); // 1024 * 8 = 8192
        fclose($fp);

        // Return the amount of data read
        return strlen($data);
    }

    public function urlEncode($input) {
        if (!is_array($input)) {
            return urlencode($input);
        }

        $arr = array();

        foreach ($input as $key => $value) {
            if (is_numeric($key)) {
                $arr[$key] = urlencode($value);
            } else {
                $arr[urlencode($key)] = urlencode($value);
            }
        }

        return $arr;
    }

    public function ipAddress() {
        $ip = $_SERVER['REMOTE_ADDR'];
        if ($ip) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            return $ip;
        }
        // There might not be any data
        return false;
    }
}
