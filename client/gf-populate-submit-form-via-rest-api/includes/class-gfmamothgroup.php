<?php

GFForms::include_addon_framework();

class GFMamothGroup extends GFAddOn {

    protected $_version                  = GF_MAMOTH_GROUP_ADDON_VERSION;
    protected $_min_gravityforms_version = '1.9';
    protected $_slug                     = 'gf-mamoth-group';
    protected $_path                     = 'gf-mamoth-group/gf-mamoth-group.php';
    protected $_full_path                = __FILE__;
    protected $_title                    = 'Mamoth Group Add-on';
    protected $_short_title              = 'MG Add-On';

    public $account = '';

    public $title = '';

    private static $_instance = null;

    /**
     * Get an instance of this class.
     *
     * @return GFMamothGroup
     */
    public static function get_instance() {
        if (self::$_instance == null) {
            self::$_instance = new GFMamothGroup();
        }

        return self::$_instance;
    }

    /**
     * Handles hooks and loading of language files.
     */
    public function init() {
        parent::init();

        // Plugin dir path
        if (!defined('GFMG_PLUGIN_DIR')) {
            define('GFMG_PLUGIN_DIR', dirname(plugin_dir_path(__FILE__)));
        }

        // Plugin folder url
        if (!defined('GFMG_PLUGIN_URL')) {
            define('GFMG_PLUGIN_URL', plugin_dir_url(__FILE__));
        }

        $this->account = 'Chrysler Trade Keys';
        $this->includes();

        add_action('gform_admin_pre_render', array($this, 'add_merge_tags'));

        // Dedicated form checks
        add_filter('gform_pre_render', array($this, 'set_conditional_logic_hide_province_and_showroom'));
        add_filter('gform_pre_process', array($this, 'set_conditional_logic_hide_province_and_showroom'));

        add_filter('gform_replace_merge_tags', array($this, 'set_merge_tag_value'), 10, 7);
        add_filter('gform_submit_button', array($this, 'form_submit_button'), 10, 2);
        add_action('gform_enqueue_scripts', array($this, 'frontend_assets'), 10, 2);

        add_action('gform_pre_submission', array($this, 'pre_submission'));

        add_action('gform_after_submission', array($this, 'after_submission'), 10, 2);

        /**
         * Obsolete; replaced with confirmation-message tracking scripts via add_confirmation_tracking_scripts
         */
        // add_action('gform_register_init_scripts', array($this, 'footer'), 10, 2);

        /**
         * Adds tracking scripts to GF confirmation messages.
         */
        add_filter('gform_confirmation', array($this, 'add_confirmation_tracking_scripts'), 10, 4);

        add_action('wp_ajax_nopriv_get_vehicle_makes', array($this, 'get_vehicle_makes'));
        add_action('wp_ajax_get_vehicle_makes', array($this, 'get_vehicle_makes'));

        add_action('wp_ajax_nopriv_get_vehicle_models', array($this, 'get_vehicle_models'));
        add_action('wp_ajax_get_vehicle_models', array($this, 'get_vehicle_models'));

        add_action('wp_ajax_nopriv_get_vehicle_trims', array($this, 'get_vehicle_trims'));
        add_action('wp_ajax_get_vehicle_trims', array($this, 'get_vehicle_trims'));

        add_action('wp_ajax_nopriv_get_vehicle_styles', array($this, 'get_vehicle_styles'));
        add_action('wp_ajax_get_vehicle_styles', array($this, 'get_vehicle_styles'));

        add_action('wp_ajax_nopriv_get_vehicle_price', array($this, 'get_vehicle_price'));
        add_action('wp_ajax_get_vehicle_price', array($this, 'get_vehicle_price'));

    }

    /**
     * Includes files
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function includes() {
        if (is_admin()) {
            return;
        }

        // Social Lead script
        include trailingslashit(GFMG_PLUGIN_DIR) . 'includes/lib/social-lead.php';

        // AR API
        include trailingslashit(GFMG_PLUGIN_DIR) . 'includes/lib/ar_api.php';
    }

    /**
     * Adds Facebook and Google tracking scripts to GF confirmation messages.
     * Note: This is primarily a POC, and currently only functions
     * with GF confirmation messages; it does not yet function with page redirects.
     *
     * @since 1.0.1
     *
     * @param [type]  $confirmation [description]
     * @param [type]  $form         [description]
     * @param [type]  $entry        [description]
     * @param [type]  $ajax         [description]
     */
    public function add_confirmation_tracking_scripts($confirmation, $form, $entry, $ajax) {
        if (is_admin()) {
            return;
        }

        $settings = $this->gfmg_settings($form);

        if (!empty($settings['gfmg_google_analytics'])) {
            $confirmation .= "<script>(function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', '" . $settings['gfmg_google_analytics'] . "', 'auto');
            ga('send', 'pageview');</script>";
        }

        if (!empty($settings['gfmg_facebook'])) {
            // Add FB tracking pixel
            $confirmation .= "<script>! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
            }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '" . $settings['gfmg_facebook'] . "');
            fbq('track', 'PageView');</script>";
        }

        return $confirmation;
    }

    /**
     * Injects footer content.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function footer($form, $field_values) {
        if (is_admin()) {
            return;
        }

        $settings = $this->gfmg_settings($form);
        $content  = "";

        if (!empty($settings['gfmg_google_analytics'])) {

            $content .= "(function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', '" . $settings['gfmg_google_analytics'] . "', 'auto');
            ga('send', 'pageview');";
        }

        if (!empty($settings['gfmg_facebook'])) {
            // Add FB tracking pixel
            $content .= "! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
            }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '" . $settings['gfmg_facebook'] . "');
            fbq('track', 'PageView');";
        }

        // echo $content;
        GFFormDisplay::add_init_script($form['id'], 'gfmg_tracking', GFFormDisplay::ON_PAGE_RENDER, $content);
    }

    public function is_dev() {
        if (false !== strpos(site_url(), '.dev') || false !== strpos(site_url(), '.test') || false !== strpos(site_url(), 'staging')) {
            return true;
        }

        return false;
    }

    /**
     * Returns empty string for local or staging server, otherwise returns '.min'.
     *
     * @return string
     * @since  04102018
     */
    public function min_string() {
        if ($this->is_dev()) {
            return '';
        }

        return '.min';
    }

    /**
     * Enqueues front-end scripts and styles.
     *
     * @since  1.0.0
     *
     * @param  [type]  $form    [description]
     * @param  [type]  $is_ajax [description]
     *
     * @return [type]           [description]
     */
    public function frontend_assets($form, $is_ajax) {
        $min = $this->min_string();

        $localized_data             = $this->get_localized_data();
        $localized_data['settings'] = $this->gfmg_settings($form);

        if (isset($form['gf-mamoth-group']['is_dedicated'])) {
            if ($form['gf-mamoth-group']['is_dedicated']) {
                $localized_data['fields'] = $this->get_dealer_node_ids($form);
            }
        }

        wp_register_script('gf-mamoth-group-js', plugins_url('/assets/js/main' . $min . '.js', dirname(__FILE__)), array('jquery'), rand(4, 16));

        wp_localize_script('gf-mamoth-group-js', 'gfmg_data', $localized_data);

        wp_enqueue_script('gf-mamoth-group-js');

        wp_enqueue_style('gf-mamoth-group-css', plugins_url('/assets/css/main' . $min . '.css', dirname(__FILE__)), array(), rand(4, 16));
    }

    /**
     * Returns admin scripts which should be enqueued.
     *
     * @return array
     */
    public function scripts() {
        $scripts = array(
            array(
                'handle'  => 'mamoth_group_js',
                'src'     => $this->get_base_url() . '/assets/js/main.js',
                'version' => $this->_version,
                'deps'    => array('jquery'),
                'strings' => array(
                    'first'  => esc_html__('First Choice', 'gf-mamoth-group'),
                    'second' => esc_html__('Second Choice', 'gf-mamoth-group'),
                    'third'  => esc_html__('Third Choice', 'gf-mamoth-group'),
                ),
                'enqueue' => array(
                    array(
                        'admin_page' => array('form_settings'),
                        // 'tab'        => 'gf-mamoth-group'
                    ),
                ),
            ),

        );

        return array_merge(parent::scripts(), $scripts);
    }

    /**
     * Returns the stylesheets which should be enqueued.
     *
     * @return array
     */
    public function styles() {
        $styles = array(
            array(
                'handle'  => 'gf-mamoth-group-styles',
                'src'     => $this->get_base_url() . '/assets/css/main.css',
                'version' => $this->_version,
                'enqueue' => array(
                    // array( 'field_types' => array( 'poll' ) )
                ),
            ),
        );

        return array_merge(parent::styles(), $styles);
    }

    public function add_merge_tags($form) {
        ?>
        <script type="text/javascript">
            gform.addFilter('gform_merge_tags', 'add_merge_tags');
            function add_merge_tags(mergeTags, elementId, hideAllFields, excludeFieldTypes, isPrepop, option){
                mergeTags["custom"].tags.push({ tag: '{dealerID}', label: 'Dealer ID' });
                // mergeTags["custom"].tags.push({ tag: '{custom_merge_tag2}', label: 'My Custom Merge Tag 2' });
                // mergeTags["required"].tags.push({ tag: '{required_merge_tag1}', label: 'My Required Merge Tag 1' });

                console.log('Merge tags:');
                console.log(mergeTags["custom"].tags);

                return mergeTags;
            }
        </script>
        <?php
return $form;
    }

    public function set_merge_tag_value($text, $form, $entry, $url_encode, $esc_html, $nl2br, $format) {
        $custom_merge_tag = '{dealerID}';
        if (strpos($text, $custom_merge_tag) === false) {
            return $text;
        }
        $settings = $this->gfmg_settings($form);

        $siteName = $settings['dealer_id'];
        $text     = str_replace($custom_merge_tag, $siteName, $text);
        return $text;
    }

    /**
     * Add the text in the plugin settings to the bottom of the form if enabled for this form.
     *
     * @param string $button The string containing the input tag to be filtered.
     * @param array $form The form currently being displayed.
     *
     * @return string
     */
    public function form_submit_button($button, $form) {
        // Reference: Gets all form settings.
        $settings = $this->get_form_settings($form);

        if (isset($settings['enabled']) && true == $settings['enabled']) {
            $text   = $this->get_plugin_setting('mytextbox');
            $button = "<div>{$text}</div>" . $button;
        }

        return $button;
    }

    public function get_addon_settings_values() {
        $settings = maybe_unserialize(get_option('gravityformsaddon_gf-mamoth-group_settings'));
        return (array) $settings;
    }

    /**
     * Configures the settings which should be rendered on the main add-on settings tab.
     *
     * @return array
     */
    public function plugin_settings_fields() {
        return array(
            array(
                'title'  => esc_html__('These add-on settings are used globally as a fallback for all Mamoth Group lead and value forms. If no value for a field is found in a form, the matching global setting below will be used in place.', 'gf-mamoth-group'),
                'fields' => array(
                    array(
                        'name'              => 'gfmg_global_taarga_access_token',
                        'tooltip'           => esc_html__('Place the Taarga Access Token here. This Taarga Access Token will be used for all forms on the site which do not have a custom Taarga Access Token already specified.', 'gf-mamoth-group'),
                        'label'             => esc_html__('Global Taarga Access Token', 'gf-mamoth-group'),
                        'type'              => 'text',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                    array(
                        'name'              => 'gfmg_global_dealer_id',
                        'tooltip'           => esc_html__('Place the Dealer ID here. This dealer ID will be used for all forms on the site which do not have a custom Dealer ID already specified.', 'gf-mamoth-group'),
                        'label'             => esc_html__('Global Dealer ID', 'gf-mamoth-group'),
                        'type'              => 'text',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                    array(
                        'name'              => 'gfmg_global_campaign_id',
                        'tooltip'           => esc_html__('Place the campaign ID here. This campaign ID will be used as a fallback for all forms on the site which do not have a custom campaign ID already specified. If both this global campaign ID setting an the form campaign ID setting are left blank, the site url will be used', 'gf-mamoth-group'),
                        'label'             => esc_html__('Global Campaign ID', 'gf-mamoth-group'),
                        'type'              => 'text',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                    array(
                        'name'              => 'gfmg_global_google_analytics',
                        'tooltip'           => esc_html__('Place your Google Analytics account ID here. This typically starts with "UA-", and will look like "UA-52478871-1". If the form does not have a custom Google Analytics account ID set, the value set in this global field will be used as a fallback. A Google Analytics tracking script will automatically be added to the form.', 'gf-mamoth-group'),
                        'label'             => esc_html__('Global Google Analytics ID', 'gf-mamoth-group'),
                        'type'              => 'text',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                    array(
                        'name'              => 'gfmg_global_facebook',
                        'tooltip'           => esc_html__('Place your Facebook account ID here. This typically starts is a number, and will look like "1529849607230335". If the form does not have a custom Facebook ID set, the value set in this global setting will be used as a fallback. A Facebook Pageview tracking script will automatically be added to the form.', 'gf-mamoth-group'),
                        'label'             => esc_html__('Global Facebook Account ID', 'gf-mamoth-group'),
                        'type'              => 'text',
                        'class'             => 'medium',
                        'feedback_callback' => array($this, 'is_valid_setting'),
                    ),
                ),
            ),
        );
    }

    /**
     * Configures the settings which should be rendered on the Form Settings > MG Add-On tab.
     *
     * @return array
     */
    public function form_settings_fields($form) {
        $admin_link = admin_url('page=gf_settings&subview=gf-mamoth-group');

        return array(
            array(
                'title'  => esc_html__('Mamoth Group Form Settings', 'gf-mamoth-group'),
                'fields' => array(
                    array(
                        'name'    => 'taarga_access_token',
                        'tooltip' => esc_html__('Place the Taarga Access Token here. This access token will be used for all form submissions for this form.', 'gf-mamoth-group'),
                        'label'   => esc_html__('Taarga Access Token', 'gf-mamoth-group'),
                        'type'    => 'text',
                        'class'   => 'medium',
                    ),
                    array(
                        'name'    => 'dealer_id',
                        'tooltip' => esc_html__('Place the Dealer ID here. This dealer ID will be used for form submissions for this form.', 'gf-mamoth-group'),
                        'label'   => esc_html__('Dealer ID', 'gf-mamoth-group'),
                        'type'    => 'text',
                        'class'   => 'medium',
                    ),
                    array(
                        'name'    => 'campaign_id',
                        'tooltip' => esc_html__('Place the campaign ID here. This campaign ID will be used for all form submissions on this form. If left blank, the site url will be used', 'gf-mamoth-group'),
                        'label'   => esc_html__('Campaign ID', 'gf-mamoth-group'),
                        'type'    => 'text',
                        'class'   => 'medium',
                    ),
                    array(
                        'name'    => 'gfmg_google_analytics',
                        'tooltip' => esc_html__('Place your Google Analytics account ID here. This typically starts with "UA-", and will look like "UA-52478871-1". If a value is set in this field, a Google Analytics tracking script will automatically be added to the page in which this form appears.', 'gf-mamoth-group'),
                        'label'   => esc_html__('Google Analytics ID', 'gf-mamoth-group'),
                        'type'    => 'text',
                        'class'   => 'medium',
                    ),
                    array(
                        'label'   => esc_html__('Facebook Account ID', 'gf-mamoth-group'),
                        'type'    => 'text',
                        'name'    => 'gfmg_facebook',
                        'tooltip' => esc_html__('Place your Facebook account ID here. This typically starts is a number, and will look like "1529849607230335". If a value is set in this field, a Facebook Pageview tracking script will automatically be added to the page in which this form appears.', 'gf-mamoth-group'),
                        'class'   => 'medium',
                    ),
                    array(
                        'label'   => esc_html__('Dedicated form', 'gf-mamoth-group'),
                        'type'    => 'checkbox',
                        'name'    => 'is_dedicated',
                        'tooltip' => esc_html__('Check this option to enable <em>Dedicated form mode</em> for this form. <em>Dedicated form mode</em> will remove the <em>Province</em> and <em>Dealer</em> dropdowns from the form, if present. If a <a href="' . $admin_link . '">global Dealer ID</a> is set for this site, that will be sent to the API.', 'gf-mamoth-group'),
                        'choices' => array(
                            array(
                                'label' => esc_html__('Dedicated form', 'gf-mamoth-group'),
                                'name'  => 'is_dedicated',
                            ),
                        ),
                    ),
                ),
            ),
        );
    }

    /**
     * [repeat_text_option_type description]
     *
     * @since  [since]
     *
     * @param  [type]  $option_name [description]
     * @param  [type]  $option      [description]
     * @param  [type]  $values      [description]
     *
     * @return [type]               [description]
     */
    public function repeat_text_option_type($option_name, $option, $values) {

        $counter = 0;

        $output = '<div class="of-repeat-loop">';

        if (is_array($values)) {
            foreach ((array) $values as $value) {

                $output .= '<div class="of-repeat-group">';
                $output .= '<input class="of-input" name="' . esc_attr($option_name . '[' . $option['id'] . '][' . $counter . ']') . '" type="text" value="' . esc_attr($value) . '" />';
                $output .= '<button class="dodelete button icon delete">' . __('Remove') . '</button>';

                $output .= '</div><!–.of-repeat-group–>';

                $counter++;
            }
        }

        $output .= '<div class="of-repeat-group to-copy">';
        $output .= '<input class="of-input" data-rel="' . esc_attr($option_name . '[' . $option['id'] . ']') . '" type="text" value="' . esc_attr($option['std']) . '" />';
        $output .= '<button class="dodelete button icon delete">' . __('Remove') . '</button>';
        $output .= '</div><!–.of-repeat-group–>';

        $output .= '<button class="docopy button icon add">Add</button>';

        $output .= '</div><!–.of-repeat-loop–>';

        return $output;

    }
    // add_filter('optionsframework_repeat_text', 'repeat_text_option_type', 10, 3);
    //

    /*
     * Custom repeating field scripts
     * Add and Delete buttons
     */
    public function repeater_assets() {?>
        <style>
            #optionsframework .to-copy {display: none;}

            #optionsframework .of-repeat-group {
            overflow: hidden;
            margin-bottom: 1.4em;
            }
            #optionsframework .of-repeat-group .of-input {
            width: 80%;
            }

            .of-repeat-group .dodelete {
            float: right;
            }

            .gfmg-dealers-wrap {
                display: block;
                width:  100%;
            }

            span.gfmg-repeater-add {
                text-align: center;
                cursor: pointer;
                font-size: 1.5rem;
                background:  transparent;
                margin-left:  .5rem;
                padding: 4px;
            }
        </style>

        <script type="text/javascript">
            jQuery(function($){

                $('.gfmg-dealers-wrap .repeater-input-item:nth-child(1)').append('<span class="gfmg-repeater-add">&plus;</span>');

                $(".docopy").on("click", function(e){

                    // the loop object
                    $loop = $(this).parent();

                    // the group to copy
                    $group = $loop.find('.to-copy').clone().insertBefore($(this)).removeClass('to-copy');

                    // the new input
                    $input = $group.find('input');

                    input_name = $input.attr('data-rel');
                    count = $loop.children('.of-repeat-group').not('.to-copy').length;

                    $input.attr('name', input_name + '[' + ( count - 1 ) + ']');

                });

                $(".of-repeat-group").on("click", ".dodelete", function(e){
                    $(this).parent().remove();
                });

            });
        </script>
    <?php
}

    /**
     * Define the markup for the gfmg_dealers_type type field.
     *
     * @param array $field The field properties.
     * @param bool|true $echo Should the setting markup be echoed.
     */
    public function settings_gfmg_dealers_type($field, $echo = true) {

        $dealers = array();

        // Bail if the form object can't be found.
        if (isset($_GET['id'])) {
            $form = GFAPI::get_form($_GET['id']);
        } else {
            $form = false;
            error_log('settings_gfmg_dealers_type: Could not retrieve form ID');
        }

        $settings = $this->gfmg_settings($form);
        $dealers  = isset($settings['dealer_data']) ? $settings['dealer_data'] : array();

        if (!$dealers) {
            error_log('settings_gfmg_dealers_type: No dealer data found');

            // Always render one
            echo '<div class="repeater-input-item">';
            echo '<input title="Dealer Name" type="text" id="repeater-input-item-1" name="repeater-input-item-1" value="" />';
            echo '<input title="Dealer ID" type="text" />';
            echo '<input title="Dealer Province" type="text" />';
            // echo '<span class="remove-repeater-item">Remove</span>';
            echo '</div><!--end repeater-item -->';
        }

        error_log('da dealerZ: ' . print_r($settings, true));

        echo '<div class="gfmg-dealers-wrap">';

        foreach ($dealers as $dealer) {

            error_log('dealer single item: ' . print_r($dealer, true));
            echo '<div class="repeater-input-item">';
            echo '<input type="text" id="" name="' . $name . '" value="' . $value . '" />';
            echo '<input type="text" />';
            echo '<span class="remove-repeater-item">Remove</span>';
            echo '</div><!--end repeater-item -->';
        }

        // $dealers =

        $dealer_repeater_name = $field['args']['dealer_repeater_name'];

        $dealer_repeater_id = $field['args']['dealer_repeater_id'];
        $dealer_data        = $field['args']['dealer_data'];

        // $this->settings_text($dealer_repeater_name);
        // $this->settings_text($dealer_repeater_id);

        echo '</div><!--end gfmg-dealers-wrap-->';

        $this->repeater_assets();
    }

    /**
     * Define the markup for the custom_logic_type type field.
     *
     * Not used - kept for reference.
     *
     * @param array $field The field properties.
     * @param bool|true $echo Should the setting markup be echoed.
     */
    public function settings_custom_logic_type($field, $echo = true) {

        // Get the setting name.
        $name = $field['name'];

        // Define the properties for the checkbox to be used to enable/disable access to the simple condition settings.
        $checkbox_field = array(
            'name'    => $name,
            'type'    => 'checkbox',
            'choices' => array(
                array(
                    'label' => esc_html__('Enabled', 'gf-mamoth-group'),
                    'name'  => $name . '_enabled',
                ),
            ),
            'onclick' => "if(this.checked){jQuery('#{$name}_condition_container').show();} else{jQuery('#{$name}_condition_container').hide();}",
        );

        // Determine if the checkbox is checked, if not the simple condition settings should be hidden.
        $is_enabled      = $this->get_setting($name . '_enabled') == '1';
        $container_style = !$is_enabled ? "style='display:none;'" : '';

        // Put together the field markup.
        $str = sprintf("%s<div id='%s_condition_container' %s>%s</div>",
            $this->settings_checkbox($checkbox_field, false),
            $name,
            $container_style,
            $this->simple_condition($name)
        );

        echo $str;
    }

    /**
     * Build an array of choices containing fields which are compatible with conditional logic.
     *
     * Not used - kept for reference.
     *
     * @return array
     */
    public function get_conditional_logic_fields() {
        $form   = $this->get_current_form();
        $fields = array();
        foreach ($form['fields'] as $field) {
            if ($field->is_conditional_logic_supported()) {
                $inputs = $field->get_entry_inputs();

                if ($inputs) {
                    $choices = array();

                    foreach ($inputs as $input) {
                        if (rgar($input, 'isHidden')) {
                            continue;
                        }
                        $choices[] = array(
                            'value' => $input['id'],
                            'label' => GFCommon::get_label($field, $input['id'], true),
                        );
                    }

                    if (!empty($choices)) {
                        $fields[] = array('choices' => $choices, 'label' => GFCommon::get_label($field));
                    }

                } else {
                    $fields[] = array('value' => $field->id, 'label' => GFCommon::get_label($field));
                }

            }
        }

        return $fields;
    }

    /**
     * Evaluate the conditional logic.
     *
     * @param array $form The form currently being processed.
     * @param array $entry The entry currently being processed.
     *
     * @return bool
     */
    public function is_custom_logic_met($form, $entry) {
        if ($this->is_gravityforms_supported('2.0.7.4')) {
            // Use the helper added in Gravity Forms 2.0.7.4.

            return $this->is_simple_condition_met('custom_logic', $form, $entry);
        }

        // Older version of Gravity Forms, use our own method of validating the simple condition.
        $settings = $this->get_form_settings($form);

        $name       = 'custom_logic';
        $is_enabled = rgar($settings, $name . '_enabled');

        if (!$is_enabled) {
            // The setting is not enabled so we handle it as if the rules are met.

            return true;
        }

        // Build the logic array to be used by Gravity Forms when evaluating the rules.
        $logic = array(
            'logicType' => 'all',
            'rules'     => array(
                array(
                    'fieldId'  => rgar($settings, $name . '_field_id'),
                    'operator' => rgar($settings, $name . '_operator'),
                    'value'    => rgar($settings, $name . '_value'),
                ),
            ),
        );

        return GFCommon::evaluate_conditional_logic($logic, $form, $entry);
    }

    /**
     * [create_guid description]
     *
     * @since  1.0.0
     *
     * @param  string  $namespace [description]
     *
     * @return [type]             [description]
     */
    public function create_guid($namespace = '') {
        static $guid = '';
        $uid         = uniqid("", true);
        $data        = $namespace;
        $data .= $_SERVER['REQUEST_TIME'];
        $data .= $_SERVER['HTTP_USER_AGENT'];
        // $data .= $_SERVER['LOCAL_ADDR'];
        // $data .= $_SERVER['LOCAL_PORT'];
        $data .= $_SERVER['REMOTE_ADDR'];
        $data .= $_SERVER['REMOTE_PORT'];
        $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
        $guid = '{' .
        substr($hash, 0, 8) .
        '-' .
        substr($hash, 8, 4) .
        '-' .
        substr($hash, 12, 4) .
        '-' .
        substr($hash, 16, 4) .
        '-' .
        substr($hash, 20, 12) .
            '}';
        return $guid;
    }

    /**
     * Hides showroom code and province code fields if it's a dedicated form being
     *
     * @since 1.0.0
     *
     * @param [type]  $form [description]
     */
    public function set_conditional_logic_hide_province_and_showroom($form) {

        $settings = $this->gfmg_settings($form);

        if (isset($form['gf-mamoth-group']['is_dedicated'])) {
            if ($form['gf-mamoth-group']['is_dedicated']) {
                foreach ($form['fields'] as $field) {
                    if ($field->adminLabel == 'showroom_code' || $field->adminLabel == 'province_code') {
                        $field->isRequired      = 0;
                        $field->visibility      = 'hidden';
                        $field->is_field_hidden = 1;
                    }
                }
            }
        }

        if (isset($form['gf-mamoth-group']['enabled'])) {
            if ($form['gf-mamoth-group']['enabled']) {
                foreach ($form['fields'] as $field) {
                    if ($field->adminLabel == 'showroom_code' || $field->adminLabel == 'province_code') {
                        $field->isRequired      = 0;
                        $field->visibility      = 'hidden';
                        $field->is_field_hidden = 1;
                    }
                }
            }
        }

        return $form;
    }

    /**
     * TODO: Ensure fields are correct
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function pre_submission($form) {
        // error_log( '$_POST is: '. print_r( $_POST, true ) );
        // $settings_global = $this->get_addon_settings_values();

        $settings = $this->gfmg_settings($form);

        // dealerID is 18 in value form, 11 for other
        $is_value_form = $this->is_value_form($form);

        // Showroom code is 22 for value form, 10 for other
        if ($is_value_form) {
            // Value form
            // dealerID field: 18
            // $showroom_code = rgpost('input_22');
            //
            // ON = 23, BC = 28
            $_POST['input_18'] = !empty($_POST['input_23']) ? $_POST['input_23'] : $settings['dealer_id'];
            $_POST['input_18'] = !empty($_POST['input_24']) ? $_POST['input_24'] : $settings['dealer_id'];
            $_POST['input_18'] = !empty($_POST['input_25']) ? $_POST['input_25'] : $settings['dealer_id'];
            $_POST['input_18'] = !empty($_POST['input_26']) ? $_POST['input_26'] : $settings['dealer_id'];
            $_POST['input_18'] = !empty($_POST['input_27']) ? $_POST['input_27'] : $settings['dealer_id'];
            $_POST['input_18'] = !empty($_POST['input_28']) ? $_POST['input_28'] : $settings['dealer_id'];
        } else {
            // Regular lead form
            // dealerID field: 11
            // $showroom_code = rgpost('input_10');

            // 12 = ON, 17 = BC
            $_POST['input_11'] = !empty($_POST['input_12']) ? $_POST['input_12'] : $settings['dealer_id'];
            $_POST['input_11'] = !empty($_POST['input_13']) ? $_POST['input_13'] : $settings['dealer_id'];
            $_POST['input_11'] = !empty($_POST['input_14']) ? $_POST['input_14'] : $settings['dealer_id'];
            $_POST['input_11'] = !empty($_POST['input_15']) ? $_POST['input_15'] : $settings['dealer_id'];
            $_POST['input_11'] = !empty($_POST['input_16']) ? $_POST['input_16'] : $settings['dealer_id'];
            $_POST['input_11'] = !empty($_POST['input_17']) ? $_POST['input_17'] : $settings['dealer_id'];

        }
    }

    /**
     * Ensures no settings are left empty.
     *
     * @since  1.0.0
     *
     * @param  [type]  $form [description]
     *
     * @return [type]        [description]
     */
    public function gfmg_settings($form) {
        $settings        = array();
        $global_settings = $this->get_addon_settings_values();
        $form_settings   = $form['gf-mamoth-group'];

        $global_taarga      = !empty($global_settings['gfmg_global_taarga_access_token']) ? $global_settings['gfmg_global_taarga_access_token'] : '';
        $global_dealer_id   = !empty($global_settings['gfmg_global_dealer_id']) ? $global_settings['gfmg_global_dealer_id'] : '';
        $global_campaign_id = !empty($global_settings['gfmg_global_campaign_id']) ? $global_settings['gfmg_global_campaign_id'] : '';
        $global_ga          = !empty($global_settings['gfmg_global_google_analytics']) ? $global_settings['gfmg_global_google_analytics'] : '';
        $global_fb          = !empty($global_settings['gfmg_global_facebook']) ? $global_settings['gfmg_global_facebook'] : '';

        $settings = $form['gf-mamoth-group'];

        $settings['taarga_access_token']   = !empty($form_settings['taarga_access_token']) ? $form_settings['taarga_access_token'] : $global_taarga;
        $settings['dealer_id']             = !empty($form_settings['dealer_id']) ? $form_settings['dealer_id'] : $global_dealer_id;
        $settings['campaign_id']           = !empty($form_settings['campaign_id']) ? $form_settings['campaign_id'] : $global_campaign_id;
        $settings['gfmg_google_analytics'] = !empty($form_settings['gfmg_google_analytics']) ? $form_settings['gfmg_google_analytics'] : $global_ga;
        $settings['gfmg_facebook']         = !empty($form_settings['gfmg_facebook']) ? $form_settings['gfmg_facebook'] : $global_fb;

        return $settings;
    }

    /**
     * Provides an array of node IDs to hide (province and dealers), if a dedicated form is in use.
     *
     * @since  1.0.2
     *
     * @param  [array]  $form [Form array]
     *
     * @return [array]  $ids  HTML node IDs of elements to remove.
     */
    public function get_dealer_node_ids($form) {
        if (empty($form)) {
            return false;
        }

        $ids = array();

        foreach ($form['fields'] as $field) {
            switch ($field['adminLabel']) {
            case 'province_code':
            case 'dealers_on':
            case 'dealers_ab':
            case 'dealers_mb':
            case 'dealers_qc':
            case 'dealers_bc':
                $ids[] = 'field_' . $form['id'] . '_' . $field['id'];
                break;
            default:
                break;
            }
        }

        return $ids;
    }

    public function is_value_form($form) {
        if (empty($form)) {
            return false;
        }

        $labels = array();

        foreach ($form['fields'] as $field) {
            $labels[] = $field['adminLabel'];
        }

        if (in_array('trim', $labels) || in_array('style', $labels)) {
            return true;
        }

        return false;
    }

    /**
     * Performing a custom action at the end of the form submission process.
     *
     * @param array $entry The entry currently being processed.
     * @param array $form The form currently being processed.
     */
    public function after_submission($entry, $form) {
        $is_value_form = $this->is_value_form($form);

        // error_log('after_submission - $form object: ' . print_r($form, true));

        $settings = $this->gfmg_settings($form);

        $data = array();

        $data['campaign_id'] = $settings['campaign_id'];

        if ($is_value_form) {

            $uniqId = $this->create_guid($entry[14]);

            if (isset($form['gf-mamoth-group']['is_dedicated'])) {
                if ($form['gf-mamoth-group']['is_dedicated']) {
                    $showroom_code = $settings['dealer_id'];
                }
            } elseif (!empty($entry[23])) {
                $showroom_code = $entry[23];
            } elseif (!empty($entry[24])) {
                $showroom_code = $entry[24];
            } elseif (!empty($entry[25])) {
                $showroom_code = $entry[25];
            } elseif (!empty($entry[26])) {
                $showroom_code = $entry[26];
            } elseif (!empty($entry[27])) {
                $showroom_code = $entry[27];
            } elseif (!empty($entry[28])) {
                $showroom_code = $entry[28];
            } else {
                $showroom_code = $settings['dealer_id'];
            }

            $data['carWorth']                      = true;
            $data['dealerID']                      = $showroom_code;
            $data['dealer_id']                     = $showroom_code;
            $data['name']                          = $entry[14];
            $data['email']                         = $entry[2];
            $data['phone']                         = $entry[3];
            $data['postal_code']                   = isset($entry[4]) ? $entry[4] : '';
            $data['provin']                        = isset($entry[21]) ? $entry[21] : '';
            $data['province']                      = isset($entry[21]) ? $entry[21] : '';
            $data['showroom_code']                 = $showroom_code;
            $data['leadId']                        = $uniqId;
            $data['meta_data']                     = array();
            $data['meta_data']['vehicle_year']     = $entry[8];
            $data['meta_data']['vehicle_make']     = $entry[9];
            $data['meta_data']['vehicle_model']    = $entry[10];
            $data['meta_data']['vehicle_trim']     = $entry[11];
            $data['meta_data']['vehicle_style']    = $entry[12];
            $data['meta_data']['vehicle_distance'] = $entry[13];
            $data['year']                          = $entry[8];
            $data['make']                          = $entry[9];
            $data['model']                         = $entry[10];
            $data['trim']                          = $entry[11];
            $data['style']                         = $entry[12];
            $data['distance']                      = $entry[13];

            // Trade in high and low
            $data['meta_data']['vehicle_tradeInHigh'] = $entry[19];
            $data['meta_data']['vehicle_tradeInLow']  = $entry[20];
            $data['tradeInHigh']                      = $entry[19];
            $data['tradeInLow']                       = $entry[20];

            $data['currVehicle']     = $data['year'] . ' ' . $data['make'] . ' ' . $data['model'];
            $data['currDescription'] = $data['year'] . ' ' . $data['make'] . ' ' . $data['model'];
        } else {
            error_log('Is not value form...');

            $uniqId = $this->create_guid($entry[8]);

            if (isset($form['gf-mamoth-group']['is_dedicated'])) {
                if ($form['gf-mamoth-group']['is_dedicated']) {
                    $showroom_code = $settings['dealer_id'];
                }
            } elseif (!empty($entry[12])) {
                $showroom_code = $entry[12];
            } elseif (!empty($entry[13])) {
                $showroom_code = $entry[13];
            } elseif (!empty($entry[14])) {
                $showroom_code = $entry[14];
            } elseif (!empty($entry[15])) {
                $showroom_code = $entry[15];
            } elseif (!empty($entry[16])) {
                $showroom_code = $entry[16];
            } elseif (!empty($entry[17])) {
                $showroom_code = $entry[17];
            } else {
                $showroom_code = $settings['dealer_id'];
            }

            $data['carWorth']                         = false;
            $data['dealerID']                         = $showroom_code;
            $data['dealer_id']                        = $showroom_code;
            $data['name']                             = $entry[8];
            $data['email']                            = $entry[2];
            $data['phone']                            = $entry[3];
            $data['postal_code']                      = $entry[6];
            $data['provin']                           = isset($entry[9]) ? $entry[9] : '';
            $data['province']                         = isset($entry[9]) ? $entry[9] : '';
            $data['showroom_code']                    = $showroom_code;
            $data['currVehicle']                      = $entry[4];
            $data['leadId']                           = $uniqId;
            $data['meta_data']                        = array();
            $data['meta_data']['vehicle_year']        = '';
            $data['meta_data']['vehicle_make']        = '';
            $data['meta_data']['vehicle_model']       = '';
            $data['meta_data']['vehicle_trim']        = '';
            $data['meta_data']['vehicle_style']       = '';
            $data['meta_data']['vehicle_distance']    = '';
            $data['meta_data']['vehicle_tradeInHigh'] = '';
            $data['meta_data']['vehicle_tradeInLow']  = '';
        }

        $account             = $this->account;
        $taarga_access_token = $settings['taarga_access_token'];
        $dealerID            = $data['dealerID'];

        $submitted  = true;
        $load_error = false;

        $sendToAPI = $is_value_form ? true : false;

        $params = array(
            'year=' . urlencode($data['meta_data']['vehicle_year']),
            'make=' . urlencode($data['meta_data']['vehicle_make']),
            'model=' . urlencode($data['meta_data']['vehicle_model']),
            'trim=' . urlencode($data['meta_data']['vehicle_trim']),
            'style=' . urlencode($data['meta_data']['vehicle_style']),
            'distance=' . urlencode($data['meta_data']['vehicle_distance']),
            'clientName=' . urlencode($data['name']),
            'clientEmail=' . urlencode($data['email']),
            'clientPhNum=' . urlencode($data['phone']),
            'clientZipCode=' . urlencode($data['postal_code']),
            'leadId=' . urlencode($uniqId),
            'sendToAPI=' . $sendToAPI,
            'dealerID=' . $dealerID,
        );

        // Process social lead for both form types
        $ar_api_report            = new ar_api($data);
        $ar_api_report->lang      = 'en';
        $ar_api_report->ad_source = 'main';
        $ar_api_report->data      = $data;
        $ar_api_report->dealerID  = $dealerID;
        $ar_api_report->offer     = urlencode($form['title']);
        $ar_api_report->create();

        $lead = new SocialLead($data);
        // $lead->api_url = "https://x1.taarga.com/social/lead/create?access_token=7ee09d42db0f0246d80d4c19263a8830&campaign_id=34&offer=Let%27s+Trade+Keys";
        $url_string      = 'https://x1.taarga.com/social/lead/create?access_token=' . $taarga_access_token . '&campaign_id=' . $data['campaign_id'] . '&offer=' . urlencode($form['title']) . '&dealerID=' . $data['dealer_id'];
        $lead->api_url   = $url_string;
        $lead->lang      = 'en';
        $lead->ad_source = 'main';
        $lead->data      = $data;
        $lead->create();
    }

    /**
     * Determines the request type by parsing an array of provided request parameters.
     * This is then routed to the API caller.
     *
     * @since  1.0.0
     *
     * @param  [type]  $params [description]
     *
     * @return [type]          [description]
     */
    public function calc_request_type($type_data) {
        $values = array(
            'year'     => !empty($type_data['year']) ? true : false,
            'make'     => !empty($type_data['make']) ? true : false,
            'model'    => !empty($type_data['model']) ? true : false,
            'trim'     => !empty($type_data['trim']) ? true : false,
            'style'    => !empty($type_data['style']) ? true : false,
            'distance' => !empty($type_data['distance']) ? true : false,
        );

        // If all are true, return price as the type.
        if (in_array('false', $values, true)) {
            return 'price';
        }

        $type = '';

        if ($values['year']) {
            $type = 'year';

            if ($values['make'] && !$values['model']) {
                $type = 'make';
            }

            if ($values['model'] && !$values['trim']) {
                $type = 'model';
            }

            if ($values['model'] && !$values['trim']) {
                $type = 'model';
            }

            if ($values['trim'] && !$values['style']) {
                $type = 'trim';
            }

            if ($values['style'] && !$values['distance']) {
                $type = 'style';
            }

            if ($values['distance']) {
                $type = 'distance';
            }
        }

        return $type;
    }

    /**
     * Returns provinces.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_provinces() {
        return array(
            array(
                'text'  => 'Ontario',
                'value' => '1',
            ),
            array(
                'text'  => 'Manitoba',
                'value' => '2',
            ),
            array(
                'text'  => 'Alberta',
                'value' => '3',
            ),
            array(
                'text'  => 'British Columbia',
                'value' => '4',
            ),
        );
    }

    /**
     * Returns dealer data. Obsolete; conditional GF fields are now used.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function dealer_data() {
        return array(
            'ON' => array(
                array(
                    'value' => '519',
                    'text'  => 'Etobicoke - Islington Chrysler',
                ),
                array(
                    'value' => '5432',
                    'text'  => 'Midland - Midland Chrysler',
                ),
                array(
                    'value' => '265',
                    'text'  => 'Mississauga - Erin Dodge',
                ),
            ),
            'AB' => array(
                array(
                    'value' => '628',
                    'text'  => 'Ponoka - Ponoka Chrysler',
                ),
                array(
                    'value' => '12290',
                    'text'  => 'Red Deer - Go Dodge Red Deer',
                ),
            ),
            'SK' => array(
            ),
            'MB' => array(
                array(
                    'value' => '354',
                    'text'  => 'Selkirk - Selkirk Chrysler',
                ),
            ),
            'QC' => array(
            ),
            'BC' => array(
            ),
        );
    }

    /**
     * Returns vehicle dealer data. Obsolete.
     *
     * For reference, the originally provided data is:
     *
     * Dealer              Province   DealerID
     * ----------------------------------------
     * Airdrie Dodge       AB         52
     * Fox Chrysler        ON         730
     * Varsity Chrysler    AB         324
     * Downsview Chrysler  ON         368
     * Kawartha Chrysler   ON         489
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_dealers() {
        $dealers = $this->dealer_data();

        return array(
            '1' => $dealers['ON'],
            '2' => $dealers['MB'],
            '3' => $dealers['AB'],
            '4' => $dealers['BC'],
        );
    }

    /**
     * Makes request on pre-submission hook for trade-in prices.
     * Obsolete; kept for reference.
     *
     * @since  1.0.0
     *
     * @param  [type]  $params [description]
     *
     * @return [type]          [description]
     */
    public function get_vehicle_trade_info($form) {
        if (!$this->is_value_form($form)) {
            return;
        }

        $settings = $this->gfmg_settings($form);
        $uniqId   = $this->create_guid($_POST['input_14']);

        $showroom_code = !empty($_POST['input_23']) ? $_POST['input_23'] : $settings['dealer_id'];
        $showroom_code = !empty($_POST['input_24']) ? $_POST['input_24'] : $settings['dealer_id'];
        $showroom_code = !empty($_POST['input_25']) ? $_POST['input_25'] : $settings['dealer_id'];
        $showroom_code = !empty($_POST['input_26']) ? $_POST['input_26'] : $settings['dealer_id'];
        $showroom_code = !empty($_POST['input_27']) ? $_POST['input_27'] : $settings['dealer_id'];
        $showroom_code = !empty($_POST['input_28']) ? $_POST['input_28'] : $settings['dealer_id'];

        $data['name']          = $_POST['input_14'];
        $data['email']         = $_POST['input_2'];
        $data['phone']         = $_POST['input_3'];
        $data['postal_code']   = $_POST['input_4'];
        $data['provin']        = $_POST['input_9'];
        $data['showroom_code'] = $showroom_code;
        $data['currVehicle']   = $_POST['input_4'];
        $data['dealerID']      = !empty($showroom_code) ? $showroom_code : $settings['dealer_id'];
        $data['leadId']        = $uniqId;
        $data['year']          = $_POST['input_8'];
        $data['make']          = $_POST['input_9'];
        $data['model']         = $_POST['input_10'];
        $data['trim']          = $_POST['input_11'];
        $data['style']         = $_POST['input_12'];
        $data['distance']      = $_POST['input_13'];
        // $data['tradeInHigh']   = $_POST['input_19'];
        // $data['tradeInLow']    = $_POST['input_20'];

        $response = $this->get_vehicle_data('price', $data);

        return;

        $localized = $this->get_localized_data();
        $base      = $localized['load_url'];

        $trade_url = $base . '?' . http_build_query($data, '', '&');

        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

        $xml = @file_get_contents($trade_url, false, $context);

        if ($xml != false) {
            $xml = @simplexml_load_string($xml);
        }

        if ($xml === false) {
            $load_error = true;
        }
    }

    public function get_vehicle_data($type = '', $params = array()) {
        if (empty($type)) {
            return false;
        }

        $price_request = false;
        $data          = array();
        $url           = '';

        // Should this key also be a GF setting?
        $base = 'https://ar.absoluteresults.com/taarga/cbb.php?key=9C941INMK6';

        switch ($type) {
        case 'dealers':
        case 'dealer':
            $data = $this->get_dealers();
            break;
        case 'provinces':
        case 'province':
            $data = $this->get_provinces();
            break;
        case 'years':
        case 'year':
            $url = $base . '&getYears';
            break;
        case 'makes':
        case 'make':
            if (empty($params)) {
                return false;
            }
            $url = $base . '&getMakes=&year=' . $params['year'];
            break;
        case 'models':
        case 'model':
            if (empty($params)) {
                return false;
            }
            $url = $base . '&getModels=&year=' . $params['year'] . '&make=' . $params['make'];
            break;
        case 'styles':
        case 'style':
            if (empty($params)) {
                return false;
            }
            $url = $base . '&getStyles=&year=' . $params['year'] . '&make=' . $params['make'] . '&model=' . $params['model'] . '&trim=' . $params['trim'];
            break;
        case 'trims':
        case 'trim':
            if (empty($params)) {
                return false;
            }
            $url = $base . '&getTrims=&year=' . $params['year'] . '&make=' . $params['make'] . '&model=' . $params['model'];
            break;
        case 'price':
        case 'prices':
            if (empty($params)) {
                return false;
            }
            $price_request = true;
            $url           = $base . '&getPrice=&year=' . $params['year'] . '&make=' . $params['make'] . '&model=' . $params['model'] . '&trim=' . $params['trim'] . '&style=' . $params['style'] . '&distance=' . $params['distance'];
            break;
        }

        if (!empty($url)) {

            $response = wp_remote_get($url);

            if (is_wp_error($response)) {
                return $data;
            }

            $data = wp_remote_retrieve_body($response);
        }

        return $data;
    }

    /**
     * Gets vehicle years.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_vehicle_years() {
        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $this->get_vehicle_data('years');
            die();
        } else {
            exit();
        }
    }

    /**
     * Gets vehicle makes.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_vehicle_makes() {
        $params = array(
            'year' => $_REQUEST['year'],
        );

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $this->get_vehicle_data('makes', $params);
            die();
        } else {
            exit();
        }
    }

    /**
     * Gets vehicle models.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_vehicle_models() {
        $params = array(
            'year' => $_REQUEST['year'],
            'make' => $_REQUEST['make'],
        );

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $this->get_vehicle_data('models', $params);
            die();
        } else {
            exit();
        }
    }

    /**
     * Gets vehicle trims
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_vehicle_trims() {
        $params = array(
            'year'  => $_REQUEST['year'],
            'make'  => $_REQUEST['make'],
            'model' => $_REQUEST['model'],
        );

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $this->get_vehicle_data('trims', $params);
            die();
        } else {
            exit();
        }
    }

    /**
     * Gets vehicle styles.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_vehicle_styles() {
        $params = array(
            'year'  => $_REQUEST['year'],
            'make'  => $_REQUEST['make'],
            'model' => $_REQUEST['model'],
            'trim'  => $_REQUEST['trim'],
        );

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $this->get_vehicle_data('styles', $params);
            die();
        } else {
            exit();
        }
    }

    /**
     * Gets vehicle trade-in high and low prices.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_vehicle_price() {
        $params = array(
            'year'     => $_REQUEST['year'],
            'make'     => $_REQUEST['make'],
            'model'    => $_REQUEST['model'],
            'trim'     => $_REQUEST['trim'],
            'style'    => $_REQUEST['style'],
            'distance' => $_REQUEST['distance'],
        );

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $this->get_vehicle_data('price', $params);
            die();
        } else {
            exit();
        }
    }

    /**
     * Makes form data available to plugin js.
     *
     * @since  1.0.0
     *
     * @return [type]  [description]
     */
    public function get_localized_data() {
        return array(
            'provinces' => $this->get_provinces(),
            'dealers'   => $this->get_dealers(),
            'load_url'  => trailingslashit(GFMG_PLUGIN_URL) . 'lib/listLoad.php',
            'ajax_url'  => admin_url('admin-ajax.php'),
        );
    }

    /**
     * Settings field validation
     *
     * @param string $value The setting value.
     *
     * @return bool
     */
    public function is_valid_setting($value) {
        return strlen($value) < 100;
    }
}
