<?php
/*
Plugin Name: GF - Mamoth Group Custom Add-On
Plugin URI: http://chryslertradekeys.ca
Description: A custom Gravity Forms add-on for Mamoth Group.
Version: 1.0.5
Author: gf-mamoth-group
Author URI: http://chryslertradekeys.ca
 */

define('GF_MAMOTH_GROUP_ADDON_VERSION', '1.0.5');

add_action('gform_loaded', array('GF_MG_Bootstrap', 'load'), 5);

class GF_MG_Bootstrap {

    public static function load() {

        if (!method_exists('GFForms', 'include_addon_framework')) {
            return;
        }

        require_once 'includes/class-gfmamothgroup.php';

        GFAddOn::register('GFMamothGroup');
    }

}

/**
 * Returns instance of the GFMamothGroup class.
 *
 * @since  [since]
 *
 * @return [type]  [description]
 */
function gf_mamoth_group() {
    return GFMamothGroup::get_instance();
}
