var project = 'gf-mamoth-group';
var projectURL = 'chryslertradekeys.ca';
var productURL = './';

var styleSRC = './assets/scss/main.scss';
var styleDestination = './assets/css/';

var jsVendorRC = './assets/js/vendor/*.js';
var jsVendorDestination = './assets/js/';
var jsVendorFile = 'vendor';

var jsCustomSRC = './assets/js/partials/*.js';
var jsCustomDestination = './assets/js/';
var jsCustomFile = 'main';

// Watch files paths.
var styleWatchFiles = './assets/scss/*.scss';
var vendorJSWatchFiles = './assets/js/vendor/*.js';
var mainJSWatchFiles = './assets/js/partials/*.js';
var projectPHPWatchFiles = './**/*.php';

const AUTOPREFIXER_BROWSERS = [
    'last 2 version',
    '> 1%',
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 34',
    'chrome >= 39',
    'safari >= 7',
    'opera >= 23',
    'ios >= 8',
    'android >= 4',
    'bb >= 10'
  ];

var gulp = require( 'gulp' );

// CSS related
var sass = require( 'gulp-sass' );
var minifycss = require( 'gulp-uglifycss' );
var autoprefixer = require( 'gulp-autoprefixer' );
var mmq = require( 'gulp-merge-media-queries' );

var concat = require( 'gulp-concat' );
var uglify = require( 'gulp-uglify' );

// Utilities.
var rename = require( 'gulp-rename' );
var filter = require( 'gulp-filter' );

var sourcemaps = require( 'gulp-sourcemaps' );

var notify = require( 'gulp-notify' );
var browserSync = require( 'browser-sync' ).create();
var reload = browserSync.reload;

gulp.task( 'browser-sync', function() {
	browserSync.init( {

		proxy: projectURL,

		// can get annoying
		open: false,

		// reload for every css change
		// injectChanges: true,

		// custom port
		// port: 7000,

	} );
} );

gulp.task( 'styles', function() {

	gulp.src( styleSRC )
		.pipe( sourcemaps.init() )
		.pipe( sass( {
			errLogToConsole: true,
			// choose from 'compressed', 'nested', or 'expanded'
			outputStyle: 'compact',
			precision: 10
		} ) )
		.on( 'error', console.error.bind( console ) )
		.pipe( sourcemaps.write( {
			includeContent: false
		} ) )
		.pipe( sourcemaps.init( {
			loadMaps: true
		} ) )
		.pipe( autoprefixer( AUTOPREFIXER_BROWSERS ) )

	.pipe( sourcemaps.write( '.' ) )
		.pipe( gulp.dest( './assets/css/' ) )

	.pipe( filter( '**/*.css' ) )
		.pipe( mmq( {
			log: true
		} ) )

	.pipe( rename( {
		suffix: '.min'
	} ) )
		.pipe( minifycss( {
			maxLineLen: 10
		} ) )
		.pipe( gulp.dest( styleDestination ) )
		.pipe( browserSync.stream() )
		.pipe( notify( {
			message: 'task: "styles" completed.',
			onLast: true
		} ) )
} );

gulp.task( 'vendorJs', function() {
	gulp.src( jsVendorRC )
		.pipe( concat( jsVendorFile + '.js' ) )
		.pipe( gulp.dest( jsVendorDestination ) )
		.pipe( rename( {
			basename: jsVendorFile,
			suffix: '.min'
		} ) )
		.pipe( uglify() )
		.pipe( gulp.dest( jsVendorDestination ) )
		.pipe( notify( {
			message: 'task: "vendorJS" completed.',
			onLast: true
		} ) );
} );

gulp.task( 'mainJS', function() {
	gulp.src( jsCustomSRC )
		.pipe( concat( jsCustomFile + '.js' ) )
		.pipe( gulp.dest( jsCustomDestination ) )
		.pipe( rename( {
			basename: jsCustomFile,
			suffix: '.min'
		} ) )
		.pipe( uglify() )
		.pipe( gulp.dest( jsCustomDestination ) )
		.pipe( notify( {
			message: 'task: "mainJS" completed.',
			onLast: true
		} ) );
} );

gulp.task( 'build', [ 'styles', 'vendorJs', 'mainJS' ], function() {
	gulp.watch( projectPHPWatchFiles, reload );
	gulp.watch( styleWatchFiles, [ 'styles', reload ] );
	gulp.watch( vendorJSWatchFiles, [ 'vendorJs', reload ] );
	gulp.watch( mainJSWatchFiles, [ 'mainJS', reload ] );
} );
