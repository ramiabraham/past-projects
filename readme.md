### Coding samples for review!

A collection of WordPress projects created for clients, product developement, and a couple WordPress-focused command line utilities, all created within the last few years.

- `product-dev` contains various products from the AffiliateWP product family, created
during my time at Sandhills Development, LLC.  Front and back-end. The `affiliate-wp` contains many contributors, but all other subdirs were written solely by me.

- `client` contains various client works developed at WDS and Maintainn.

- `utility` is just a couple bash scripts, to demonstrate cli comfort.

Thanks for the checking it out!
